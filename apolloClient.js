import { ApolloClient, toIdValue, IntrospectionFragmentMatcher } from 'react-apollo'

import { createNetworkInterface } from './utils'

// The object id retrieved is already unique, it's actually
// ObjectType:primaryKey encoded in base64, so we define our
// own dataIdFromObject instead of using the default one `${o.__typename}:o.id`.
// This allows us to define a custom resolver for the node query.
// for more info about customResolvers, read
// http://dev.apollodata.com/react/query-splitting.html
const fragmentMatcher = new IntrospectionFragmentMatcher({
  introspectionQueryResultData: {
    __schema: {
      types: [
        {
          kind: 'UNION',
          name: 'EntityUnion',
          possibleTypes: [{ name: 'Root' }, { name: 'Idea' }, { name: 'Person' }],
        },
        {
          kind: 'INTERFACE',
          name: 'IEntity',
          possibleTypes: [{ name: 'Root' }, { name: 'Idea' }, { name: 'Person' }],
        },
      ],
    },
  },
})
const dataIdFromObject = o => o.id

const apolloClient = new ApolloClient({
  dataIdFromObject,
  fragmentMatcher,
  customResolvers: {
    Query: {
      node: (_, args) => toIdValue(dataIdFromObject({ id: args.id })),
    },
  },
  networkInterface: createNetworkInterface({
    uri: '/graphql',
  }),
})

export default apolloClient
