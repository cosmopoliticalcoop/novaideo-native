const path = require('path')

module.exports = {
  entry: {
    main: './index.web.js',
  },
  output: {
    path: path.join(__dirname, 'build'),
    filename: '[name].js',
  },
  module: {
    rules: [
      {
        test: /\.js?$/,
        exclude: /node_modules/,
        use: {
          loader: 'babel-loader',
          options: {
            presets: ['react-native'],
          },
        },
      },
      {
        test: /\.js?$/,
        include: [
          path.resolve(__dirname, 'node_modules/react-native-hyperlink'),
          path.resolve(__dirname, 'node_modules/react-native-menu'),
          path.resolve(__dirname, 'node_modules/react-native-swiper'),
          path.resolve(__dirname, 'node_modules/react-native-vector-icons'),
        ],
        use: {
          loader: 'babel-loader',
          options: {
            presets: ['react-native'],
          },
        },
      },
      {
        test: /\.(ttf|eot|svg|png)(\?v=[0-9]\.[0-9]\.[0-9])?$/,
        loader: 'file-loader',
      },
    ],
  },
  resolve: {
    alias: {
      'react-native': 'react-native-web',
    },
    extensions: ['.web.js', '.js'],
  },
}
