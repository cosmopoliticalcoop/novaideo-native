import React from 'react'
import { View, StyleSheet, Image, Text } from 'react-native'

import Icon from './Icon'
import Avatar from './Avatar'
import NovaIdeoLogo from '../novaideo_logo.png'
import SeparatorComponent from './SeparatorComponent'
import { DEFAULT_USER_IMG } from '../constants'

const styles = StyleSheet.create({
  userDataContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    width: '100%',
    padding: 5,
    backgroundColor: '#ececec',
    borderWidth: 0.5,
    borderColor: '#d6d7da',
    borderTopLeftRadius: 3,
    borderTopRightRadius: 3,
  },
  avatarContainer: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  userTitle: {
    marginLeft: 5,
  },
  instanceContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    width: '100%',
    paddingTop: 5,
    paddingBottom: 5,
  },
  instanceLogo: {
    width: 40,
    height: 40,
    marginLeft: 10,
  },
  instanceTitle: {
    fontSize: 15,
  },
  menu: {
    padding: 10,
    paddingTop: 10,
    paddingBottom: 10,
  },
})

const AccountInformations = ({ globalProps, instance, options, theme }) => {
  const logo = instance.logo
  const account = globalProps.account
  return (
    <View>
      <View style={styles.userDataContainer}>
        {account
          ? <View style={styles.avatarContainer}>
            <Avatar
              size={30}
              borderRadius={15}
              image={
                <Image
                  source={
                      account.picture ? { uri: `${account.picture.url}/profil` } : DEFAULT_USER_IMG
                    }
                />
                }
            />
            <Text style={styles.userTitle}>
              {account.title}
            </Text>
          </View>
          : <Icon name="account-circle" size={25} color={theme.primary.bgColor} />}
      </View>
      <View style={styles.instanceContainer}>
        <Text style={styles.instanceTitle}>
          {instance.title}
        </Text>
        <Image
          resizeMode="contain"
          source={logo ? { uri: logo } : NovaIdeoLogo}
          style={styles.instanceLogo}
        />
      </View>
      <SeparatorComponent />
      <View style={styles.menu}>
        {options}
      </View>
    </View>
  )
}

export default AccountInformations
