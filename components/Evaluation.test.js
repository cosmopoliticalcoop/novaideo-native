import React from 'react'
import renderer from 'react-test-renderer'

import Evaluation from './Evaluation'

describe('Evaluation component', () => {
  it('should match Evaluation snapshot', () => {
    const props = {
      icon: { top: 'arrow-up-drop-circle', down: 'arrow-down-drop-circle' },
      text: { top: '40', down: '2' },
    }
    const rendered = renderer.create(<Evaluation {...props} />).toJSON()
    expect(rendered).toMatchSnapshot()
  })
})
