import React from 'react'
import ShallowRenderer from 'react-test-renderer/shallow'

import { DumbApp } from './NovaIdeoApp'

describe('Nova-Ideo native app', () => {
  it('should match snapshot', () => {
    const myinstance = {
      id: 'myinstance',
      loadingState: 'completed',
      url: 'http://myinstance.nova-ideo.com',
    }
    const user = { token: undefined, loadingState: 'completed' }
    const renderer = new ShallowRenderer()
    renderer.render(<DumbApp instance={myinstance} user={user} loadAdapters={() => {}} />)
    const rendered = renderer.getRenderOutput()
    expect(rendered).toMatchSnapshot()
  })
})
