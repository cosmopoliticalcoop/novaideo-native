/* eslint-disable jsx-boolean-value */
import React from 'react'
import { View, Image, StyleSheet, ActivityIndicator, Linking } from 'react-native'
import { connect } from 'react-redux'
import { withApollo } from 'react-apollo'
import Menu, { MenuOptions, MenuTrigger, MenuOption } from 'react-native-menu'
import { withTheme } from 'glamorous-native'

import IconWithText from './IconWithText'
import Icon from './Icon'
import AccountInformations from './AccountInformations'
import { logout, initInstance, setURLState, initLogin, updateUserToken } from '../actions'
import Avatar from './Avatar'
import { DEFAULT_USER_IMG } from '../constants'
import I18n from '../locale'

const styles = StyleSheet.create({
  styleIconText: {
    paddingLeft: 3,
    paddingRight: 3,
    fontSize: 14,
    color: '#4a4a4a',
  },
})

export class DumbAccountActivator extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      viewOpened: false,
    }
    this.onLogin = this.onLogin.bind(this)
    this.onViewClose = this.onViewClose.bind(this)
    this.onLogout = this.onLogout.bind(this)
    this.onChangeInstance = this.onChangeInstance.bind(this)
    this.onSelect = this.onSelect.bind(this)
    this.onChangeTheme = this.onChangeTheme.bind(this)
  }

  onChangeTheme() {
    if (!this.state.viewOpened) {
      this.setState({
        viewOpened: true,
      })
      return this.props.globalProps.navigation.navigate('ChangeTheme', {
        onClose: this.onViewClose,
        client: this.props.client,
        title: I18n.t('changeTheme'),
      })
    }
    return undefined
  }

  onLogin() {
    if (!this.state.viewOpened) {
      this.setState({
        viewOpened: true,
        logoutProgress: false,
      })
      return this.props.globalProps.navigation.navigate('Login', {
        onClose: this.onViewClose,
        client: this.props.client,
      })
    }
    return undefined
  }

  onLogout() {
    this.setState({ logoutProgress: true }, () =>
      this.props
        .logout(this.props.instance)
        .then(() => {
          const { instance, client } = this.props
          this.props.updateUserToken(instance, undefined)
          if (instance.isPrivate) {
            client.resetStore()
          } else {
            this.setState({ logoutProgress: false }, client.resetStore)
          }
        })
        .catch(() => {
          this.setState({ logoutProgress: false }, this.props.initLogin)
        }),
    )
  }

  onChangeInstance() {
    if (!this.state.viewOpened) {
      this.setState({
        viewOpened: true,
        logoutProgress: false,
      })
      return this.props.globalProps.navigation.navigate('InstanceSwitch', {
        onClose: this.onViewClose,
        client: this.props.client,
        title: I18n.t('changeInstance'),
      })
    }
    return undefined
  }

  onViewClose() {
    if (this.state.viewOpened) {
      this.setState({ viewOpened: false })
    }
  }

  onSelect(value) {
    switch (value) {
      case 'login':
        this.onLogin()
        break
      case 'logout':
        this.onLogout()
        break
      case 'changeInstance':
        this.onChangeInstance()
        break
      case 'changeTheme':
        this.onChangeTheme()
        break
      case 'privacyPolicy':
        Linking.openURL(I18n.t('policyUrl'))
        break
      default:
    }
    if (value) {
      this.setState({ viewOpened: false })
    }
  }

  render() {
    const { instance, network, theme, globalProps } = this.props
    const { logoutProgress } = this.state
    if (logoutProgress) {
      return <ActivityIndicator animating color={'white'} />
    }

    const account = globalProps.account
    const menuIconSize = 15
    const networkError = !network.isConnected || network.url.error
    return (
      <Menu onSelect={this.onSelect}>
        <MenuTrigger>
          <View>
            {network.isLogged
              ? <Avatar
                size={30}
                borderRadius={15}
                image={
                  <Image
                    source={
                        account && account.picture
                          ? { uri: `${account.picture.url}/profil` }
                          : DEFAULT_USER_IMG
                      }
                  />
                  }
              />
              : <Icon name="account-circle" size={25} color={theme.primary.color} />}
          </View>
        </MenuTrigger>
        <MenuOptions
          optionsContainerStyle={{ width: 255 }}
          renderOptionsContainer={options =>
            <AccountInformations {...{ globalProps, instance, options, theme }} />}
        >
          {network.isLogged
            ? <MenuOption disabled={networkError} value={'logout'}>
              <IconWithText
                iconColor="#ef6e18"
                styleText={styles.styleIconText}
                name="logout"
                text={I18n.t('logout')}
                iconSize={menuIconSize}
              />
            </MenuOption>
            : <MenuOption disabled={networkError} value={'login'}>
              <IconWithText
                iconColor="#54902a"
                styleText={styles.styleIconText}
                name="login"
                text={I18n.t('login')}
                iconSize={menuIconSize}
              />
            </MenuOption>}
          <MenuOption disabled={networkError} value={'changeInstance'}>
            <IconWithText
              iconColor="#4580c5"
              styleText={styles.styleIconText}
              name="apps"
              text={I18n.t('changeInstance')}
              iconSize={menuIconSize}
            />
          </MenuOption>
          <MenuOption value={'changeTheme'}>
            <IconWithText
              iconColor="#e32636"
              styleText={styles.styleIconText}
              name="format-color-fill"
              text={I18n.t('changeTheme')}
              iconSize={16}
            />
          </MenuOption>
          <MenuOption value={'privacyPolicy'}>
            <IconWithText
              iconColor="gray"
              styleText={styles.styleIconText}
              name="information"
              text={I18n.t('privacyPolicy')}
              iconSize={16}
            />
          </MenuOption>
        </MenuOptions>
      </Menu>
    )
  }
}

export const mapStateToProps = state => ({
  instance: state.instance,
  network: state.network,
  globalProps: state.globalProps,
})

export const mapDispatchToProps = {
  logout,
  updateUserToken,
  initLogin,
  initInstance,
  setURLState,
}

export default withApollo(
  connect(mapStateToProps, mapDispatchToProps)(withTheme(DumbAccountActivator)),
)
