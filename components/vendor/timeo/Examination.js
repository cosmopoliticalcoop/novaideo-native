import React from 'react'
import { View, StyleSheet, TouchableOpacity, Alert, Platform } from 'react-native'

import Icon from '../../Icon'
import I18n from '../../../locale'

const styles = StyleSheet.create({
  circleContainer: {
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'center',
    padding: 5,
    paddingTop: 7,
    paddingBottom: 7,
  },
  circle: {
    textShadowOffset: { width: 0.1, height: 0.1 },
    textShadowRadius: Platform.OS === 'android' ? 10 : 4,
  },
  circleTop: {
    color: '#fff',
    textShadowColor: 'gray',
  },
  circleMiddle: {
    color: '#ffc300',
    textShadowColor: '#ffc300',
  },
  circleBottom: {
    color: '#40b322',
    textShadowColor: '#40b322',
  },
})

const Examination = ({ message, value }) =>
  <TouchableOpacity
    style={styles.circleContainer}
    onPress={() => {
      Alert.alert(I18n.t('examinerOpinion'), message, [{ text: I18n.t('close') }], {
        cancelable: true,
      })
    }}
    hitSlop={{ top: 15, bottom: 15, left: 15, right: 15 }}
  >
    <View>
      {value === 'top'
        ? <Icon style={[styles.circle, styles.circleTop]} name="lightbulb" size={20} />
        : undefined}
      {value === 'middle'
        ? <Icon style={[styles.circle, styles.circleMiddle]} name="lightbulb" size={20} />
        : undefined}
      {value === 'bottom'
        ? <Icon style={[styles.circle, styles.circleBottom]} name="lightbulb" size={20} />
        : undefined}
    </View>
  </TouchableOpacity>

export default Examination
