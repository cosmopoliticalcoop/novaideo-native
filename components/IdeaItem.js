/* eslint-disable react/no-array-index-key, no-undef */
import React from 'react'
import { View, Image, Text, TouchableOpacity, StyleSheet, Linking, BackHandler } from 'react-native'
import Moment from 'moment'
import Hyperlink from 'react-native-hyperlink'
import { connect } from 'react-redux'
import { gql, graphql } from 'react-apollo'
import update from 'immutability-helper'

import Avatar from './Avatar'
import ImagesPreview from './ImagesPreview'
import Keywords from './Keywords'
import IconWithText from './IconWithText'
import Evaluation from './Evaluation'
import { searchEntities } from '../actions'
import { getActions } from '../utils'
import I18n from '../locale'
import * as constants from '../constants'
import { actionFragment } from '../queries'

const styles = StyleSheet.create({
  ideaItem: {
    marginBottom: 10,
    marginTop: 5,
  },
  header: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
    padding: 5,
    paddingLeft: 10,
  },
  headerTitle: {
    fontSize: 13,
    color: '#999999ff',
    justifyContent: 'space-around',
    paddingLeft: 10,
  },
  headerAddOn: {
    fontSize: 10,
    color: '#999999ff',
    paddingLeft: 5,
  },
  body: {
    flex: 1,
    flexDirection: 'row',
    marginTop: -5,
  },
  bodyTitle: {
    width: '90%',
  },
  bodyLeft: {
    width: 60,
    justifyContent: 'center',
    alignItems: 'center',
    paddingTop: 10,
    paddingBottom: 10,
  },
  bodyContent: {
    width: '85%',
    justifyContent: 'space-between',
  },
  contentText: {
    width: '95%',
  },
  bodyFooter: {
    flexDirection: 'row',
    marginTop: 15,
  },
  actionsText: {
    fontSize: 14,
    color: '#585858',
    marginLeft: 8,
    marginRight: 50,
  },
  actionsIcon: {
    marginTop: 1,
  },
  sliderHeader: {
    marginLeft: 4,
    fontSize: 17,
    color: 'white',
    width: '90%',
  },
})

const evaluationActions = {
  support: {
    nodeId: 'support',
    description: I18n.t('supportTheIdea'),
  },
  oppose: {
    nodeId: 'oppose',
    description: I18n.t('opposeTheIdea'),
  },
  withdrawToken: {
    nodeId: 'withdraw_token',
    description: I18n.t('withdrawTokenIdea'),
  },
}

export class DumbIdeaItem extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      opened: false,
      actionOpened: false,
    }
    this.onPress = this.onPress.bind(this)
    this.onUserPress = this.onUserPress.bind(this)
    this.onClose = this.onClose.bind(this)
    this.getExaminationValue = this.getExaminationValue.bind(this)
    this.evaluationPress = this.evaluationPress.bind(this)
    this.onActionClose = this.onActionClose.bind(this)
    this.onActionPress = this.onActionPress.bind(this)
    this.performAction = this.performAction.bind(this)
    this.onActionLongPress = this.onActionLongPress.bind(this)
    this.getEvaluationActions = this.getEvaluationActions.bind(this)
  }

  componentDidMount() {
    this.subs = BackHandler.addEventListener('backPress', () =>
      this.setState(state => (state.opened ? { opened: false } : null)),
    )
  }

  componentWillReceiveProps(nextProps) {
    if (__DEV__) {
      Object.keys(nextProps).forEach((key) => {
        if (nextProps[key] !== this.props[key]) {
          console.log(nextProps.node.id, 'prop changed', key) // eslint-disable-line
        }
      })
    }
  }

  componentWillUnmount() {
    if (this.subs) this.subs.remove()
  }

  onPress() {
    if (!this.state.opened) {
      this.setState({ opened: true })
      return this.props.globalProps.navigation.navigate('Idea', {
        ideaId: this.props.node.id,
        instance: this.props.instance,
        adapters: this.props.adapters,
        onClose: this.onClose,
        onActionLongPress: this.onActionLongPress,
        searchEntities: this.props.searchEntities,
        getEvaluationActions: this.getEvaluationActions,
        getExaminationValue: this.getExaminationValue,
        evaluationPress: this.evaluationPress,
        performAction: this.performAction,
        theme: this.props.theme,
      })
    }
    return undefined
  }

  onUserPress() {
    const user = this.props.node.author
    if (!this.state.opened && !user.isAnonymous) {
      this.setState({ opened: true })
      this.props.globalProps.navigation.navigate('User', {
        user,
        onClose: this.onClose,
        theme: this.props.theme,
      })
    }
  }

  onClose() {
    if (this.state.opened) {
      this.setState({ opened: false })
    }
  }

  onActionClose() {
    if (this.state.actionOpened) {
      this.setState({ actionOpened: false })
    }
  }

  onActionLongPress(action) {
    if (action && action.description) this.props.globalProps.showMessage(action.description)
  }

  onActionPress(action) {
    this.performAction(action, this.props.node, this)
  }

  getExaminationValue() {
    const { node } = this.props
    if (node.state.includes('favorable')) return 'bottom'
    if (node.state.includes('to_study')) return 'middle'
    if (node.state.includes('unfavorable')) return 'top'
    return undefined
  }

  getEvaluationActions() {
    const { node } = this.props
    const withdraw = evaluationActions.withdrawToken
    const support = node.userToken === 'support' ? withdraw : evaluationActions.support
    const oppose = node.userToken === 'oppose' ? withdraw : evaluationActions.oppose
    const result = {
      top: support,
      down: oppose,
    }
    return result
  }

  evaluationPress(action) {
    const { node, network, globalProps } = this.props
    if (!network.isLogged) {
      globalProps.showMessage(I18n.t('LogInToPerformThisAction'))
    } else if (!action) {
      globalProps.showMessage(I18n.t('AuthorizationFailed'))
    } else {
      switch (action.nodeId) {
        case 'withdraw_token':
          this.props
            .withdraw({
              context: node.oid,
            })
            .catch(globalProps.showError)
          break
        case 'support':
          if (globalProps.account.availableTokens || node.userToken === 'oppose') {
            this.props
              .support({
                context: node.oid,
              })
              .catch(globalProps.showError)
          } else {
            globalProps.showMessage(I18n.t('AuthorizationFailed'))
          }
          break
        case 'oppose':
          if (globalProps.account.availableTokens || node.userToken === 'support') {
            this.props
              .oppose({
                context: node.oid,
              })
              .catch(globalProps.showError)
          } else {
            globalProps.showMessage(I18n.t('AuthorizationFailed'))
          }
          break
        default:
          globalProps.showMessage(I18n.t('comingSoon'))
      }
    }
  }

  performAction(action, idea, source) {
    const { network, select, deselect, theme, globalProps } = this.props
    if (action.nodeId === 'comment') {
      if (!source.state.actionOpened) {
        source.setState({ actionOpened: true })
        this.props.globalProps.navigation.navigate('Comments', {
          id: idea.channel.id,
          title: idea.title,
          onClose: source.onActionClose,
          theme,
          action,
        })
      }
    } else if (!network.isLogged) {
      globalProps.showMessage(I18n.t('LogInToPerformThisAction'))
    } else {
      switch (action.behaviorId) {
        case 'select':
          select({ context: idea.oid }).catch(globalProps.showError)
          break
        case 'deselect':
          deselect({ context: idea.oid }).catch(globalProps.showError)
          break
        default:
          globalProps.showMessage(I18n.t('comingSoon'))
      }
    }
  }

  render() {
    const { node, instance, adapters, globalProps } = this.props
    const author = node.author
    const authorPicture = author.picture
    const defaultPicture = author.isAnonymous
      ? constants.DEFAULT_ANONYMOUS_IMG
      : constants.DEFAULT_USER_IMG
    const createdAt = Moment(node.createdAt).format(I18n.t('datetimeFormat'))
    const images = node.attachedFiles ? node.attachedFiles.filter(image => image.isImage) : []
    const Examination = adapters.examination
    const communicationActions = getActions(node.actions, constants.ACTIONS.communicationAction)
    if (__DEV__) {
      console.log(node.id, 'render') // eslint-disable-line
    }
    return (
      <TouchableOpacity onPress={this.onPress} style={styles.ideaItem}>
        <TouchableOpacity onPress={this.onUserPress} style={styles.header}>
          <Avatar
            size={40}
            image={
              <Image
                source={authorPicture ? { uri: `${authorPicture.url}/profil` } : defaultPicture}
              />
            }
          />
          <Text style={styles.headerTitle}>
            {author.title}
          </Text>
          <Text style={styles.headerAddOn}>
            {createdAt}
          </Text>
        </TouchableOpacity>
        <View style={styles.body}>
          <View style={styles.bodyLeft}>
            {instance.support_ideas && node.state.includes('published')
              ? <Evaluation
                icon={{
                  top:
                      node.userToken === 'support'
                        ? 'arrow-up-drop-circle-outline'
                        : 'arrow-up-drop-circle',
                  down:
                      node.userToken === 'oppose'
                        ? 'arrow-down-drop-circle-outline'
                        : 'arrow-down-drop-circle',
                }}
                onPress={{ top: this.evaluationPress, down: this.evaluationPress }}
                onLongPress={{ top: this.onActionLongPress, down: this.onActionLongPress }}
                text={{ top: node.tokensSupport, down: node.tokensOpposition }}
                action={this.getEvaluationActions()}
                active={node.state.includes('submitted_support')}
              />
              : undefined}
            {instance.examine_ideas && node.state.includes('examined')
              ? <Examination message={node.opinion} value={this.getExaminationValue()} />
              : undefined}
          </View>
          <View style={styles.bodyContent}>
            <View>
              <View style={styles.bodyTitle}>
                <IconWithText name="lightbulb" text={node.title} iconSize={17} />
              </View>
              <Keywords onKeywordPress={this.props.searchEntities} keywords={node.keywords} />
              <Hyperlink
                onPress={url => Linking.openURL(url)}
                linkStyle={{ fontSize: 16, color: '#1990b8' }}
              >
                <Text style={styles.contentText}>
                  {node.presentationText}
                </Text>
              </Hyperlink>
              <ImagesPreview
                navigation={globalProps.navigation}
                images={images}
                sliderHeader={() =>
                  <IconWithText
                    iconColor={'white'}
                    styleText={styles.sliderHeader}
                    name="lightbulb"
                    text={node.title}
                    iconSize={17}
                    numberOfLines={1}
                  />}
              />
            </View>
            <View style={styles.bodyFooter}>
              {communicationActions.map((action, key) =>
                <TouchableOpacity
                  key={key}
                  onPress={() => this.onActionPress(action)}
                  onLongPress={() => this.onActionLongPress(action)}
                  hitSlop={{ top: 15, bottom: 15, left: 15, right: 15 }}
                >
                  <IconWithText
                    styleText={styles.actionsText}
                    styleIcon={styles.actionsIcon}
                    iconColor="#585858"
                    name={action.stylePicto}
                    text={action.counter}
                    iconSize={17}
                    numberOfLines={1}
                  />
                </TouchableOpacity>,
              )}
            </View>
          </View>
        </View>
      </TouchableOpacity>
    )
  }
}

const support = gql`
  mutation($context: String!) {
    supportIdea(context: $context) {
      status
      user {
        availableTokens
      }
      idea {
        id
        tokensSupport
        tokensOpposition
        userToken
        actions {
          ...action
        }
      }
    }
  }
    ${actionFragment}
`

const oppose = gql`
  mutation($context: String!) {
    opposeIdea(context: $context) {
      status
      user {
        availableTokens
      }
      idea {
        id
        tokensSupport
        tokensOpposition
        userToken
        actions{
          ...action
        }
      }
    }
  }
    ${actionFragment}
`

const withdraw = gql`
  mutation($context: String!) {
    withdrawTokenIdea(context: $context) {
      status
      user {
        availableTokens
      }
      idea {
        id
        tokensSupport
        tokensOpposition
        userToken
        actions {
          ...action
        }
      }
    }
  }
    ${actionFragment}
`

const select = gql`
  mutation($context: String!, $processId: String, $nodeIds: [String!]) {
    select(context: $context) {
      status
      idea {
        id
        oid
        actions(processId: $processId, nodeIds: $nodeIds) {
          ...action
        }
      }
    }
  }
  ${actionFragment}
`

const deselect = gql`
  mutation($context: String!, $processId: String, $nodeIds: [String!]) {
    deselect(context: $context) {
      status
      idea {
        id
        oid
        actions(processId: $processId, nodeIds: $nodeIds) {
          ...action
        }
      }
    }
  }
  ${actionFragment}
`

const DumbIdeaItemActions = graphql(support, {
  props({ ownProps, mutate }) {
    return {
      support({ context }) {
        const { tokensSupport, tokensOpposition, userToken } = ownProps.node
        return mutate({
          variables: { context },
          optimisticResponse: {
            __typename: 'Mutation',
            supportIdea: {
              __typename: 'SupportIdea',
              status: true,
              user: {
                __typename: 'Person',
                availableTokens: ownProps.globalProps.account.availableTokens - 1,
              },
              idea: {
                ...ownProps.node,
                tokensSupport: tokensSupport + 1,
                tokensOpposition: userToken === 'oppose' ? tokensOpposition - 1 : tokensOpposition,
                userToken: 'support',
              },
            },
          },
          updateQueries: {
            Account: (prev, { mutationResult }) =>
              update(prev, {
                account: {
                  availableTokens: { $set: mutationResult.data.supportIdea.user.availableTokens },
                },
              }),
            MySupports: (prev, { mutationResult }) => {
              const newIdea = mutationResult.data.supportIdea.idea
              const currentIdea = prev.account.supportedIdeas.edges.filter(
                item => item && item.node.id === newIdea.id,
              )[0]
              if (!currentIdea) {
                return update(prev, {
                  account: {
                    supportedIdeas: {
                      edges: {
                        $unshift: [
                          {
                            __typename: 'Idea',
                            node: { ...ownProps.node, ...newIdea },
                          },
                        ],
                      },
                    },
                  },
                })
              }
              const index = prev.account.supportedIdeas.edges.indexOf(currentIdea)
              return update(prev, {
                account: {
                  supportedIdeas: {
                    edges: {
                      $splice: [
                        [
                          index,
                          1,
                          {
                            __typename: 'Idea',
                            node: { ...ownProps.node, ...newIdea },
                          },
                        ],
                      ],
                    },
                  },
                },
              })
            },
          },
        })
      },
    }
  },
})(
  graphql(oppose, {
    props({ ownProps, mutate }) {
      return {
        oppose({ context }) {
          const { tokensSupport, tokensOpposition, userToken } = ownProps.node
          return mutate({
            variables: { context },
            optimisticResponse: {
              __typename: 'Mutation',
              opposeIdea: {
                __typename: 'OpposeIdea',
                status: true,
                user: {
                  __typename: 'Person',
                  availableTokens: ownProps.globalProps.account.availableTokens - 1,
                },
                idea: {
                  ...ownProps.node,
                  tokensOpposition: tokensOpposition + 1,
                  tokensSupport: userToken === 'support' ? tokensSupport - 1 : tokensSupport,
                  userToken: 'oppose',
                },
              },
            },
            updateQueries: {
              Account: (prev, { mutationResult }) =>
                update(prev, {
                  account: {
                    availableTokens: { $set: mutationResult.data.opposeIdea.user.availableTokens },
                  },
                }),
              MySupports: (prev, { mutationResult }) => {
                const newIdea = mutationResult.data.opposeIdea.idea
                const currentIdea = prev.account.supportedIdeas.edges.filter(
                  item => item && item.node.id === newIdea.id,
                )[0]
                if (!currentIdea) {
                  return update(prev, {
                    account: {
                      supportedIdeas: {
                        edges: {
                          $unshift: [
                            {
                              __typename: 'Idea',
                              node: { ...ownProps.node, ...newIdea },
                            },
                          ],
                        },
                      },
                    },
                  })
                }
                const index = prev.account.supportedIdeas.edges.indexOf(currentIdea)
                return update(prev, {
                  account: {
                    supportedIdeas: {
                      edges: {
                        $splice: [
                          [
                            index,
                            1,
                            {
                              __typename: 'Idea',
                              node: { ...ownProps.node, ...newIdea },
                            },
                          ],
                        ],
                      },
                    },
                  },
                })
              },
            },
          })
        },
      }
    },
  })(
    graphql(withdraw, {
      props({ ownProps, mutate }) {
        return {
          withdraw({ context }) {
            const { tokensSupport, tokensOpposition, userToken } = ownProps.node
            return mutate({
              variables: { context },
              optimisticResponse: {
                __typename: 'Mutation',
                withdrawTokenIdea: {
                  __typename: 'OpposeIdea',
                  status: true,
                  user: {
                    __typename: 'Person',
                    availableTokens: ownProps.globalProps.account.availableTokens + 1,
                  },
                  idea: {
                    ...ownProps.node,
                    tokensSupport: userToken === 'support' ? tokensSupport - 1 : tokensSupport,
                    tokensOpposition:
                      userToken === 'oppose' ? tokensOpposition - 1 : tokensOpposition,
                    userToken: 'withdraw',
                  },
                },
              },
              updateQueries: {
                Account: (prev, { mutationResult }) =>
                  update(prev, {
                    account: {
                      availableTokens: {
                        $set: mutationResult.data.withdrawTokenIdea.user.availableTokens,
                      },
                    },
                  }),
                MySupports: (prev, { mutationResult }) => {
                  const newIdea = mutationResult.data.withdrawTokenIdea.idea
                  const currentIdea = prev.account.supportedIdeas.edges.filter(
                    item => item && item.node.id === newIdea.id,
                  )[0]
                  const index = prev.account.supportedIdeas.edges.indexOf(currentIdea)
                  return update(prev, {
                    account: {
                      supportedIdeas: {
                        edges: {
                          $splice: [[index, 1]],
                        },
                      },
                    },
                  })
                },
                IdeasList: (prev, { mutationResult }) => {
                  const newIdea = mutationResult.data.withdrawTokenIdea.idea
                  const currentIdea = prev.ideas.edges.filter(
                    item => item && item.node.id === newIdea.id,
                  )[0]
                  if (!currentIdea) return prev
                  const index = prev.ideas.edges.indexOf(currentIdea)
                  return update(prev, {
                    ideas: {
                      edges: {
                        $splice: [
                          [
                            index,
                            1,
                            { __typename: 'Idea', node: { ...currentIdea.node, ...newIdea } },
                          ],
                        ],
                      },
                    },
                  })
                },
              },
            })
          },
        }
      },
    })(
      graphql(select, {
        props({ ownProps, mutate }) {
          return {
            select({ context }) {
              const selectAction = ownProps.node.actions.filter(
                item => item && item.behaviorId === 'select',
              )[0]
              const indexAction = ownProps.node.actions.indexOf(selectAction)
              const newAction = update(selectAction, {
                counter: { $set: selectAction.counter + 1 },
                nodeId: { $set: 'deselect' },
                behaviorId: { $set: 'deselect' },
                stylePicto: { $set: 'glyphicon glyphicon-star' },
              })
              const optimisticIdea = update(ownProps.node, {
                actions: {
                  $set: [newAction],
                },
              })
              return mutate({
                variables: { context, processId: 'novaideoabstractprocess', nodeIds: ['deselect'] },
                optimisticResponse: {
                  __typename: 'Mutation',
                  select: {
                    __typename: 'Select',
                    status: true,
                    idea: {
                      ...optimisticIdea,
                    },
                  },
                },
                updateQueries: {
                  MyFollowings: (prev, { mutationResult }) => {
                    const newActions = mutationResult.data.select.idea.actions
                    const newIdea = update(ownProps.node, {
                      actions: {
                        $splice: [[indexAction, 1, ...newActions]],
                      },
                    })
                    return update(prev, {
                      account: {
                        followedIdeas: {
                          edges: {
                            $unshift: [
                              {
                                __typename: 'Idea',
                                node: { ...newIdea },
                              },
                            ],
                          },
                        },
                      },
                    })
                  },
                },
              })
            },
          }
        },
      })(
        graphql(deselect, {
          props({ ownProps, mutate }) {
            return {
              deselect({ context }) {
                const deselectAction = ownProps.node.actions.filter(
                  item => item && item.behaviorId === 'deselect',
                )[0]
                const newAction = update(deselectAction, {
                  counter: { $set: deselectAction.counter - 1 },
                  nodeId: { $set: 'select' },
                  behaviorId: { $set: 'select' },
                  stylePicto: { $set: 'glyphicon glyphicon-star-empty' },
                })
                const optimisticIdea = update(ownProps.node, {
                  actions: {
                    $set: [newAction],
                  },
                })
                return mutate({
                  variables: { context, processId: 'novaideoabstractprocess', nodeIds: ['select'] },
                  optimisticResponse: {
                    __typename: 'Mutation',
                    deselect: {
                      __typename: 'Deselect',
                      status: true,
                      idea: {
                        ...optimisticIdea,
                      },
                    },
                  },
                  updateQueries: {
                    MyFollowings: (prev, { mutationResult }) => {
                      const newIdea = mutationResult.data.deselect.idea
                      const currentIdea = prev.account.followedIdeas.edges.filter(
                        item => item && item.node.id === newIdea.id,
                      )[0]
                      const index = prev.account.followedIdeas.edges.indexOf(currentIdea)
                      return update(prev, {
                        account: {
                          followedIdeas: {
                            edges: {
                              $splice: [[index, 1]],
                            },
                          },
                        },
                      })
                    },
                    IdeasList: (prev, { mutationResult }) => {
                      const indexAction = ownProps.node.actions.indexOf(deselectAction)
                      const idea = mutationResult.data.deselect.idea
                      const newActions = idea.actions
                      const currentIdea = prev.ideas.edges.filter(
                        item => item && item.node.id === idea.id,
                      )[0]
                      if (!currentIdea) return prev
                      const newIdea = update(currentIdea, {
                        node: {
                          actions: {
                            $splice: [[indexAction, 1, ...newActions]],
                          },
                        },
                      })
                      const index = prev.ideas.edges.indexOf(currentIdea)
                      return update(prev, {
                        ideas: {
                          edges: {
                            $splice: [[index, 1, newIdea]],
                          },
                        },
                      })
                    },
                  },
                })
              },
            }
          },
        })(DumbIdeaItem),
      ),
    ),
  ),
)

export const mapDispatchToProps = { searchEntities }

export const mapStateToProps = state => ({
  globalProps: state.globalProps,
  network: state.network,
  instance: state.instance,
  adapters: state.adapters,
})

export default connect(mapStateToProps, mapDispatchToProps)(DumbIdeaItemActions)
