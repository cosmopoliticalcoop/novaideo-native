import React from 'react'
import { graphql } from 'react-apollo'
import { connect } from 'react-redux'

import Icon from './Icon'
import IdeaItem from './IdeaItem'
import EntitiesList from './EntitiesList'
import I18n from '../locale'
import { myContentsQuery } from '../queries'

export class DumbMyContents extends React.Component {
  static navigationOptions = {
    tabBarIcon: ({ tintColor }) => <Icon size={18} name="inbox" color={tintColor} />,
  }

  render() {
    const { data } = this.props
    return (
      <EntitiesList
        data={data}
        getEntities={entities => (entities.account ? entities.account.contents : undefined)}
        noContentIcon="inbox"
        noContentMessage={I18n.t('noContent')}
        noContentFoundMessage={I18n.t('noContentFound')}
        offlineFilter={(entity, text) =>
          entity.node.title.toLowerCase().search(text) >= 0 ||
          entity.node.text.toLowerCase().search(text) >= 0 ||
          entity.node.keywords.join(' ').toLowerCase().search(text) >= 0}
        ListItem={IdeaItem}
      />
    )
  }
}

const MyContentsGQL = graphql(myContentsQuery, {
  options: props => ({
    notifyOnNetworkStatusChange: true,
    variables: { first: 15, after: '', filter: props.filter },
  }),
})(DumbMyContents)

export const mapStateToProps = state => ({
  filter: state.search.text,
})

export default connect(mapStateToProps)(MyContentsGQL)
