import React from 'react'
import renderer from 'react-test-renderer'

import { DumbChannelItem } from './ChannelItem'

describe('ChannelItem component', () => {
  it('should match public ChannelItem snapshot', () => {
    const channel = {
      title: 'Foobar',
      unreadComments: [],
      subject: {},
    }
    const props = {
      node: channel,
    }
    const rendered = renderer.create(<DumbChannelItem {...props} />).toJSON()
    expect(rendered).toMatchSnapshot()
  })

  it('should match private ChannelItem snapshot', () => {
    const channel = {
      title: 'Foobar',
      unreadComments: [],
      subject: {
        picture: {
          url: 'http//example.nova-ideo.com/principals/users/JohnDoe/avatar.jpg',
        },
      },
    }
    const props = {
      node: channel,
      itemdata: { isDiscussion: true },
    }
    const rendered = renderer.create(<DumbChannelItem {...props} />).toJSON()
    expect(rendered).toMatchSnapshot()
  })
})
