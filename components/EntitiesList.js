/* eslint-disable no-undef */
import React from 'react'
import { FlatList, ActivityIndicator, StyleSheet, View, Animated, Text } from 'react-native'
import { connect } from 'react-redux'
import { withTheme } from 'glamorous-native'

import { setURLState, searchEntities } from '../actions'
import Icon from './Icon'
import SeparatorComponent from './SeparatorComponent'

const AnimatedFlatList = Animated.createAnimatedComponent(FlatList)

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  loading: {
    flex: 1,
    justifyContent: 'flex-start',
    alignItems: 'center',
    paddingTop: 10,
  },
  header: { marginTop: 15 },
  footer: {
    width: '100%',
  },
  noContent: {
    alignItems: 'center',
    padding: 30,
    paddingTop: '25%',
  },
  noContentText: {
    color: 'gray',
    marginLeft: 4,
    fontSize: 20,
    textAlign: 'center',
  },
})

export class DumbEntitiesList extends React.Component {
  constructor(props) {
    super(props)
    this.offline = {
      entities: undefined,
      status: false,
    }
    this.onEndReached = this.onEndReached.bind(this)
    this.onRefresh = this.onRefresh.bind(this)
    this.setOfflineData = this.setOfflineData.bind(this)
  }

  componentWillUpdate(nextProps) {
    const { data, filter, network, getEntities, offlineFilter } = nextProps
    let entities
    let offlineStatus = false
    try {
      if (data.error || !network.isConnected || network.url.error) {
        if (__DEV__) {
          if (data.error) {
            console.error(data.error) // eslint-disable-line
          }
        }
        const dataEntities = getEntities(data)
        offlineStatus = true
        if (!filter) {
          entities = dataEntities.edges
        } else {
          const words = filter.toLowerCase().split(' ').filter(item => item)
          entities = dataEntities.edges.filter(entity =>
            words.map(item => offlineFilter(entity, item)).includes(true),
          )
        }
      }
      this.setOfflineData(entities, offlineStatus)
    } catch (e) {
      this.setOfflineData(entities, offlineStatus, true, network)
    }
  }

  onEndReached() {
    // The fetchMore method is used to load new data and add it
    // to the original query we used to populate the list
    const { data, network, getEntities } = this.props
    // If no request is in flight for this query, and no errors happened. Everything is OK.
    if (data.networkStatus === 7) {
      data
        .fetchMore({
          variables: { after: getEntities(data).pageInfo.endCursor || '' },
          updateQuery: (previousResult, { fetchMoreResult }) => {
            // Don't do anything if there weren't any new items
            const previousResultEntities = getEntities(previousResult)
            if (!fetchMoreResult || !previousResultEntities.pageInfo.hasNextPage) {
              return previousResult
            }
            const fetchMoreResultEntities = getEntities(fetchMoreResult)
            fetchMoreResultEntities.edges = previousResultEntities.edges.concat(
              fetchMoreResultEntities.edges,
            )
            return fetchMoreResult
          },
        })
        .then(() => {
          if (network.url.error) this.props.setURLState(false, [])
        })
        .catch((e) => {
          if (!network.url.error) this.props.setURLState(true, [e.message])
        })
    }
  }

  onRefresh() {
    const { data, network } = this.props
    data
      .refetch()
      .then(() => {
        if (network.url.error) this.props.setURLState(false, [])
      })
      .catch((e) => {
        if (!network.url.error) this.props.setURLState(true, [e.message])
      })
  }

  setOfflineData(entities, status, hasError, network) {
    this.offline = { entities, status }
    if (hasError && !network.url.error) {
      this.props.setURLState(true, [])
    }
  }

  render() {
    const {
      data,
      filter,
      getEntities,
      ListItem,
      noContentMessage,
      noContentFoundMessage,
      noContentIcon,
      withoutSeparator,
      listHeader,
      itemdata,
      style,
      theme,
      activityIndicatorColor,
      ...listProps
    } = this.props
    if (data.error) {
      // the fact of checking data.error remove the Unhandled (in react-apollo)
      // ApolloError error when the graphql server is down
      // Do nothing
    }
    const dataEntities = getEntities(data)
    if (dataEntities == null || data.networkStatus === 1 || data.networkStatus === 2) {
      return (
        <View style={styles.loading}>
          <ActivityIndicator
            size="large"
            animating
            color={activityIndicatorColor || theme.primary.bgColor}
          />
        </View>
      )
    }
    const offline = this.offline
    const entities = offline.status ? offline.entities : dataEntities.edges
    return (
      <View style={styles.container}>
        {entities && entities.length > 0
          ? <AnimatedFlatList
            ListHeaderComponent={listHeader || (() => <View style={styles.header} />)}
            ItemSeparatorComponent={withoutSeparator ? null : SeparatorComponent}
            data={entities}
            disableVirtualization={false}
            shouldItemUpdate={(props, nextProps) => props.item.node.id !== nextProps.item.node.id}
            refreshing={data.networkStatus === 4}
            onRefresh={this.onRefresh}
            onEndReachedThreshold={0.6}
            onEndReached={this.onEndReached}
            keyExtractor={item => item.node.id}
            renderItem={({ item }) =>
              <ListItem itemdata={itemdata} node={item.node} theme={theme} />}
            ListFooterComponent={() =>
              <View style={styles.footer}>
                {data.networkStatus === 3 && dataEntities.pageInfo.hasNextPage
                    ? <ActivityIndicator
                      size={30}
                      animating
                      color={activityIndicatorColor || theme.primary.bgColor}
                    />
                    : undefined}
              </View>}
            {...listProps}
          />
          : <View style={styles.noContent}>
            <Icon name={noContentIcon} size={20} color={style.noContentIconColor || 'gray'} />
            <Text style={[styles.noContentText, style.noContentText || {}]}>
              {filter && noContentFoundMessage ? noContentFoundMessage : noContentMessage}
            </Text>
          </View>}
      </View>
    )
  }
}

DumbEntitiesList.defaultProps = {
  style: {},
}

export const mapStateToProps = state => ({
  network: state.network,
  filter: state.search.text,
})

export const mapDispatchToProps = {
  setURLState,
  searchEntities,
}

const ThemedDumbEntitiesList = withTheme(DumbEntitiesList)
export default connect(mapStateToProps, mapDispatchToProps)(ThemedDumbEntitiesList)
