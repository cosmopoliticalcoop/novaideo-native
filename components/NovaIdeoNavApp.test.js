import React from 'react'
import ShallowRenderer from 'react-test-renderer/shallow'

import { DumbNovaIdeoNavApp } from './NovaIdeoNavApp'
import getTheme from '../theme'

describe('Nova-Ideo native app', () => {
  it('should match snapshot', () => {
    const props = {
      history: {},
      instance: { id: 'foobar' },
      adapters: {
        theme: getTheme,
      },
    }
    const renderer = new ShallowRenderer()
    renderer.render(<DumbNovaIdeoNavApp {...props} />)
    const rendered = renderer.getRenderOutput()
    expect(rendered).toMatchSnapshot()
  })
})
