import React from 'react'
import ShallowRenderer from 'react-test-renderer/shallow'

import Channels from './Channels'

describe('Channels component', () => {
  it('should match Channels with data snapshot', () => {
    const props = {
      navigation: {},
    }
    const renderer = new ShallowRenderer()
    renderer.render(<Channels {...props} />)
    const rendered = renderer.getRenderOutput()
    expect(rendered).toMatchSnapshot()
  })
})
