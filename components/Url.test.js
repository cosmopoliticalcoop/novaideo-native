import React from 'react'
import renderer from 'react-test-renderer'

import Url from './Url'

describe('Url component', () => {
  it('should match Url with data snapshot', () => {
    const props = {
      url: 'https://foobar.com',
      title: 'Foobar title',
      description: 'Foobar description',
      imageUrl: 'https://foobar.com/image',
      siteName: 'foobar',
      favicon: 'https://foobar.com/favicon',
      authorAvatar: null,
      authorName: null,
    }
    const rendered = renderer.create(<Url data={props} />).toJSON()
    expect(rendered).toMatchSnapshot()
  })
  it('should match Url without author snapshot', () => {
    const props = {
      url: 'https://foobar.com',
      title: 'Foobar title',
      description: 'Foobar description',
      imageUrl: 'https://foobar.com/image',
      siteName: 'foobar',
      favicon: 'https://foobar.com/favicon',
      authorAvatar: 'https://foobar.com/author',
      authorName: 'Alice',
    }
    const rendered = renderer.create(<Url data={props} />).toJSON()
    expect(rendered).toMatchSnapshot()
  })
})
