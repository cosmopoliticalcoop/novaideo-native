import React from 'react'
import renderer from 'react-test-renderer'

import Keywords from './Keywords'

describe('Keywords component', () => {
  it('should match Keywords snapshot', () => {
    const rendered = renderer.create(<Keywords keywords={['foo', 'bar']} />).toJSON()
    expect(rendered).toMatchSnapshot()
  })
})
