/* eslint-disable jsx-boolean-value */
import React from 'react'
import { BackHandler } from 'react-native'
import { connect } from 'react-redux'
import { gql, graphql } from 'react-apollo'

import ThemedActionButton from './ThemedActionButton'
import { logout, initInstance, setURLState, initLogin } from '../actions'
import I18n from '../locale'

export class DumbCreateIdeaButton extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      opened: false,
    }
    this.onClose = this.onClose.bind(this)
    this.onPress = this.onPress.bind(this)
  }

  componentDidMount() {
    this.subs = BackHandler.addEventListener('backPress', () =>
      this.setState(state => (state.opened ? { opened: false } : null)),
    )
  }

  componentWillUnmount() {
    if (this.subs) this.subs.remove()
  }

  onClose() {
    if (this.state.opened) {
      this.setState({ opened: false })
    }
  }

  onPress() {
    const { data, rootProps, globalProps: { navigation } } = this.props
    if (!this.state.opened) {
      this.setState({ opened: true }, () =>
        navigation.navigate('CreateIdea', {
          title: I18n.t('createAnIdea'),
          onClose: this.onClose,
          actions: data.actions.edges.map(node => node.node),
          rootProps,
        }),
      )
    }
  }

  render() {
    const { data } = this.props
    if (data.error || data.actions == null || data.actions.edges.length === 0 || data.loading) {
      return null
    }
    return <ThemedActionButton iconName="lightbulb" onPress={this.onPress} />
  }
}

const actionsQuery = gql`
  query($processId: String, $nodeIds: [String], $context: String) {
    actions(processId: $processId, nodeIds: $nodeIds, context: $context) {
      edges {
        node {
          processId
          nodeId
          title
          submissionTitle
        }
      }
    }
  }
`

const CreateIdeaButton = graphql(actionsQuery, {
  options: () => ({
    notifyOnNetworkStatusChange: true,
    variables: {
      processId: 'ideamanagement',
      nodeIds: ['creatandpublish', 'creat'], // TODO
      context: '',
    },
  }),
})(DumbCreateIdeaButton)

export const mapDispatchToProps = {
  logout,
  initLogin,
  initInstance,
  setURLState,
}

export const mapStateToProps = state => ({
  globalProps: state.globalProps,
})

export default connect(mapStateToProps, mapDispatchToProps)(CreateIdeaButton)
