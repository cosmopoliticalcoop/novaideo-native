import React from 'react'
import ShallowRenderer from 'react-test-renderer/shallow'

import { DumbComment } from './Comment'

describe('Comment component', () => {
  it('should match Comment with data snapshot', () => {
    const comment = {
      oid: 'foo',
      comments: { edges: [] },
      actions: [{ nodeId: 'respond' }],
    }
    const props = {
      navigation: { state: { params: { comment } } },
      data: { node: { ...comment } },
    }
    const renderer = new ShallowRenderer()
    renderer.render(<DumbComment {...props} />)
    const rendered = renderer.getRenderOutput()
    expect(rendered).toMatchSnapshot()
  })
  it('should match Comment snapshot (without respond action)', () => {
    const comment = {
      oid: 'foo',
      comments: { edges: [] },
      actions: [],
    }
    const props = {
      navigation: { state: { params: { comment } } },
      data: { node: { ...comment } },
    }
    const renderer = new ShallowRenderer()
    renderer.render(<DumbComment {...props} />)
    const rendered = renderer.getRenderOutput()
    expect(rendered).toMatchSnapshot()
  })
  it('should match Comment snapshot (offline)', () => {
    const comment = {
      oid: 'foo',
      comments: { edges: [] },
      actions: [],
    }
    const props = {
      navigation: { state: { params: { comment } } },
      data: { node: { oid: 'bar', comments: { edges: [] }, actions: [] } },
    }
    const renderer = new ShallowRenderer()
    renderer.render(<DumbComment {...props} />)
    const rendered = renderer.getRenderOutput()
    expect(rendered).toMatchSnapshot()
  })
})
