import React from 'react'
import ShallowRenderer from 'react-test-renderer/shallow'

import { DumbInstanceSwitch } from './InstanceSwitch'

describe('DumbInstanceSwitch component', () => {
  it('should match DumbInstanceSwitch with data snapshot', () => {
    const props = {
      network: { isConnected: true, url: { error: false } },
      history: {},
      instance: { id: 'foobar' },
    }
    const renderer = new ShallowRenderer()
    renderer.render(<DumbInstanceSwitch {...props} />)
    const rendered = renderer.getRenderOutput()
    expect(rendered).toMatchSnapshot()
  })
})
