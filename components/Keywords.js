import React from 'react'
import { View, Text, TouchableOpacity, StyleSheet } from 'react-native'

import Icon from './Icon'

const styles = StyleSheet.create({
  icon: {
    marginTop: 2,
  },
  keywordsContainer: {
    flexDirection: 'row',
    flexWrap: 'wrap',
    paddingLeft: 15,
    paddingRight: 5,
    marginBottom: 5,
  },
  keywordsText: {
    color: '#666666ff',
    marginLeft: 4,
    fontSize: 12,
  },
})

const Keywords = ({ keywords, onKeywordPress, goBackOnPress, navigation, closeParent }) => {
  const KeywordPressHandler = (k) => {
    if (goBackOnPress && navigation && navigation.state) {
      closeParent()
      navigation.goBack(null)
    }
    onKeywordPress(k)
  }

  return (
    <View style={styles.keywordsContainer}>
      <Icon style={styles.icon} name="tag-multiple" size={12} color={'#666666ff'} />
      {keywords.map((k, index) =>
        <TouchableOpacity
          hitSlop={{ top: 15, bottom: 15, left: 15, right: 15 }}
          key={k}
          title={k}
          onPress={() => KeywordPressHandler(k)}
        >
          <Text style={styles.keywordsText}>
            {k}
            {index + 1 !== keywords.length ? ',' : ''}
          </Text>
        </TouchableOpacity>,
      )}
    </View>
  )
}

export default Keywords
