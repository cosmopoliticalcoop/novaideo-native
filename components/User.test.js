import React from 'react'
import ShallowRenderer from 'react-test-renderer/shallow'

import { theme } from '../theme'
import { DumbUser } from './User'

describe('PublicChannels component', () => {
  it('should match PublicChannels with data snapshot', () => {
    const props = {
      data: {},
      navigation: {
        state: {
          params: {
            theme,
            user: {
              picture: {
                url: 'http//example.nova-ideo.com/principals/users/JohnDoe/avatar.jpg',
              },
              title: 'John Doe',
              description: 'John Doe description',
            },
          },
        },
      },
    }
    const renderer = new ShallowRenderer()
    renderer.render(<DumbUser {...props} />)
    const rendered = renderer.getRenderOutput()
    expect(rendered).toMatchSnapshot()
  })
})
