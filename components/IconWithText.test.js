import React from 'react'
import renderer from 'react-test-renderer'

import IconWithText from './IconWithText'

describe('IconWithText component', () => {
  it('should match IconWithText snapshot', () => {
    const props = {
      name: 'lightbulb',
      text: 'text',
    }
    const rendered = renderer.create(<IconWithText {...props} />).toJSON()
    expect(rendered).toMatchSnapshot()
  })
})
