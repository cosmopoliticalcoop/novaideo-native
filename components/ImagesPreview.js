/* eslint-disable react/no-array-index-key */
import React from 'react'
import { View, Image, StyleSheet, TouchableOpacity, Text, BackHandler } from 'react-native'

const styles = StyleSheet.create({
  imagesContainer: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'center',
    marginTop: 10,
    paddingRight: 10,
  },
  itemsContainer: {
    flexDirection: 'row',
    width: '100%',
    maxWidth: 400,
  },
  images: {
    borderRadius: 3,
    backgroundColor: 'black',
    width: '100%',
  },
  firstItem: {
    width: '100%',
    height: 200,
    borderRadius: 3,
    borderWidth: 0.5,
    borderColor: '#d6d7da',
  },
  otherItem: {
    width: '100%',
    height: 50,
    borderRadius: 3,
    marginTop: 2,
    borderWidth: 0.5,
    borderColor: '#d6d7da',
  },
  otherItemsContainer: {
    width: '19%',
  },
  globalItemContainer: {
    marginRight: 3,
    width: '100%',
  },
  firstItemContainer: {
    marginRight: 3,
    width: '80%',
  },
  plusItemContainer: {
    width: '100%',
    height: 50,
    borderRadius: 3,
    borderWidth: 0.5,
    borderColor: '#d6d7da',
    backgroundColor: '#d8d8d8',
    marginTop: 2,
    alignItems: 'center',
    justifyContent: 'center',
  },
  plusItem: {
    fontSize: 24,
    fontWeight: 'bold',
    color: '#929292',
  },
})

export default class ImagesPreview extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      opened: false,
    }
    this.onPress = this.onPress.bind(this)
    this.onClose = this.onClose.bind(this)
  }

  componentDidMount() {
    this.subs = BackHandler.addEventListener('backPress', () =>
      this.setState(state => (state.opened ? { opened: false } : null)),
    )
  }

  componentWillUnmount() {
    if (this.subs) this.subs.remove()
  }

  onPress() {
    if (!this.state.opened) {
      this.setState({ opened: true })
      return this.props.navigation.navigate('ImagesSlider', {
        images: this.props.images,
        sliderHeader: this.props.sliderHeader,
        onClose: this.onClose,
      })
    }
    return undefined
  }
  onClose() {
    if (this.state.opened) {
      this.setState({ opened: false })
    }
  }
  render() {
    const { images } = this.props
    if (images.length === 0) return <View />
    const firstImage = images[0]
    const otherImagesToPreview = 3
    const limit = images.length >= otherImagesToPreview ? otherImagesToPreview : images.length
    const otherImages = images.length > 1 ? images.slice(1, limit) : []
    const nbHiddenImages = limit === otherImagesToPreview ? images.length - otherImagesToPreview : 0
    return (
      <TouchableOpacity onPress={this.onPress} style={styles.imagesContainer}>
        <View style={styles.itemsContainer}>
          <View
            style={otherImages.length > 0 ? styles.firstItemContainer : styles.globalItemContainer}
          >
            <Image source={{ uri: `${firstImage.url}/big` }} style={styles.firstItem} />
          </View>
          <View style={styles.otherItemsContainer}>
            {otherImages.map((image, key) =>
              <Image key={key} source={{ uri: `${image.url}/small` }} style={styles.otherItem} />,
            )}
            {nbHiddenImages
              ? <View style={styles.plusItemContainer}>
                <Text style={styles.plusItem}>{`+${nbHiddenImages}`}</Text>
              </View>
              : undefined}
          </View>
        </View>
      </TouchableOpacity>
    )
  }
}
