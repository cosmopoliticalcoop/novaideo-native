import React from 'react'
import { graphql } from 'react-apollo'
import { connect } from 'react-redux'

import Icon from './Icon'
import IdeaItem from './IdeaItem'
import EntitiesList from './EntitiesList'
import I18n from '../locale'
import { mySupportsQuery } from '../queries'

export class DumbMySupports extends React.Component {
  static navigationOptions = {
    tabBarIcon: ({ tintColor }) =>
      <Icon size={18} name="checkbox-blank-circle" color={tintColor} />,
  }

  render() {
    const { data } = this.props
    return (
      <EntitiesList
        data={data}
        getEntities={entities => (entities.account ? entities.account.supportedIdeas : undefined)}
        noContentIcon="checkbox-blank-circle"
        noContentMessage={I18n.t('noEvaluation')}
        noContentFoundMessage={I18n.t('noEvaluationFound')}
        offlineFilter={(entity, text) =>
          entity.node.title.toLowerCase().search(text) >= 0 ||
          entity.node.text.toLowerCase().search(text) >= 0 ||
          entity.node.keywords.join(' ').toLowerCase().search(text) >= 0}
        ListItem={IdeaItem}
      />
    )
  }
}

const MySupportsGQL = graphql(mySupportsQuery, {
  options: props => ({
    notifyOnNetworkStatusChange: true,
    variables: { first: 15, after: '', filter: props.filter },
  }),
})(DumbMySupports)

export const mapStateToProps = state => ({
  filter: state.search.text,
})

export default connect(mapStateToProps)(MySupportsGQL)
