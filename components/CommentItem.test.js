import React from 'react'
import renderer from 'react-test-renderer'

import { DumbCommentItem } from './CommentItem'

describe('CommentItem component', () => {
  it('should match public CommentItem snapshot', () => {
    const channel = {
      title: 'Foobar',
      unreadComments: [],
      subject: {},
    }
    const comment = {
      id: 'commentid',
      oid: 'commentoid',
      text: 'Comment text',
      createdAt: '2017-01-01T12:30:59',
      actions: [],
      urls: [],
      lenComments: 3,
      author: {
        picture: {
          url: 'http//example.nova-ideo.com/principals/users/JohnDoe/avatar.jpg',
        },
        title: 'John Doe',
      },
    }
    const props = {
      node: comment,
      itemdata: { channel },
      globalProps: {},
    }
    const rendered = renderer.create(<DumbCommentItem {...props} />).toJSON()
    expect(rendered).toMatchSnapshot()
  })
})
