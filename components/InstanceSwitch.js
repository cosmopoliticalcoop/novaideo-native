/* eslint-disable react/no-array-index-key, no-undef */
import React from 'react'
import {
  StyleSheet,
  View,
  KeyboardAvoidingView,
  Text,
  Image,
  TouchableOpacity,
  ScrollView,
} from 'react-native'
import { connect } from 'react-redux'

import ThemedButton from './ThemedButton'
import TextField from './TextField'
import { setInstance, initSelection, initInstance, userLogin } from '../actions'
import NovaIdeoLogo from '../novaideo_logo.png'
import LoadingState from './LoadingState'
import * as constants from '../constants'
import I18n from '../locale'

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fafafaff',
    alignItems: 'center',
  },
  instancesContainer: {
    backgroundColor: 'transparent',
    flexWrap: 'wrap',
    flexDirection: 'row',
  },
  instance: {
    backgroundColor: 'white',
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 3,
    elevation: 3,
    shadowColor: 'gray',
    shadowOpacity: 0.8,
    shadowRadius: 2,
    shadowOffset: {
      height: 1,
      width: 0,
    },
    width: 100,
    height: 100,
    margin: 10,
    marginBottom: 5,
  },
  submitView: {
    padding: 10,
  },
  loadingState: {
    alignItems: 'center',
  },
  fieldDescription: {
    fontSize: 12,
    color: '#bbb',
    opacity: 0.6,
  },
  instanceTitle: {
    color: 'gray',
    fontSize: 16,
  },
  instanceImage: {
    margin: 5,
    width: 50,
    height: 50,
  },
})

export class DumbInstanceSwitch extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      instanceId: '',
    }
    this.onChangeText = this.onChangeText.bind(this)
    this.onSetInstance = this.onSetInstance.bind(this)
  }
  componentWillUnmount() {
    const { onClose } = this.props.navigation.state.params
    if (onClose) onClose()
  }
  onChangeText(text) {
    this.setState({
      instanceId: text,
      loading: false,
    })
    if (this.props.loadingState !== constants.LOADING_STATES.progress) {
      this.props.initSelection()
    }
  }
  onSetInstance(id) {
    const { instance, navigation, history } = this.props
    if (instance.id !== id) {
      const { client } = this.props.navigation.state.params
      const historyEntry = history[id]
      const token = historyEntry ? historyEntry.data.token : undefined
      const reset = () => {
        // reset the apollo store
        client.resetStore().then(() =>
          this.setState(
            {
              loading: constants.LOADING_STATES.success,
            },
            () => navigation.goBack(),
          ),
        )
      }
      this.setState(
        {
          loading: constants.LOADING_STATES.pending,
        },
        () =>
          // init the current instance (logout the current user)
          this.props.initInstance(instance).then(() =>
            // load the new instance
            this.props.setInstance(id).then(() => {
              // login if the token associated to the new instance is defined
              if (token) {
                this.props.userLogin(historyEntry.data, '', '', token).then(reset)
              } else {
                reset()
              }
            }),
          ),
      )
    }
  }
  render() {
    const { network, history, instance } = this.props
    const loadingState = this.state.loading
    const textInputColor = loadingState !== 'error' ? '#a2a4a2ff' : '#f00'
    const networkError = !network.isConnected || network.url.error
    return (
      <KeyboardAvoidingView style={styles.container} behavior="padding" keyboardVerticalOffset={60}>
        <ScrollView>
          <View style={styles.instancesContainer}>
            {Object.values(history).map((itme, key) =>
              <TouchableOpacity
                key={key}
                style={[
                  styles.instance,
                  instance.id === itme.data.id ? { borderWidth: 3, borderColor: '#ef6e18' } : {},
                ]}
                onPress={() => this.onSetInstance(itme.data.id)}
              >
                <Text style={styles.instanceTitle}>
                  {itme.data.id}
                </Text>
                <Image
                  resizeMode="contain"
                  source={itme.data.logo ? { uri: itme.data.logo } : NovaIdeoLogo}
                  style={styles.instanceImage}
                />
              </TouchableOpacity>,
            )}
          </View>
        </ScrollView>
        <LoadingState
          state={loadingState}
          errorText={I18n.t('instanceNotFound')}
          indicatorColor="#ef6e18"
          style={styles.loadingState}
        />
        <View style={styles.submitView}>
          <TextField
            disabled={loadingState === constants.LOADING_STATES.pending}
            label={I18n.t('enterNovaIdeoInstance')}
            value={this.state.instanceId}
            dense
            highlightColor={textInputColor}
            labelColor={textInputColor}
            borderColor={textInputColor}
            textColor={'#ef6e18ff'}
            onChangeText={this.onChangeText}
          />
          <Text style={styles.fieldDescription}>
            {I18n.t('instanceSelectorText')}
          </Text>
          <ThemedButton
            primary
            disabled={networkError || loadingState === constants.LOADING_STATES.pending}
            text={I18n.t('continue')}
            value="Continue"
            raised
            onPress={() => this.onSetInstance(this.state.instanceId)}
          />
        </View>
      </KeyboardAvoidingView>
    )
  }
}

export const mapStateToProps = state => ({
  instance: state.instance,
  network: state.network,
  history: state.history,
})

export const mapDispatchToProps = {
  initInstance,
  setInstance,
  initSelection,
  userLogin,
}

export default connect(mapStateToProps, mapDispatchToProps)(DumbInstanceSwitch)
