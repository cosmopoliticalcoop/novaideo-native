import React from 'react'
import renderer from 'react-test-renderer'

import LoadingState from './LoadingState'

describe('LoadingState component', () => {
  it('should match LoadingState snapshot', () => {
    const props = {
      state: 'penging',
      style: {},
      idcatorColor: '#fff',
      successText: 'ok',
      errorText: 'error',
    }
    const rendered = renderer.create(<LoadingState {...props} />).toJSON()
    expect(rendered).toMatchSnapshot()
  })
})
