import React from 'react'
import {
  StyleSheet,
  View,
  KeyboardAvoidingView,
  Keyboard,
  Animated,
  TouchableOpacity,
  Linking,
  Text,
} from 'react-native'
import { connect } from 'react-redux'

import ThemedButton from './ThemedButton'
import TextField from './TextField'
import { userLogin, initLogin, initInstance, updateUserToken } from '../actions'
import NovaIdeoLogo from '../novaideo_logo.png'
import LoadingState from './LoadingState'
import Icon from './Icon'
import { Header, HeaderTitle, HeaderIcon } from './styled-components/HeaderNav'
import * as constants from '../constants'
import I18n from '../locale'

const styles = StyleSheet.create({
  page: {
    backgroundColor: '#fafafaff',
    flex: 1,
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  container: {
    flex: 1,
    justifyContent: 'center',
  },
  title: {
    color: '#666666ff',
  },
  image: {
    margin: 5,
  },
  submitView: {
    width: '100%',
    padding: 15,
    marginBottom: 5,
  },
  loadingState: {
    width: '100%',
    alignItems: 'center',
    justifyContent: 'center',
  },
  policyText: {
    color: 'gray',
    fontSize: 12,
    textDecorationLine: 'underline',
  },
  policyContainer: {
    alignItems: 'center',
    marginTop: 10,
  },
})

export class DumbLogin extends React.Component {
  static navigationOptions = {
    header: props =>
      <Header>
        <HeaderIcon>
          <TouchableOpacity
            onPress={() => {
              props.scene.route.params.onClose()
              props.navigation.goBack()
            }}
            hitSlop={{ top: 15, bottom: 15, left: 15, right: 15 }}
          >
            <Icon name="arrow-left" size={22} color="white" />
          </TouchableOpacity>
        </HeaderIcon>
        <View>
          <HeaderTitle>
            {I18n.t('login')}
          </HeaderTitle>
        </View>
      </Header>,
  }

  constructor() {
    super()
    this.state = {
      login: '',
      password: '',
    }
    this.onChangeLogin = this.onChangeLogin.bind(this)
    this.onChangePassword = this.onChangePassword.bind(this)
    this.onViewClose = this.onViewClose.bind(this)
    this.onChangeInstance = this.onChangeInstance.bind(this)
    this.keyboardWillShow = this.keyboardWillShow.bind(this)
    this.keyboardWillHide = this.keyboardWillHide.bind(this)
    this.imageHeight = new Animated.Value(constants.HOME_IMG_HEIGHT)
    this.welcomeFontSize = new Animated.Value(constants.HOME_TEXT_FS)
  }

  componentWillMount() {
    this.keyboardWillShowSub = Keyboard.addListener('keyboardDidShow', this.keyboardWillShow)
    this.keyboardWillHideSub = Keyboard.addListener('keyboardDidHide', this.keyboardWillHide)
  }

  componentWillUnmount() {
    const { navigation } = this.props
    if (navigation) navigation.state.params.onClose()
    this.keyboardWillShowSub.remove()
    this.keyboardWillHideSub.remove()
  }
  onChangeLogin(text) {
    this.setState({
      login: text,
      password: this.state.password,
    })
    if (this.props.instance.loadingState !== constants.LOADING_STATES.progress) {
      this.props.initLogin()
    }
  }

  onChangePassword(text) {
    this.setState({
      login: this.state.login,
      password: text,
    })
    if (this.props.instance.loadingState !== constants.LOADING_STATES.progress) {
      this.props.initLogin()
    }
  }

  onChangeInstance() {
    if (!this.state.viewOpened) {
      this.setState({
        viewOpened: true,
      })
      const navigation = this.props.navigation || this.props.globalProps.navigation
      const client = this.props.client || this.props.navigation.state.params.client
      navigation.goBack()
      // TODO ??
      setTimeout(() =>
        navigation.navigate('InstanceSwitch', { client, title: I18n.t('changeInstance') }),
      )
    }
  }

  onViewClose() {
    if (this.state.viewOpened) {
      this.setState({ viewOpened: false })
      if (this.props.navigation) this.props.navigation.goBack()
    }
  }

  keyboardWillShow = () => {
    Animated.timing(this.imageHeight, {
      duration: constants.ANIM_DURATION,
      toValue: constants.HOME_IMG_HEIGHT_SMALL,
    }).start()
    Animated.timing(this.welcomeFontSize, {
      duration: constants.ANIM_DURATION,
      toValue: constants.HOME_TEXT_FS_SMALL,
    }).start()
  }

  keyboardWillHide = () => {
    Animated.timing(this.imageHeight, {
      duration: constants.ANIM_DURATION,
      toValue: constants.HOME_IMG_HEIGHT,
    }).start()
    Animated.timing(this.welcomeFontSize, {
      duration: constants.ANIM_DURATION,
      toValue: constants.HOME_TEXT_FS,
    }).start()
  }

  render() {
    const { instance, user, network } = this.props
    const textInputColor = user.loadingState !== 'error' ? '#a2a4a2ff' : '#f00'
    const logo = instance.logo
    const networkError = !network.isConnected || network.url.error
    return (
      <KeyboardAvoidingView style={styles.container} behavior="padding">
        <View style={styles.page}>
          <View style={styles.container}>
            <Animated.Text style={[styles.title, { fontSize: this.welcomeFontSize }]}>
              {instance.title}
            </Animated.Text>
            <Animated.Image
              resizeMode="contain"
              source={logo ? { uri: logo } : NovaIdeoLogo}
              style={[styles.image, { width: this.imageHeight, height: this.imageHeight }]}
            />
          </View>
          <View style={styles.submitView}>
            <TextField
              disabled={user.loadingState === constants.LOADING_STATES.pending}
              keyboardType="email-address"
              label={I18n.t('labelLogin')}
              value={this.state.login}
              dense
              returnKeyType="next"
              highlightColor={textInputColor}
              labelColor={textInputColor}
              borderColor={textInputColor}
              textColor={'#ef6e18ff'}
              onChangeText={this.onChangeLogin}
            />
            <TextField
              disabled={user.loadingState === constants.LOADING_STATES.pending}
              label={I18n.t('password')}
              value={this.state.password}
              dense
              secureTextEntry
              returnKeyType="go"
              highlightColor={textInputColor}
              labelColor={textInputColor}
              borderColor={textInputColor}
              textColor={'#ef6e18ff'}
              onChangeText={this.onChangePassword}
            />
            <LoadingState
              state={user.loadingState}
              errorText={I18n.t('failedlogin')}
              successText={user.token}
              indicatorColor="#ef6e18"
              style={styles.loadingState}
            />
            <ThemedButton
              primary
              disabled={networkError || user.loadingState === constants.LOADING_STATES.pending}
              text={I18n.t('login')}
              value="login"
              raised
              onPress={() => {
                this.props
                  .userLogin(instance, this.state.login, this.state.password)
                  .then(({ value }) => {
                    // update the user token (see the history reducer)
                    this.props.updateUserToken(instance, value.token)
                    if (value.status && this.props.navigation) {
                      this.props.navigation.goBack()
                      this.props.navigation.state.params.client.resetStore()
                    } else {
                      this.props.client.resetStore()
                    }
                  })
                  .catch(() => undefined)
              }}
            />
            <ThemedButton
              disabled={user.loadingState === constants.LOADING_STATES.pending}
              text={I18n.t('changeInstance')}
              value="changeInstance"
              raised
              onPress={this.onChangeInstance}
            />
            <TouchableOpacity
              style={styles.policyContainer}
              onPress={() => Linking.openURL(I18n.t('policyUrl'))}
            >
              <Text style={styles.policyText}>
                {I18n.t('privacyPolicy')}
              </Text>
            </TouchableOpacity>
          </View>
        </View>
      </KeyboardAvoidingView>
    )
  }
}

export const mapStateToProps = state => ({
  instance: state.instance,
  user: state.user,
  network: state.network,
  globalProps: state.globalProps,
})

export const mapDispatchToProps = { userLogin, initLogin, initInstance, updateUserToken }

export default connect(mapStateToProps, mapDispatchToProps)(DumbLogin)
