import React from 'react'
import { TextInput, TouchableOpacity, NetInfo } from 'react-native'
import { connect } from 'react-redux'
import debounce from 'lodash.debounce'
import { withTheme } from 'glamorous-native'

import SearchContainer, { InputContainer, IconContainer } from './styled-components/SearchContainer'
import Icon from './Icon'
import AccountActivator from './AccountActivator'
import { searchEntities, setURLState, setConnectionState } from '../actions'
import {
  HeaderContainer,
  Container,
  MessageBox,
  MessageText,
  HeaderItemContainer,
} from './styled-components/Header'
import I18n from '../locale'

class DumbHeader extends React.Component {
  constructor(props) {
    super(props)
    this.onChangeSearchText = this.onChangeSearchText.bind(this)
    this.onSetURLStatus = this.onSetURLStatus.bind(this)
    this.onSetNetworkStatus = this.onSetNetworkStatus.bind(this)
    this.state = {
      searchText: '',
      searchStatus: 'completed',
      connectionStatus: 'completed',
      debounce: undefined,
    }
  }

  onChangeSearchText(text) {
    const searchDebounce = debounce(() => {
      this.setState({ searchStatus: 'completed', debounce: undefined }, () =>
        this.props.searchEntities(text),
      )
    }, 200)
    if (this.state.searchText !== text) {
      if (this.state.debounce !== undefined) this.state.debounce.cancel()

      this.setState(
        {
          searchText: text,
          searchStatus: 'pending',
          debounce: searchDebounce,
        },
        () => searchDebounce(),
      )
    }
  }

  onSetURLStatus() {
    this.props.setURLState(false, [])
  }

  onSetNetworkStatus() {
    this.setState({ connectionStatus: 'pending' }, () => {
      NetInfo.isConnected.fetch().then((isConnected) => {
        this.setState({ connectionStatus: 'completed' })
        this.props.setConnectionState(isConnected)
      })
    })
  }

  render() {
    const { network, search, theme, globalProps } = this.props
    return (
      <HeaderContainer>
        <Container>
          {network.isLogged
            ? <HeaderItemContainer>
              <TouchableOpacity
                onPress={globalProps.openDrawer}
                hitSlop={{ top: 15, bottom: 15, left: 15, right: 15 }}
              >
                <Icon name="comment" size={22} color={theme.primary.color} />
              </TouchableOpacity>
            </HeaderItemContainer>
            : null}
          <SearchContainer flex={1}>
            <IconContainer>
              {this.state.searchStatus === 'pending'
                ? <Icon name="pencil" size={20} color={theme.color.orange} />
                : <Icon name="magnify" size={20} color={theme.primary.color} />}
            </IconContainer>
            <InputContainer>
              <TextInput
                style={{ color: theme.primary.color }}
                returnKeyType="search"
                placeholder={I18n.t('search')}
                placeholderTextColor={theme.primary.lightColor}
                underlineColorAndroid="rgba(0,0,0,0)"
                onChangeText={this.onChangeSearchText}
                value={this.state.searchStatus === 'pending' ? this.state.searchText : search.text}
              />
            </InputContainer>
            <IconContainer>
              {search.text
                ? <TouchableOpacity
                  hitSlop={{ top: 15, bottom: 15, left: 15, right: 15 }}
                  onPress={() => this.props.searchEntities('')}
                >
                  <Icon name="close" size={20} color={theme.primary.color} />
                </TouchableOpacity>
                : undefined}
            </IconContainer>
          </SearchContainer>
          <HeaderItemContainer>
            <AccountActivator />
          </HeaderItemContainer>
        </Container>
        {network.isConnected && network.url.error
          ? <MessageBox error>
            <MessageText>
              {I18n.t('dataLoadingProblem')}
            </MessageText>
            <TouchableOpacity
              hitSlop={{ top: 15, bottom: 15, left: 15, right: 15 }}
              onPress={this.onSetURLStatus}
            >
              <Icon name={'close'} size={13} color="white" />
            </TouchableOpacity>
          </MessageBox>
          : undefined}
        {!network.isConnected
          ? <MessageBox>
            <MessageText>
              {I18n.t('offline')}
            </MessageText>
          </MessageBox>
          : undefined}
      </HeaderContainer>
    )
  }
}

export const mapStateToProps = state => ({
  network: state.network,
  search: state.search,
  globalProps: state.globalProps,
})

export const mapDispatchToProps = {
  searchEntities,
  setURLState,
  setConnectionState,
}

export const ThemedDumbHeader = withTheme(DumbHeader)
export default connect(mapStateToProps, mapDispatchToProps)(ThemedDumbHeader)
