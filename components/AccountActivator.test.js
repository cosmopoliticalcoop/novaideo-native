import React from 'react'
import renderer from 'react-test-renderer'
import { MenuContext } from 'react-native-menu'

import { DumbAccountActivator } from './AccountActivator'
import { theme } from '../theme'

describe('AccountActivator component', () => {
  it('should match AccountActivator with data snapshot', () => {
    const instance = {
      id: 'foobar',
      url: 'https://foobar.com/logo.png/',
      isPrivate: true,
      logo: 'https://foobar.com/logo.png/',
      title: 'Foobar',
      loadingState: 'completed',
    }
    const props = {
      instance,
      network: { isConnected: true, url: { error: false } },
      user: { token: 'usertoken' },
      data: {
        account: {
          title: 'Alice',
          picture: {
            url: 'https://foobar.com/Alice.png',
          },
        },
      },
      theme,
      globalProps: {},
    }
    const rendered = renderer
      .create(
        <MenuContext>
          <DumbAccountActivator {...props} />
        </MenuContext>,
      )
      .toJSON()
    expect(rendered).toMatchSnapshot()
  })
})
