import React from 'react'
import ShallowRenderer from 'react-test-renderer/shallow'

import { DumbMyContents } from './MyContents'
import Examination from './Examination'

describe('MyContents component', () => {
  it('should match MyContents loading snapshot', () => {
    const props = {
      data: {
        error: {},
        networkStatus: 1,
      },
      screenProps: { rootProps: {} },
    }

    const renderer = new ShallowRenderer()
    renderer.render(<DumbMyContents {...props} />)
    const rendered = renderer.getRenderOutput()
    expect(rendered).toMatchSnapshot()
  })

  it('should match MyContents with data snapshot', () => {
    const props = {
      screenProps: { rootProps: {} },
      data: {
        error: {},
        loading: false,
        account: {
          contents: {
            pageInfo: {
              endCursor: 'idea_id',
            },
            edges: [
              {
                node: {
                  title: 'Foobar',
                  keywords: ['k1', 'k2'],
                  text: 'Idea text',
                  createdAt: '2017-01-01T12:30:59',
                  presentationText: 'Presentation text',
                  state: ['published', 'submitted_support'],
                  actions: [],
                  author: {
                    picture: {
                      url: 'http//example.nova-ideo.com/principals/users/JohnDoe/avatar.jpg',
                    },
                    title: 'John Doe',
                  },
                },
              },
            ],
          },
        },
      },
      adapters: {
        examination: Examination,
      },
      instance: {
        support_ideas: true,
      },
    }
    const renderer = new ShallowRenderer()
    renderer.render(<DumbMyContents {...props} />)
    const rendered = renderer.getRenderOutput()
    expect(rendered).toMatchSnapshot()
  })
})
