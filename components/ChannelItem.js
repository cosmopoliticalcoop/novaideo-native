/* eslint-disable react/no-array-index-key */
import React from 'react'
import { View, Text, TouchableOpacity, StyleSheet, BackHandler, Image } from 'react-native'
import { connect } from 'react-redux'

import IconWithText from './IconWithText'
import { searchEntities } from '../actions'
import * as constants from '../constants'
import Avatar from './Avatar'

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    padding: 5,
    paddingLeft: 15,
    paddingRight: 10,
  },
  channelItem: {
    color: 'white',
    fontSize: 15,
    opacity: 0.7,
    marginLeft: 10,
  },
  styleIcon: {
    marginTop: 2,
  },
  textContainer: { width: '80%' },
  unreadCommentContainer: {
    height: 25,
    backgroundColor: '#f00',
    padding: 3,
    paddingLeft: 8,
    paddingRight: 8,
    borderRadius: 30,
    marginTop: -3,
  },
  unreadComment: {
    color: 'white',
    fontWeight: 'bold',
  },
  discussion: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
  },
})

export class DumbChannelItem extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      opened: false,
    }
    this.onPress = this.onPress.bind(this)
    this.onClose = this.onClose.bind(this)
  }

  componentDidMount() {
    this.subs = BackHandler.addEventListener('backPress', () =>
      this.setState(state => (state.opened ? { opened: false } : null)),
    )
  }

  componentWillUnmount() {
    if (this.subs) this.subs.remove()
  }

  onPress() {
    if (!this.state.opened) {
      this.setState({ opened: true })
      const { node, itemdata } = this.props
      return this.props.globalProps.navigation.navigate('Comments', {
        id: node.id,
        isDiscussion: itemdata && itemdata.isDiscussion,
        title: node.title,
        onClose: this.onClose,
        theme: this.props.theme,
      })
    }
    return undefined
  }

  onClose() {
    if (this.state.opened) {
      this.setState({ opened: false })
    }
  }

  render() {
    const { node, itemdata } = this.props
    const channelPicture = node.subject.picture
    const lenUnreadComments = node.unreadComments.length
    const channelItemStyle = [
      styles.channelItem,
      lenUnreadComments > 0 ? { opacity: 1, fontWeight: 'bold' } : {},
    ]
    return (
      <TouchableOpacity onPress={this.onPress}>
        <View style={styles.container}>
          <View style={styles.textContainer}>
            {itemdata && itemdata.isDiscussion
              ? <View style={styles.discussion}>
                <Avatar
                  size={30}
                  borderRadius={15}
                  image={
                    <Image
                      source={
                          channelPicture
                            ? { uri: `${channelPicture.url}profil` }
                            : constants.DEFAULT_USER_IMG
                        }
                    />
                    }
                />
                <Text style={channelItemStyle}>
                  {node.title}
                </Text>
              </View>
              : <IconWithText
                iconColor={lenUnreadComments > 0 ? 'white' : 'rgba(255, 255, 255, 0.7)'}
                iconSize={15}
                name="pound"
                styleIcon={styles.icon}
                styleText={channelItemStyle}
                text={node.title}
                numberOfLines={1}
              />}
          </View>
          {lenUnreadComments > 0
            ? <View style={styles.unreadCommentContainer}>
              <Text style={styles.unreadComment}>
                {lenUnreadComments}
              </Text>
            </View>
            : null}
        </View>
      </TouchableOpacity>
    )
  }
}

export const mapDispatchToProps = { searchEntities }

export const mapStateToProps = state => ({
  globalProps: state.globalProps,
})

export default connect(mapStateToProps, mapDispatchToProps)(DumbChannelItem)
