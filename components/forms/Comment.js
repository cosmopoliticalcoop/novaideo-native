/* eslint-disable react/no-array-index-key, no-confusing-arrow */
import React from 'react'
import { View, StyleSheet, TouchableOpacity, KeyboardAvoidingView } from 'react-native'
import { Field, reduxForm, initialize } from 'redux-form'
import { connect } from 'react-redux'
import { gql, graphql } from 'react-apollo'
import { ReactNativeFile } from 'apollo-upload-client'
import update from 'immutability-helper'
import { withTheme } from 'glamorous-native'

import { commentFragment } from '../../queries'
import { renderInput, renderFilesList, renderCheckbox } from './utils'
import I18n from '../../locale'
import Icon from '../Icon'

const styles = StyleSheet.create({
  contentContainerStyle: {
    backgroundColor: '#fafafaff',
    marginBottom: -10,
  },
  container: {
    borderTopWidth: 0.5,
    borderColor: '#b3b3b3',
  },
  addon: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    backgroundColor: '#fafafaff',
  },
  inputContainer: {
    backgroundColor: '#fafafaff',
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  textField: {
    paddingLeft: 10,
    flex: 0.93,
  },
  action: {
    flex: 0.07,
    marginTop: 5,
  },
})

export class DumbCommentForm extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      displayErrors: false,
      sending: false,
    }
    this.handleSubmit = this.handleSubmit.bind(this)
  }

  handleSubmit() {
    const { formData, valid, globalProps, context, action } = this.props
    if (valid) {
      // submit form here
      this.setState({ sending: true })
      // we must encode the file name
      const files = formData.values.files
        ? ReactNativeFile.list(
            formData.values.files.map(file => ({
              uri: file.url,
              name: encodeURI(file.name),
              type: `${file.type}/*`,
            })),
          )
        : []
      const anonymous = Boolean(formData.values.anonymous)
      this.props.commentObject({
        context,
        comment: formData.values.comment,
        attachedFiles: files,
        anonymous,
        account: globalProps.account,
        action: `${action.processId}.${action.nodeId}`,
      })
      this.props.dispatch(initialize('Comment', { anonymous, files: [] }))
    } else {
      this.setState({ displayErrors: true })
    }
  }

  render() {
    const { formData, theme, channel, globalProps: { siteConf } } = this.props
    const errors = formData ? formData.syncErrors : {}
    const hasComment = formData && formData.values && formData.values.comment
    const withAnonymous = siteConf.anonymisation && !channel.isDiscuss
    return (
      <KeyboardAvoidingView
        contentContainerStyle={styles.contentContainerStyle}
        behavior="padding"
        keyboardVerticalOffset={60}
      >
        <View style={styles.container}>
          <View style={withAnonymous ? styles.addon : {}}>
            {withAnonymous
              ? <Field
                props={{
                  label: I18n.t('RemainAnonymous'),
                  displayErrors: this.state.displayErrors,
                  errors,
                }}
                name="anonymous"
                component={renderCheckbox}
                type="boolean"
              />
              : null}
            <Field name="files" component={renderFilesList} />
          </View>
          <View style={styles.inputContainer}>
            <View style={styles.textField}>
              <Field
                props={{
                  errors,
                  multiline: true,
                  style: {
                    borderColor: 'transparent',
                    highlightColor: 'transparent',
                  },
                  wrapperStyle: {
                    paddingTop: 5,
                    paddingBottom: 3,
                  },
                  placeholder: I18n.t('yourMessageHere'),
                  placeholderTextColor: theme.color.gray,
                  inputStyle: { fontSize: 14 },
                }}
                name="comment"
                component={renderInput}
                type="text"
              />
            </View>
            <View style={styles.action}>
              <TouchableOpacity onPress={hasComment ? this.handleSubmit : undefined}>
                <Icon
                  name="send"
                  size={22}
                  color={hasComment ? theme.color.blue : theme.color.gray}
                />
              </TouchableOpacity>
            </View>
          </View>
        </View>
      </KeyboardAvoidingView>
    )
  }
}

// Decorate the form component
const CommentReduxForm = reduxForm({
  form: 'Comment', // a unique name for this form
})(DumbCommentForm)

const mapStateToProps = state => ({
  formData: state.form.Comment,
  globalProps: state.globalProps,
})

const commentObject = gql`
  mutation($context: String!, $comment: String!, $action: String!, $attachedFiles: [Upload], $anonymous: Boolean) {
    commentObject(
      context: $context
      comment: $comment
      action: $action
      attachedFiles: $attachedFiles,
      anonymous: $anonymous
    ) {
      status
      comment {
        ...comment
      }
    }
  }
  ${commentFragment}
`

const CommentForm = graphql(commentObject, {
  props({ ownProps, mutate }) {
    return {
      commentObject({ context, comment, action, attachedFiles, anonymous, account }) {
        const { formData } = ownProps
        const files =
          attachedFiles.length > 0
            ? formData.values.files.map(file => ({
              url: file.url,
              isImage: file.type === 'image',
              __typename: 'File',
            }))
            : []
        const createdAt = new Date()
        return mutate({
          variables: {
            context,
            comment,
            attachedFiles,
            anonymous,
            action,
          },
          optimisticResponse: {
            __typename: 'Mutation',
            commentObject: {
              __typename: 'CommentObject',
              status: true,
              comment: {
                __typename: 'Comment',
                id: '0',
                oid: '0',
                channel: { ...ownProps.channel, unreadComments: [] },
                rootOid: ownProps.rootContext,
                createdAt: createdAt.toISOString(),
                text: comment,
                attachedFiles: files,
                urls: [],
                author: {
                  __typename: 'Person',
                  id: `${account.id}comment`,
                  oid: `${account.oid}comment`,
                  isAnonymous: anonymous,
                  description: account.description,
                  function: account.function,
                  title: !anonymous ? account.title : 'Anonymous',
                  picture:
                    !anonymous && account.picture
                      ? {
                        __typename: 'File',
                        url: account.picture.url,
                      }
                      : null,
                },
                actions: [],
                lenComments: 0,
              },
            },
          },
          updateQueries: {
            IdeasList: (prev) => {
              const currentIdea = prev.ideas.edges.filter(
                item => item && item.node.oid === ownProps.rootContext,
              )[0]
              if (!currentIdea) {
                return prev
              }
              const commentAction = currentIdea.node.actions.filter(
                item => item && item.behaviorId === 'comment',
              )[0]
              const indexAction = currentIdea.node.actions.indexOf(commentAction)
              const newAction = update(commentAction, {
                counter: { $set: commentAction.counter + 1 },
              })
              const newIdea = update(currentIdea, {
                node: {
                  actions: {
                    $splice: [[indexAction, 1, newAction]],
                  },
                },
              })
              const index = prev.ideas.edges.indexOf(currentIdea)
              return update(prev, {
                ideas: {
                  edges: {
                    $splice: [[index, 1, newIdea]],
                  },
                },
              })
            },
            Profil: (prev) => {
              if (prev.person.oid !== ownProps.rootContext) return prev
              const commentAction = prev.person.actions.filter(
                item => item && item.behaviorId === 'discuss',
              )[0]
              const indexAction = prev.person.actions.indexOf(commentAction)
              const newAction = update(commentAction, {
                counter: { $set: commentAction.counter + 1 },
              })
              return update(prev, {
                person: {
                  actions: {
                    $splice: [[indexAction, 1, newAction]],
                  },
                },
              })
            },
            Channels: (prev) => {
              if (ownProps.channel.isDiscuss) return prev
              const currentChannel = prev.account.channels.edges.filter(
                item => item && item.node.id === ownProps.channel.id,
              )[0]
              if (currentChannel) {
                return prev
              }
              const newChannel = { ...ownProps.channel, unreadComments: [] }
              return update(prev, {
                account: {
                  channels: {
                    edges: {
                      $unshift: [
                        {
                          __typename: 'Channel',
                          node: newChannel,
                        },
                      ],
                    },
                  },
                },
              })
            },
            Discussions: (prev) => {
              if (!ownProps.channel.isDiscuss) return prev
              const currentChannel = prev.account.discussions.edges.filter(
                item => item && item.node.id === ownProps.channel.id,
              )[0]
              if (currentChannel) {
                return prev
              }
              const newChannel = { ...ownProps.channel, unreadComments: [] }
              return update(prev, {
                account: {
                  discussions: {
                    edges: {
                      $unshift: [
                        {
                          __typename: 'Channel',
                          node: newChannel,
                        },
                      ],
                    },
                  },
                },
              })
            },
            Comments: (prev, { mutationResult }) => {
              if (ownProps.context !== prev.node.subject.oid) {
                return prev
              }

              const newComment = mutationResult.data.commentObject.comment
              return update(prev, {
                node: {
                  lenComments: { $set: prev.node.lenComments + 1 },
                  comments: {
                    edges: {
                      $unshift: [
                        {
                          __typename: 'Comment',
                          node: newComment,
                        },
                      ],
                    },
                  },
                },
              })
            },
            Comment: (prev, { mutationResult }) => {
              if (ownProps.context !== prev.node.oid) {
                return prev
              }
              const newComment = mutationResult.data.commentObject.comment
              return update(prev, {
                node: {
                  lenComments: { $set: prev.node.lenComments + 1 },
                  comments: {
                    edges: {
                      $unshift: [
                        {
                          __typename: 'Comment',
                          node: newComment,
                        },
                      ],
                    },
                  },
                },
              })
            },
          },
        })
      },
    }
  },
})(CommentReduxForm)

export const ThemedCommentReduxForm = withTheme(CommentForm)
export default connect(mapStateToProps)(ThemedCommentReduxForm)
