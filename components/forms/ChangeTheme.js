/* eslint-disable react/no-array-index-key, no-undef */
import React from 'react'
import {
  View,
  TouchableOpacity,
  ActivityIndicator,
  Text,
  StyleSheet,
  ScrollView,
} from 'react-native'
import { connect } from 'react-redux'
import debounce from 'lodash.debounce'

import { setTheme } from '../../actions'
import I18n from '../../locale'
import Icon from '../Icon'

const colors = [
  '#54902a',
  '#556b2f',
  '#e32636',
  '#db4437',
  '#a52a2a',
  '#ef6e18',
  '#cc5500',
  '#4b3621',
  '#329cc3',
  '#3b5998',
  '#003366',
  '#4d394b',
  '#702963',
  '#534b4f',
  '#536872',
]

const styles = StyleSheet.create({
  container: {
    flex: 1,
    // justifyContent: 'center',
    backgroundColor: '#fafafaff',
    alignItems: 'center',
  },
  themesContainer: {
    backgroundColor: 'transparent',
    flexWrap: 'wrap',
    flexDirection: 'row',
  },
  theme: {
    flexDirection: 'row',
    justifyContent: 'space-around',
    alignItems: 'center',
    borderRadius: 3,
    elevation: 3,
    shadowColor: 'gray',
    shadowOpacity: 0.8,
    shadowRadius: 2,
    shadowOffset: {
      height: 1,
      width: 0,
    },
    width: 50,
    height: 50,
    margin: 10,
    marginBottom: 5,
  },
  messageContainer: {
    alignItems: 'center',
    padding: 30,
  },
  message: {
    color: 'gray',
    marginLeft: 4,
    fontSize: 20,
    textAlign: 'center',
  },
})

export class DumbChangeTheme extends React.Component {
  constructor(props) {
    super(props)
    const { instance, history } = props
    const historyEntry = history[instance.id]
    const appColor = historyEntry ? historyEntry.userPreferences.appColor : undefined
    this.state = {
      currentColor: appColor,
      loadingColor: false,
    }
    this.changeColor = this.changeColor.bind(this)
  }
  changeColor(color) {
    if (this.state.appColor !== color) {
      const { instance } = this.props
      const changeColorDebounce = debounce(() => {
        this.props.setTheme(instance, color)
        this.setState({ loadingColor: false })
      }, 1)
      this.setState({ loadingColor: true, currentColor: color }, changeColorDebounce)
    }
  }
  render() {
    return (
      <View style={styles.container}>
        <ScrollView>
          <View style={styles.messageContainer}>
            <Icon name={'format-color-fill'} size={30} color={this.state.currentColor || 'gray'} />
            <Text
              style={[
                styles.message,
                this.state.loadingColor ? { color: this.state.currentColor } : {},
              ]}
            >
              {this.state.loadingColor ? I18n.t('loadingTheme') : I18n.t('selectTheme')}
            </Text>
          </View>
          <View style={styles.themesContainer}>
            {colors.map((color, key) =>
              <TouchableOpacity key={key} onPress={() => this.changeColor(color)}>
                <View
                  style={[
                    styles.theme,
                    { backgroundColor: color },
                    this.state.currentColor === color
                      ? { borderWidth: 3, borderColor: 'white' }
                      : {},
                  ]}
                >
                  {this.state.loadingColor && this.state.currentColor === color
                    ? <ActivityIndicator
                      style={{
                        elevation: 4,
                      }}
                      animating
                      color={'white'}
                    />
                    : null}
                </View>
              </TouchableOpacity>,
            )}
          </View>
        </ScrollView>
      </View>
    )
  }
}

export const mapStateToProps = state => ({
  instance: state.instance,
  history: state.history,
})

export const mapDispatchToProps = { setTheme }

export default connect(mapStateToProps, mapDispatchToProps)(DumbChangeTheme)
