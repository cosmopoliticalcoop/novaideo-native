/* eslint-disable react/no-array-index-key, no-confusing-arrow */
import React from 'react'
import { View, StyleSheet, ActivityIndicator, Text } from 'react-native'
import { Field, reduxForm } from 'redux-form'
import { connect } from 'react-redux'
import { gql, graphql } from 'react-apollo'
import { ReactNativeFile } from 'apollo-upload-client'
import update from 'immutability-helper'

import ThemedButton from '../ThemedButton'
import { renderInput, renderFilesList, renderSelect, renderCheckbox } from './utils'
import I18n from '../../locale'
import { ideaFragment } from '../../queries'

const styles = StyleSheet.create({
  container: {
    backgroundColor: '#fafafaff',
    flex: 1,
    padding: 10,
    justifyContent: 'space-between',
  },
  loading: {
    flex: 1,
    alignItems: 'center',
    margin: 50,
  },
  loadingText: {
    color: 'gray',
    fontSize: 17,
    marginBottom: 5,
  },
  footerField: {
    marginBottom: 10,
  },
})

export class DumbCreateIdeaForm extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      displayErrors: false,
      sending: false,
    }
    this.handleSubmit = this.handleSubmit.bind(this)
  }
  componentWillUnmount() {
    this.props.navigation.state.params.onClose()
  }

  handleSubmit(action) {
    const { formData, valid, globalProps } = this.props
    if (valid) {
      // submit form here
      const close = () => {
        this.props.navigation.goBack()
      }
      this.setState({ sending: true })
      // we must encode the file name
      const files = formData.values.files
        ? ReactNativeFile.list(
            formData.values.files.map(file => ({
              uri: file.url,
              name: encodeURI(file.name),
              type: `${file.type}/*`,
            })),
          )
        : []
      if (action.nodeId === 'creatandpublish') {
        this.props.createAndPublish({
          text: formData.values.text,
          title: formData.values.title,
          keywords: formData.values.keywords.map(item => item.title),
          attachedFiles: files,
          anonymous: Boolean(formData.values.anonymous),
          account: globalProps.account,
        })
        close()
      }
      if (action.nodeId === 'creat') {
        this.props.createIdea({
          text: formData.values.text,
          title: formData.values.title,
          keywords: formData.values.keywords.map(item => item.title),
          attachedFiles: files,
          anonymous: Boolean(formData.values.anonymous),
          account: globalProps.account,
        })
        close()
      }
    } else {
      this.setState({ displayErrors: true })
    }
  }

  render() {
    const { submitting, formData, globalProps: { siteConf } } = this.props
    const errors = formData ? formData.syncErrors : {}
    const { actions } = this.props.navigation.state.params
    const keywords = siteConf.keywords.map(keyword => ({ title: keyword, id: keyword }))
    if (this.state.sending) {
      return (
        <View style={styles.loading}>
          <Text style={styles.loadingText}>
            {I18n.t('sendingRequest')}
          </Text>
          <ActivityIndicator size="large" animating color={'#54902aff'} />
        </View>
      )
    }
    return (
      <View style={styles.container}>
        <View>
          <Field
            props={{
              label: I18n.t('title'),
              displayErrors: this.state.displayErrors,
              errors,
            }}
            validate={value => (!value ? I18n.t('ideaTitleRequierd') : undefined)}
            name="title"
            component={renderInput}
            type="text"
          />
          <Field
            props={{
              label: I18n.t('keywords'),
              options: keywords,
              defaultValue: 'Select keywords',
              canAdd: siteConf.canAddKeywords,
              displayErrors: this.state.displayErrors,
              errors,
            }}
            validate={value =>
              !value || value.length === 0 ? I18n.t('ideaKeywordsRequierd') : undefined}
            name="keywords"
            component={renderSelect}
          />
          <Field
            props={{
              label: I18n.t('text'),
              displayErrors: this.state.displayErrors,
              errors,
              multiline: true,
              style: {
                height: 100,
              },
            }}
            validate={value => (!value ? I18n.t('ideaTextRequierd') : undefined)}
            name="text"
            component={renderInput}
            type="text"
          />
          <Field name="files" component={renderFilesList} />
        </View>
        <View>
          {siteConf.anonymisation
            ? <View style={styles.footerField}>
              <Field
                props={{
                  label: I18n.t('RemainAnonymous'),
                  displayErrors: this.state.displayErrors,
                  errors,
                }}
                name="anonymous"
                component={renderCheckbox}
                type="boolean"
              />
            </View>
            : null}
          {actions.map((action, key) =>
            <ThemedButton
              primary
              key={key}
              disabled={this.state.sending || submitting}
              text={action.submissionTitle || action.title}
              value={`${action.processId}.${action.nodeId}`}
              raised
              onPress={() => this.handleSubmit(action)}
            />,
          )}
        </View>
      </View>
    )
  }
}

// Decorate the form component
const CreateIdeaReduxForm = reduxForm({
  form: 'createIdea', // a unique name for this form
})(DumbCreateIdeaForm)

const mapStateToProps = state => ({
  formData: state.form.createIdea,
  instance: state.instance,
  globalProps: state.globalProps,
})

const createPublishIdea = gql`
  mutation($text: String!, $title: String!, $keywords: [String]!, $attachedFiles: [Upload], $anonymous: Boolean) {
    createAndPublish(
      title: $title
      keywords: $keywords
      text: $text
      attachedFiles: $attachedFiles,
      anonymous: $anonymous
    ) {
      status
      idea {
        ...idea
      }
    }
  }
  ${ideaFragment}
`
const createIdea = gql`
  mutation($text: String!, $title: String!, $keywords: [String]!, $attachedFiles: [Upload], $anonymous: Boolean) {
    createIdea(
      title: $title,
      keywords: $keywords,
      text: $text,
      attachedFiles: $attachedFiles,
      anonymous: $anonymous) {
      status
      idea {
        ...idea
      }
    }
  }
  ${ideaFragment}
`

const CreateIdeaForm = graphql(createPublishIdea, {
  props({ ownProps, mutate }) {
    return {
      createAndPublish({ text, title, keywords, attachedFiles, anonymous, account }) {
        const { formData, instance } = ownProps
        const files =
          attachedFiles.length > 0
            ? formData.values.files.map(file => ({
              url: file.url,
              isImage: file.type === 'image',
              __typename: 'File',
            }))
            : []
        const createdAt = new Date()
        return mutate({
          variables: {
            text,
            title,
            keywords,
            attachedFiles,
            anonymous,
          },
          optimisticResponse: {
            __typename: 'Mutation',
            createAndPublish: {
              __typename: 'CreateAndPublish',
              status: true,
              idea: {
                __typename: 'Idea',
                id: '0',
                oid: '0',
                createdAt: createdAt.toISOString(),
                title,
                keywords,
                text,
                presentationText: text,
                attachedFiles: files,
                tokensSupport: 0,
                tokensOpposition: 0,
                userToken: null,
                state: instance.support_ideas ? ['submitted_support'] : ['published'],
                channel: {
                  __typename: 'Channel',
                  id: 'channel-id',
                  oid: 'channel-oid',
                },
                opinion: '',
                urls: [],
                author: {
                  __typename: 'Person',
                  isAnonymous: anonymous,
                  id: `${account.id}createidea`,
                  oid: `${account.oid}createidea`,
                  title: !anonymous ? account.title : 'Anonymous',
                  description: account.description,
                  function: account.function,
                  picture:
                    !anonymous && account.picture
                      ? {
                        __typename: 'File',
                        url: account.picture.url,
                      }
                      : null,
                },
                actions: [],
              },
            },
          },
          updateQueries: {
            IdeasList: (prev, { mutationResult }) => {
              const newIdea = mutationResult.data.createAndPublish.idea
              // if the idea is submitted to moderation
              if (newIdea.state.includes('submitted')) return prev
              return update(prev, {
                ideas: {
                  edges: {
                    $unshift: [
                      {
                        __typename: 'Idea',
                        node: newIdea,
                      },
                    ],
                  },
                },
              })
            },
            MyContents: (prev, { mutationResult }) => {
              const newIdea = mutationResult.data.createAndPublish.idea
              return update(prev, {
                account: {
                  contents: {
                    edges: {
                      $unshift: [
                        {
                          __typename: 'Idea',
                          node: newIdea,
                        },
                      ],
                    },
                  },
                },
              })
            },
          },
        })
      },
    }
  },
})(
  graphql(createIdea, {
    props({ ownProps, mutate }) {
      return {
        createIdea({ text, title, keywords, attachedFiles, anonymous, account }) {
          const { formData } = ownProps
          const files =
            attachedFiles.length > 0
              ? formData.values.files.map(file => ({
                url: file.url,
                isImage: file.type === 'image',
                __typename: 'File',
              }))
              : []
          const createdAt = new Date()
          return mutate({
            variables: {
              text,
              title,
              keywords,
              attachedFiles,
              anonymous,
            },
            optimisticResponse: {
              __typename: 'Mutation',
              createIdea: {
                __typename: 'CreateIdea',
                status: true,
                idea: {
                  __typename: 'Idea',
                  id: '0',
                  oid: '0',
                  createdAt: createdAt.toISOString(),
                  title,
                  keywords,
                  text,
                  presentationText: text,
                  attachedFiles: files,
                  tokensSupport: 0,
                  tokensOpposition: 0,
                  userToken: null,
                  state: ['to work'],
                  channel: {
                    __typename: 'Channel',
                    id: 'channel-id',
                    oid: 'channel-oid',
                  },
                  opinion: '',
                  urls: [],
                  author: {
                    __typename: 'Person',
                    isAnonymous: anonymous,
                    id: `${account.id}createidea`,
                    oid: `${account.oid}createidea`,
                    title: !anonymous ? account.title : 'Anonymous',
                    description: account.description,
                    function: account.function,
                    picture:
                      !anonymous && account.picture
                        ? {
                          __typename: 'File',
                          url: account.picture.url,
                        }
                        : null,
                  },
                  actions: [],
                },
              },
            },
            updateQueries: {
              MyContents: (prev, { mutationResult }) => {
                const newIdea = mutationResult.data.createIdea.idea
                return update(prev, {
                  account: {
                    contents: {
                      edges: {
                        $unshift: [
                          {
                            __typename: 'Idea',
                            node: newIdea,
                          },
                        ],
                      },
                    },
                  },
                })
              },
            },
          })
        },
      }
    },
  })(CreateIdeaReduxForm),
)

export default connect(mapStateToProps)(CreateIdeaForm)
