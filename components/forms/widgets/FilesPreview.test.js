import React from 'react'
import renderer from 'react-test-renderer'

import FilesPreview from './FilesPreview'

describe('FilesPreview component', () => {
  it('should match FilesPreview with data snapshot', () => {
    const props = {
      actions: [{ name: 'remove', icon: 'close-circle', color: 'gray' }],
      files: [
        {
          url: 'https://foo.bar',
          type: 'file',
          name: 'foo',
        },
      ],
    }
    const rendered = renderer.create(<FilesPreview {...props} />).toJSON()
    expect(rendered).toMatchSnapshot()
  })
})
