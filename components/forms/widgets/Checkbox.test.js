import React from 'react'
import renderer from 'react-test-renderer'

import Checkbox from './Checkbox'

describe('Checkbox component', () => {
  it('should match Checkbox with data snapshot', () => {
    const props = {
      value: true,
      title: 'Foo',
    }
    const rendered = renderer.create(<Checkbox {...props} />).toJSON()
    expect(rendered).toMatchSnapshot()
  })
})
