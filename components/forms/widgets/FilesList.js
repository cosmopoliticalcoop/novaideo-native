import React from 'react'
import { View, TouchableOpacity, StyleSheet } from 'react-native'
import { ImagePicker, DocumentPicker } from 'expo'

import Icon from '../../Icon'
import FilesPreview from './FilesPreview'

const styles = StyleSheet.create({
  container: { backgroundColor: '#fafafaff' },
  actionsContainer: {
    flexDirection: 'row',
    justifyContent: 'flex-end',
    marginTop: 5,
    marginBottom: 0,
  },
  icon: {
    marginLeft: 10,
  },
})

const options = {
  allowsEditing: true,
}

class FilesList extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      files: this.props.value || [],
    }
    this.onSelectImagePicker = this.onSelectImagePicker.bind(this)
    this.onSelectFilePicker = this.onSelectFilePicker.bind(this)
    this.onClear = this.onClear.bind(this)
    this.onActionPress = this.onActionPress.bind(this)
  }
  componentWillReceiveProps(nextProps) {
    this.setState({
      files: nextProps.value || [],
    })
  }
  onSelectImagePicker = async (type) => {
    const result =
      type === 'camera'
        ? await ImagePicker.launchCameraAsync(options)
        : await ImagePicker.launchImageLibraryAsync(options)
    if (!result.cancelled) {
      this.setState(
        {
          files: [
            ...this.state.files,
            { url: result.uri, type: 'image', name: result.uri.split('/').slice(-1)[0] },
          ],
        },
        () => this.props.onChange(this.state.files),
      )
    }
  }

  onSelectFilePicker = async () => {
    const result = await DocumentPicker.getDocumentAsync(options)
    if (result.type === 'success') {
      this.setState(
        {
          files: [...this.state.files, { url: result.uri, type: 'file', name: result.name }],
        },
        () => this.props.onChange(this.state.files),
      )
    }
  }

  onClear() {
    this.setState({ files: [] })
    this.props.onChange([])
  }

  onActionPress(action, file, index) {
    if (action.name === 'remove') {
      const newFiles = [...this.state.files]
      newFiles.splice(index, 1)
      this.setState({ files: newFiles }, () => this.props.onChange(this.state.files))
    }
  }
  render() {
    return (
      <View style={styles.container}>
        <View style={styles.actionsContainer}>
          <TouchableOpacity style={styles.icon} onPress={() => this.onSelectImagePicker('camera')}>
            <Icon name="camera" color="gray" size={25} />
          </TouchableOpacity>
          <TouchableOpacity style={styles.icon} onPress={() => this.onSelectImagePicker('library')}>
            <Icon name={'folder-image'} color="gray" size={25} />
          </TouchableOpacity>
          <TouchableOpacity style={styles.icon} onPress={this.onSelectFilePicker}>
            <Icon name={'file'} color="gray" size={23} />
          </TouchableOpacity>
          {this.state.files.length > 0
            ? <TouchableOpacity style={styles.icon} onPress={this.onClear}>
              <Icon name={'delete-forever'} color="#ff5722" size={25} />
            </TouchableOpacity>
            : null}
        </View>
        <FilesPreview
          files={this.state.files}
          actions={[{ name: 'remove', icon: 'close-circle', color: 'gray' }]}
          onActionPress={this.onActionPress}
        />
      </View>
    )
  }
}

export default FilesList
