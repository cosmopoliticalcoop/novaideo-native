import React from 'react'
import renderer from 'react-test-renderer'

import { DumbSelect } from './Select'
import { theme } from '../../../theme'

describe('Select component', () => {
  it('should match Select with data snapshot', () => {
    const options = [
      { title: 'Foo', id: 'foo', selected: true },
      { title: 'Bar', id: 'bar', selected: false },
    ]
    const props = {
      options,
      value: options,
      theme,
    }
    const rendered = renderer.create(<DumbSelect {...props} />).toJSON()
    expect(rendered).toMatchSnapshot()
  })
})
