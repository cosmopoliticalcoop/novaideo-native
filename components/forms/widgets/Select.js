/* eslint-disable react/no-array-index-key */
import React from 'react'
import { TextInput, View, TouchableOpacity, StyleSheet, Text, ScrollView } from 'react-native'
import Modal from 'react-native-modalbox'
import { withTheme } from 'glamorous-native'

import I18n from '../../../locale'
import Icon from '../../Icon'
import IconWithText from '../../IconWithText'
import SearchContainer, {
  InputContainer,
  IconContainer,
} from '../../styled-components/SearchContainer'

const styles = StyleSheet.create({
  container: {
    marginTop: 15,
  },
  modal: {
    height: '50%',
    padding: 20,
  },
  optionsContainer: {
    marginTop: 15,
  },
  option: {
    marginTop: 10,
  },
  selectedOptions: {
    flexDirection: 'row',
    flexWrap: 'wrap',
  },
  selectedOptionContainer: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 4,
    marginRight: 5,
    marginBottom: 5,
    padding: 2,
    paddingRight: 5,
    paddingLeft: 5,
    backgroundColor: 'gray',
  },
  selectedOption: {
    color: 'white',
    marginRight: 10,
  },
  activatorContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingTop: 5,
    paddingBottom: 5,
    marginRight: 10,
    borderBottomWidth: 0.9,
  },
})

export class DumbSelect extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      options: this.props.options,
      searchText: '',
    }
    this.modal = undefined
    this.toggleOption = this.toggleOption.bind(this)
    this.onChangeSearchText = this.onChangeSearchText.bind(this)
    this.addOption = this.addOption.bind(this)
  }

  onChangeSearchText(text) {
    this.setState({
      searchText: text,
    })
  }

  toggleOption(option) {
    this.setState(
      {
        options: this.state.options.map((item) => {
          if (item.id === option.id) {
            return { ...item, selected: !item.selected }
          }
          return item
        }),
      },
      () => this.props.onChange(this.state.options.filter(item => item.selected)),
    )
  }

  addOption(text) {
    this.setState(
      {
        options: [{ title: text, id: text, selected: true }, ...this.state.options],
        searchText: '',
      },
      () => this.props.onChange(this.state.options.filter(item => item.selected)),
    )
  }

  render() {
    const { theme } = this.props
    const options = this.state.options.filter(
      option => option.title.toLowerCase().search(this.state.searchText) >= 0,
    )
    return (
      <View style={styles.container}>
        {this.props.value.length > 0
          ? <Text style={{ color: this.props.labelColor }}>
            {this.props.label}
          </Text>
          : null}
        <TouchableOpacity onPress={() => this.modal.open()}>
          <View style={[styles.activatorContainer, { borderBottomColor: this.props.labelColor }]}>
            <View style={styles.selectedOptions}>
              {this.props.value.length === 0
                ? <Text style={{ color: this.props.labelColor }}>
                  {this.props.label}
                </Text>
                : null}
              {this.props.value.map((option, index) =>
                <View key={index} style={styles.selectedOptionContainer}>
                  <Text style={styles.selectedOption}>
                    {option.title}
                  </Text>
                  <TouchableOpacity onPress={() => this.toggleOption(option)}>
                    <Icon name="close" color="white" />
                  </TouchableOpacity>
                </View>,
              )}
            </View>
            <Icon name="chevron-down" size={22} color="gray" />
          </View>
        </TouchableOpacity>
        <Modal
          style={styles.modal}
          coverScreen
          swipeToClose
          position={'bottom'}
          ref={(modal) => {
            this.modal = modal
          }}
          onClosed={this.onClose}
          onOpened={this.onOpen}
          onClosingState={this.onClosingState}
        >
          <SearchContainer>
            <IconContainer>
              <Icon name="magnify" size={20} color={theme.primary.bgColor} />
            </IconContainer>
            <InputContainer>
              <TextInput
                style={{ color: theme.primary.bgColor }}
                returnKeyType="search"
                placeholder={I18n.t('search')}
                placeholderTextColor={theme.primary.lightColor}
                underlineColorAndroid="rgba(0,0,0,0)"
                onChangeText={this.onChangeSearchText}
                value={this.state.searchText}
              />
            </InputContainer>
            <IconContainer>
              {this.state.searchText && this.props.canAdd
                ? <TouchableOpacity onPress={() => this.addOption(this.state.searchText)}>
                  <Icon name="check" size={20} color="gray" />
                </TouchableOpacity>
                : null}
            </IconContainer>
          </SearchContainer>
          <ScrollView style={styles.optionsContainer}>
            {options.map((option, index) =>
              <TouchableOpacity
                style={styles.option}
                key={index}
                onPress={() => this.toggleOption(option, index)}
              >
                <IconWithText
                  name={
                    option.selected
                      ? 'checkbox-marked-circle-outline'
                      : 'checkbox-blank-circle-outline'
                  }
                  text={option.title}
                  iconSize={17}
                />
              </TouchableOpacity>,
            )}
          </ScrollView>
        </Modal>
      </View>
    )
  }
}

export default withTheme(DumbSelect)
