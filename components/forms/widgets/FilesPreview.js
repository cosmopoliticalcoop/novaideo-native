/* eslint-disable react/no-array-index-key */
import React from 'react'
import { View, Image, StyleSheet, TouchableOpacity } from 'react-native'

import Icon from '../../Icon'

const styles = StyleSheet.create({
  filesContainer: {
    flexDirection: 'row',
    flexWrap: 'wrap',
  },
  fileContainer: {
    width: 40,
    marginRight: 10,
    marginTop: 10,
    marginLeft: 10,
  },
  actionsContainer: {
    width: '100%',
    position: 'absolute',
    alignItems: 'flex-end',
    padding: 2,
    top: -10,
    right: -10,
  },
  action: {
    backgroundColor: '#fafafaff',
    borderRadius: 50,
  },
  file: {
    width: '100%',
    height: 50,
    borderRadius: 3,
    marginTop: 2,
    borderWidth: 0.5,
    borderColor: '#d6d7da',
  },
})

export default class FilesPreview extends React.Component {
  constructor(props) {
    super(props)
    this.renderImageItem = this.renderImageItem.bind(this)
    this.renderFileItem = this.renderFileItem.bind(this)
  }

  renderImageItem(file, key) {
    const actions = this.props.actions
    return (
      <View key={key} style={styles.fileContainer}>
        <Image source={{ uri: file.url }} style={styles.file} />
        <View style={styles.actionsContainer}>
          {actions.map((action, index) =>
            <TouchableOpacity
              key={index}
              onPress={() => this.props.onActionPress(action, file, key)}
            >
              <Icon
                style={styles.action}
                name={action.icon}
                size={25}
                color={action.color || 'gray'}
              />
            </TouchableOpacity>,
          )}
        </View>
      </View>
    )
  }

  renderFileItem(file, key) {
    const actions = this.props.actions
    return (
      <View key={key} style={styles.fileContainer}>
        <View style={styles.file}>
          <Icon name={'file-export'} size={45} color={'gray'} />
        </View>
        <View style={styles.actionsContainer}>
          {actions.map((action, index) =>
            <TouchableOpacity
              style={styles.action}
              key={index}
              onPress={() => this.props.onActionPress(action, file, key)}
            >
              <Icon name={action.icon} size={25} color={action.color || 'gray'} />
            </TouchableOpacity>,
          )}
        </View>
      </View>
    )
  }
  render() {
    const { files } = this.props
    if (files.length === 0) return <View />
    return (
      <View style={styles.filesContainer}>
        {files.map((file, key) => {
          if (file.type === 'image') {
            return this.renderImageItem(file, key)
          }
          return this.renderFileItem(file, key)
        })}
      </View>
    )
  }
}
