/* eslint-disable react/no-array-index-key */
import React from 'react'
import { View, TouchableOpacity, StyleSheet } from 'react-native'

import IconWithText from '../../IconWithText'

const styles = StyleSheet.create({
  container: {
    marginLeft: 5,
    marginTop: 7,
  },
})

class Checkbox extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      value: this.props.value || false,
    }
    this.onPress = this.onPress.bind(this)
  }
  onPress() {
    this.setState({ value: !this.state.value }, () => this.props.onChange(this.state.value))
  }
  render() {
    const { label, styleText, styleIcon, iconSize, iconColor } = this.props
    return (
      <View style={styles.container}>
        <TouchableOpacity style={styles.option} onPress={this.onPress}>
          <IconWithText
            name={
              this.state.value ? 'checkbox-marked-circle-outline' : 'checkbox-blank-circle-outline'
            }
            text={label}
            iconColor={iconColor}
            iconSize={iconSize}
            styleText={styleText}
            styleIcon={styleIcon}
          />
        </TouchableOpacity>
      </View>
    )
  }
}

export default Checkbox
