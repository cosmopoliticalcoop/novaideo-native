import React from 'react'
import renderer from 'react-test-renderer'

import FilesList from './FilesList'

describe('FilesList component', () => {
  it('should match FilesList with data snapshot', () => {
    const props = {
      value: [
        {
          url: 'https://foo.bar',
          type: 'file',
          name: 'foo',
        },
      ],
    }
    const rendered = renderer.create(<FilesList {...props} />).toJSON()
    expect(rendered).toMatchSnapshot()
  })
})
