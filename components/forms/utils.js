import React from 'react'

import Checkbox from './widgets/Checkbox'
import TextField from '../TextField'
import FilesList from './widgets/FilesList'
import Select from './widgets/Select'

export const renderInput = ({
  input: { name, value, onChange },
  errors,
  displayErrors,
  label,
  disabled,
  multiline,
  style,
  wrapperStyle,
  inputStyle,
  placeholder,
  placeholderTextColor,
}) => {
  const defaultColor = '#a2a4a2ff'
  const hasError = displayErrors && errors && name in errors
  const fieldStyle = style || {
    textInputColor: defaultColor,
    borderColor: defaultColor,
    highlightColor: defaultColor,
  }
  const textInputColor = hasError ? '#f00' : fieldStyle.textInputColor || defaultColor
  const highlightColor = hasError ? '#f00' : fieldStyle.highlightColor || defaultColor
  const borderColor = hasError ? '#f00' : fieldStyle.borderColor || defaultColor
  return (
    <TextField
      minHeight={22}
      underlineColorAndroid="transparent"
      placeholder={placeholder}
      placeholderTextColor={placeholderTextColor}
      wrapperStyle={wrapperStyle}
      inputStyle={inputStyle}
      label={hasError ? errors[name] : label}
      value={value}
      multiline={multiline}
      autoGrow={multiline}
      disabled={disabled}
      dense
      highlightColor={highlightColor}
      labelColor={textInputColor}
      borderColor={borderColor}
      textColor={fieldStyle.textColor}
      onChangeText={onChange}
      height={style && style.height}
    />
  )
}

export const renderFilesList = ({ input: { value, onChange }, navigation }) =>
  <FilesList navigation={navigation} value={value} onChange={onChange} />

export const renderSelect = ({
  input: { name, value, onChange },
  options,
  label,
  canAdd,
  errors,
  displayErrors,
  style,
}) => {
  const hasError = displayErrors && errors && name in errors
  const inputStyle = style || { labelColor: '#a2a4a2ff' }
  const labelColor = hasError ? '#f00' : inputStyle.labelColor || '#a2a4a2ff'
  return (
    <Select
      options={options}
      label={hasError ? errors[name] : label}
      labelColor={labelColor}
      canAdd={canAdd}
      value={value || []}
      onChange={onChange}
    />
  )
}

export const renderCheckbox = ({ input: { value, onChange }, label }) =>
  <Checkbox
    iconSize={15}
    styleText={{
      color: 'gray',
      marginLeft: 4,
      fontSize: 15,
    }}
    styleIcon={{
      marginTop: 2,
    }}
    iconColor="gray"
    label={label}
    value={value}
    onChange={onChange}
  />
