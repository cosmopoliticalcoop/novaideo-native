import React from 'react'
import ShallowRenderer from 'react-test-renderer/shallow'

import { DumbCommentForm } from './Comment'
import { theme } from '../../theme'

describe('DumbCommentForm component', () => {
  it('should match DumbCommentForm with data snapshot', () => {
    const props = {
      formData: {
        values: {
          comment: 'My comment text',
          files: [
            {
              url: 'https://foo.bar',
              type: 'file',
              name: 'foo',
            },
          ],
        },
      },
      theme,
      globalProps: {
        siteConf: { anonymisation: false },
      },
    }
    const renderer = new ShallowRenderer()
    renderer.render(<DumbCommentForm {...props} />)
    const rendered = renderer.getRenderOutput()
    expect(rendered).toMatchSnapshot()
  })
})
