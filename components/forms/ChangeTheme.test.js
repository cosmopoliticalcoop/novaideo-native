import React from 'react'
import ShallowRenderer from 'react-test-renderer/shallow'

import { DumbChangeTheme } from './ChangeTheme'

describe('DumbChangeTheme component', () => {
  it('should match DumbChangeTheme with data snapshot', () => {
    const renderer = new ShallowRenderer()
    renderer.render(<DumbChangeTheme {...{ history: {}, instance: { id: 'foobar' } }} />)
    const rendered = renderer.getRenderOutput()
    expect(rendered).toMatchSnapshot()
  })
})
