import React from 'react'
import ShallowRenderer from 'react-test-renderer/shallow'

import { DumbCreateIdeaForm } from './CreateIdea'
import { theme } from '../../theme'

describe('DumbCreateIdeaForm component', () => {
  it('should match DumbCreateIdeaForm with data snapshot', () => {
    const keywords = [{ title: 'Foo', id: 'foo' }, { title: 'Bar', id: 'bar' }]
    const props = {
      data: {},
      formData: {
        values: {
          title: 'My idea title',
          text: 'My idea text',
          keywords,
          files: [
            {
              url: 'https://foo.bar',
              type: 'file',
              name: 'foo',
            },
          ],
        },
      },
      navigation: {
        state: {
          params: {
            actions: [
              { title: 'Foo', submissionTitle: 'Send', processId: 'processid', nodeId: 'nodeid' },
            ],
          },
        },
      },
      theme,
      globalProps: {
        siteConf: { anonymisation: false, keywords: ['foo', 'bar'] },
      },
    }
    const renderer = new ShallowRenderer()
    renderer.render(<DumbCreateIdeaForm {...props} />)
    const rendered = renderer.getRenderOutput()
    expect(rendered).toMatchSnapshot()
  })
})
