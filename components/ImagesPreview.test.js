import React from 'react'
import renderer from 'react-test-renderer'

import ImagesPreview from './ImagesPreview'

describe('ImagesPreview component', () => {
  it('should match ImagesPreview with data snapshot', () => {
    const props = {
      images: [
        { url: 'https://foobar.com/image1' },
        { url: 'https://foobar.com/image2' },
        { url: 'https://foobar.com/image3' },
        { url: 'https://foobar.com/image4' },
      ],
    }
    const rendered = renderer.create(<ImagesPreview {...props} />).toJSON()
    expect(rendered).toMatchSnapshot()
  })
  it('should match ImagesPreview without images snapshot', () => {
    const props = {
      images: [],
    }
    const rendered = renderer.create(<ImagesPreview {...props} />).toJSON()
    expect(rendered).toMatchSnapshot()
  })
})
