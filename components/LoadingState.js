import React from 'react'
import { StyleSheet, Text, View, ActivityIndicator } from 'react-native'
import { LOADING_STATES } from '../constants'

const styles = StyleSheet.create({
  successMessage: {
    color: '#54902a',
    fontSize: 12,
  },
  errorMessage: {
    color: '#f00',
    fontSize: 12,
  },
})

const LoadingState = ({ state, style, indicatorColor, successText, errorText }) => {
  switch (state) {
    case LOADING_STATES.pending:
      return (
        <View style={style}>
          <ActivityIndicator color={indicatorColor} />
        </View>
      )
    case LOADING_STATES.error:
      return (
        <View style={style}>
          <Text style={styles.errorMessage}>
            {errorText}
          </Text>
        </View>
      )
    case LOADING_STATES.success:
      return (
        <View style={style}>
          <Text style={styles.successMessage}>
            {successText}
          </Text>
        </View>
      )
    default:
      return null
  }
}

export default LoadingState
