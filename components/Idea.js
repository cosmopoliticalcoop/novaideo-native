/* eslint-disable react/no-array-index-key */
import React from 'react'
import { View, Image, Text, StyleSheet, Linking, ScrollView, TouchableOpacity } from 'react-native'
import Moment from 'moment'
import Hyperlink from 'react-native-hyperlink'
import { graphql } from 'react-apollo'

import Icon from './Icon'
import Avatar from './Avatar'
import IconWithText from './IconWithText'
import ImagesPreview from './ImagesPreview'
import Evaluation from './Evaluation'
import Keywords from './Keywords'
import Url from './Url'
import { getActions } from '../utils'
import { Header, HeaderTitle, HeaderIcon } from './styled-components/HeaderNav'
import * as constants from '../constants'
import I18n from '../locale'
import { ideaQuery } from '../queries'

const styles = StyleSheet.create({
  ideaContainer: {
    paddingRight: 5,
    backgroundColor: '#fafafaff',
    width: '100%',
    flex: 1,
  },
  header: {
    flexDirection: 'row',
    alignItems: 'center',
    padding: 5,
  },
  headerAvatar: {
    justifyContent: 'space-around',
  },
  headerTitle: {
    fontSize: 13,
    color: '#999999ff',
    justifyContent: 'space-around',
    paddingLeft: 5,
  },
  headerAddOn: {
    fontSize: 10,
    color: '#999999ff',
    paddingLeft: 5,
  },
  body: {
    flex: 1,
    flexDirection: 'row',
    marginTop: -5,
  },
  bodyTitle: {
    width: '90%',
  },
  bodyLeft: {
    width: 50,
    justifyContent: 'center',
    alignItems: 'center',
    paddingTop: 10,
    paddingBottom: 10,
  },
  bodyContent: {
    paddingLeft: 5,
    width: '90%',
  },
  contentText: {
    width: '95%',
  },
  bodyFooter: {
    flexDirection: 'row',
    marginTop: 15,
  },
  actionsText: {
    fontSize: 14,
    color: '#585858',
    marginLeft: 8,
    marginRight: 50,
  },
  actionsIcon: {
    marginTop: 1,
  },
  sliderHeader: {
    marginLeft: 4,
    fontSize: 17,
    color: 'white',
    width: '90%',
  },
  urlsContainer: {
    paddingRight: 30,
    paddingLeft: 10,
    marginTop: 15,
  },
})

export class DumbIdea extends React.Component {
  static navigationOptions = {
    header: props =>
      <Header>
        <HeaderIcon>
          <TouchableOpacity
            onPress={() => {
              props.navigation.goBack()
            }}
            hitSlop={{ top: 15, bottom: 15, left: 15, right: 15 }}
          >
            <Icon name="arrow-left" size={22} color="white" />
          </TouchableOpacity>
        </HeaderIcon>
        <View>
          <HeaderTitle>
            {I18n.t('idea')}
          </HeaderTitle>
        </View>
      </Header>,
  }
  constructor(props) {
    super(props)
    this.state = {
      actionOpened: false,
      opened: false,
    }
    this.onActionClose = this.onActionClose.bind(this)
    this.onActionPress = this.onActionPress.bind(this)
    this.onUserPress = this.onUserPress.bind(this)
    this.onClose = this.onClose.bind(this)
  }

  componentWillUnmount() {
    this.props.navigation.state.params.onClose()
  }

  onActionClose() {
    if (this.state.actionOpened) {
      this.setState({ actionOpened: false })
    }
  }

  onActionPress(action) {
    const { data, navigation } = this.props
    navigation.state.params.performAction(action, data.idea, this)
  }

  onUserPress() {
    const { data, navigation } = this.props
    const user = data.idea.author
    if (!this.state.opened && !user.isAnonymous) {
      this.setState({ opened: true })
      navigation.navigate('User', {
        user,
        onClose: this.onClose,
        theme: navigation.state.params.theme,
      })
    }
  }

  onClose() {
    if (this.state.opened) {
      this.setState({ opened: false })
    }
  }

  render() {
    const { idea } = this.props.data
    const {
      instance,
      adapters,
      onClose,
      searchEntities,
      evaluationPress,
      getExaminationValue,
      getEvaluationActions,
      onActionLongPress,
    } = this.props.navigation.state.params
    const author = idea.author
    const authorPicture = author.picture
    const defaultPicture = author.isAnonymous
      ? constants.DEFAULT_ANONYMOUS_IMG
      : constants.DEFAULT_USER_IMG
    const createdAt = Moment(idea.createdAt).format(I18n.t('datetimeFormat'))
    const images = idea.attachedFiles ? idea.attachedFiles.filter(image => image.isImage) : []
    const Examination = adapters.examination
    const communicationActions = getActions(idea.actions, constants.ACTIONS.communicationAction)
    return (
      <ScrollView style={styles.ideaContainer}>
        <TouchableOpacity onPress={this.onUserPress} style={styles.header}>
          <Avatar
            size={40}
            style={styles.headerAvatar}
            image={
              <Image
                source={authorPicture ? { uri: `${authorPicture.url}/profil` } : defaultPicture}
              />
            }
          />
          <Text style={styles.headerTitle}>
            {author.title}
          </Text>
          <Text style={styles.headerAddOn}>
            {createdAt}
          </Text>
        </TouchableOpacity>
        <View style={styles.body}>
          <View style={styles.bodyLeft}>
            {instance.support_ideas && idea.state.includes('published')
              ? <Evaluation
                icon={{
                  top:
                      idea.userToken === 'support'
                        ? 'arrow-up-drop-circle-outline'
                        : 'arrow-up-drop-circle',
                  down:
                      idea.userToken === 'oppose'
                        ? 'arrow-down-drop-circle-outline'
                        : 'arrow-down-drop-circle',
                }}
                onPress={{ top: evaluationPress, down: evaluationPress }}
                onLongPress={{ top: onActionLongPress, down: onActionLongPress }}
                text={{ top: idea.tokensSupport, down: idea.tokensOpposition }}
                action={getEvaluationActions()}
                active={idea.state.includes('submitted_support')}
              />
              : undefined}
            {instance.examine_ideas && idea.state.includes('examined')
              ? <Examination message={idea.opinion} value={getExaminationValue()} />
              : undefined}
          </View>
          <View style={styles.bodyContent}>
            <View style={styles.bodyTitle}>
              <IconWithText name="lightbulb" text={idea.title} iconSize={17} />
            </View>
            <Keywords
              keywords={idea.keywords}
              onKeywordPress={searchEntities}
              navigation={this.props.navigation}
              closeParent={onClose}
              goBackOnPress
            />
            <Hyperlink
              onPress={url => Linking.openURL(url)}
              linkStyle={{ fontSize: 16, color: '#1990b8' }}
            >
              <Text style={styles.contentText}>
                {idea.text}
              </Text>
            </Hyperlink>
            <ImagesPreview
              navigation={this.props.navigation}
              images={images}
              sliderHeader={() =>
                <IconWithText
                  iconColor={'white'}
                  styleText={styles.sliderHeader}
                  name="lightbulb"
                  text={idea.title}
                  iconSize={17}
                  numberOfLines={1}
                />}
            />
            <View style={styles.urlsContainer}>
              {idea.urls.map((url, key) =>
                <Url navigation={this.props.navigation} key={key} data={url} onPressUrlImage />,
              )}
            </View>
            <View style={styles.bodyFooter}>
              {communicationActions.map((action, key) =>
                <TouchableOpacity
                  key={key}
                  onPress={() => this.onActionPress(action)}
                  onLongPress={() => onActionLongPress(action)}
                  hitSlop={{ top: 15, bottom: 15, left: 15, right: 15 }}
                >
                  <IconWithText
                    styleText={styles.actionsText}
                    styleIcon={styles.actionsIcon}
                    iconColor="#585858"
                    name={action.stylePicto}
                    text={action.counter}
                    iconSize={17}
                    numberOfLines={1}
                  />
                </TouchableOpacity>,
              )}
            </View>
          </View>
        </View>
        <View style={styles.bodyFooter} />
      </ScrollView>
    )
  }
}

export default graphql(ideaQuery, {
  options: props => ({
    fetchPolicy: 'cache-first',
    variables: { id: props.navigation.state.params.ideaId },
  }),
})(DumbIdea)
