import React from 'react'
import ShallowRenderer from 'react-test-renderer/shallow'

import { DumbComments } from './Comments'

describe('Comment component', () => {
  it('should match Comment with data snapshot', () => {
    const channel = {
      oid: 'foo',
      comments: { edges: [] },
      subject: {
        oid: 'bar',
        actions: [{ behaviorId: 'comment' }],
      },
    }
    const props = {
      navigation: {},
      data: { node: { ...channel } },
    }
    const renderer = new ShallowRenderer()
    renderer.render(<DumbComments {...props} />)
    const rendered = renderer.getRenderOutput()
    expect(rendered).toMatchSnapshot()
  })
  it('should match Comment snapshot (without comment action)', () => {
    const channel = {
      oid: 'foo',
      comments: { edges: [] },
      subject: {
        oid: 'bar',
        actions: [],
      },
    }
    const props = {
      navigation: {},
      data: { node: { ...channel } },
    }
    const renderer = new ShallowRenderer()
    renderer.render(<DumbComments {...props} />)
    const rendered = renderer.getRenderOutput()
    expect(rendered).toMatchSnapshot()
  })
})
