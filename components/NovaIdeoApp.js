/* eslint-disable react/no-did-mount-set-state */
// @flow
import React from 'react'
import { connect } from 'react-redux'
import { NetInfo, StyleSheet, View } from 'react-native'

import apolloClient from '../apolloClient'
import Font from './Font'
import AppForInstance from './AppForInstance'
import InstanceSelector from './InstanceSelector'
import Login from './Login'
import { configureNetworkInterface } from '../utils'
import {
  setConnectionState,
  userLogin,
  loadAdapters,
  logout,
  updateUserToken,
  updateGlobalProps,
} from '../actions'
import LoadingPage from './LoadingPage'
import { FONTS } from '../constants'

const styles = StyleSheet.create({
  instanceContainer: {
    flex: 1,
    backgroundColor: '#fafafaff',
  },
  selectorContainer: {
    flex: 1,
    justifyContent: 'center',
    backgroundColor: '#373a35',
  },
})

const configureClient = (instance: Object, user: Object) => {
  configureNetworkInterface(apolloClient, {
    uri: `${instance.url}/graphql`,
    token: user.token,
  })
}

export class DumbApp extends React.Component {
  static navigationOptions = {
    header: () => undefined,
  }

  constructor(props: any) {
    super(props)
    this.state = {
      requirementsLoaded: false,
    }
    this.handleConnectionChange = this.handleConnectionChange.bind(this)
  }

  state: { requirementsLoaded: boolean }

  componentWillMount() {
    if (this.props.instance.id) {
      configureClient(this.props.instance, this.props.user)
      // update adapters of the instance
      this.props.loadAdapters(this.props.instance.id)
    }
  }

  // $FlowFixMe
  async componentDidMount() {
    // we need the connection status
    const isConnected = await NetInfo.isConnected.fetch()
    NetInfo.isConnected.addEventListener('change', this.handleConnectionChange)
    await Font.loadAsync({
      'Roboto-Regular': FONTS.robotoRegular,
      'Roboto-Medium': FONTS.robotoMedium,
    })
    const { instance, user, network, history } = this.props
    // connect the user if he is not logged in (only if online).
    // When Offline mode is enabled (isConnected === false), we must display all of stored data
    const historyEntry = history[instance.id]
    let token = historyEntry ? historyEntry.data.token : user.token
    token = token || user.token
    if (isConnected && !network.isLogged && instance.id && token) {
      const reset = (newToken) => {
        apolloClient
          .resetStore()
          .then(() =>
            this.setState({ requirementsLoaded: true }, () =>
              this.props.updateUserToken(instance, newToken),
            ),
          )
      }
      this.props
        .userLogin(instance, '', '', token)
        .then(({ value }) => {
          // if login failed we must logout the user then update data. else we
          // need to update user data
          if (!value.status) {
            this.props.logout(instance).then(reset)
          } else {
            // update the user token (see the history reducer)
            reset(value.token)
          }
        })
        .catch(() => {
          this.props.logout(instance).then(reset)
        })
    } else {
      this.setState({ requirementsLoaded: true })
    }
    this.handleConnectionChange(isConnected)
  }

  componentWillReceiveProps(nextProps: any) {
    if (
      this.props.instance.id !== nextProps.instance.id ||
      this.props.user.token !== nextProps.user.token
    ) {
      configureClient(nextProps.instance, nextProps.user)
    }
    if (nextProps.navigation) {
      this.props.updateGlobalProps({
        navigation: nextProps.navigation,
      })
    }
  }

  componentWillUnmount() {
    NetInfo.isConnected.removeEventListener('change', this.handleConnectionChange)
  }

  handleConnectionChange = (isConnected: boolean) => {
    this.props.setConnectionState(isConnected)
  }

  render() {
    const { instance, user } = this.props
    if (!this.state.requirementsLoaded) return <LoadingPage />
    if (instance.id) {
      return (
        <View style={styles.instanceContainer}>
          {instance.isPrivate && !user.token ? <Login client={apolloClient} /> : <AppForInstance />}
        </View>
      )
    }
    return (
      <View style={styles.selectorContainer}>
        <InstanceSelector />
      </View>
    )
  }
}

const mapStateToProps = state => ({
  instance: state.instance,
  user: state.user,
  network: state.network,
  adapters: state.adapters,
  history: state.history,
})

export const mapDispatchToProps = {
  setConnectionState,
  userLogin,
  logout,
  loadAdapters,
  updateUserToken,
  updateGlobalProps,
}

export default connect(mapStateToProps, mapDispatchToProps)(DumbApp)
