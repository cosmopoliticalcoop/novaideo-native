import React from 'react'
import { StyleSheet, View } from 'react-native'

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
  },
  separator: {
    borderWidth: 0.3,
    width: '90%',
  },
})

class SeparatorComponent extends React.PureComponent {
  render() {
    const color = this.props.color
    return (
      <View style={styles.container}>
        <View style={[styles.separator, { borderColor: color || '#d6d7da' }]} />
      </View>
    )
  }
}

export default SeparatorComponent
