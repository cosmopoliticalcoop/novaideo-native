import React from 'react'
import ShallowRenderer from 'react-test-renderer/shallow'

import { DumbAppForInstance } from './AppForInstance'

describe('AppForInstance component', () => {
  it('should match snapshot (user logged)', () => {
    const renderer = new ShallowRenderer()
    renderer.render(<DumbAppForInstance network={{ isLogged: true }} />)
    const rendered = renderer.getRenderOutput()
    expect(rendered).toMatchSnapshot()
  })
  it('should match snapshot (user not logged)', () => {
    const renderer = new ShallowRenderer()
    renderer.render(<DumbAppForInstance network={{ isLogged: false }} />)
    const rendered = renderer.getRenderOutput()
    expect(rendered).toMatchSnapshot()
  })
})
