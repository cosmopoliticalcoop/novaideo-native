/* eslint-disable no-undef */
import React from 'react'
import { graphql } from 'react-apollo'
import { connect } from 'react-redux'

import Icon from './Icon'
import IdeaItem from './IdeaItem'
import EntitiesList from './EntitiesList'
import I18n from '../locale'
import { ideasListQuery } from '../queries'

export class DumbIdeasList extends React.Component {
  static navigationOptions = {
    tabBarIcon: ({ tintColor }) => <Icon size={18} name="home" color={tintColor} />,
  }

  componentWillReceiveProps(nextProps) {
    if (__DEV__) {
      Object.keys(nextProps).forEach((key) => {
        if (nextProps[key] !== this.props[key]) {
          console.log('IdeasList', 'prop changed:', key) // eslint-disable-line
        }
      })
    }
  }

  render() {
    const { data } = this.props
    if (__DEV__) {
      console.log('render IdeasList') // eslint-disable-line
    }
    return (
      <EntitiesList
        data={data}
        getEntities={entities => entities.ideas}
        noContentIcon="lightbulb"
        noContentMessage={I18n.t('noIdeas')}
        noContentFoundMessage={I18n.t('noIdeaFound')}
        offlineFilter={(entity, text) =>
          entity.node.title.toLowerCase().search(text) >= 0 ||
          entity.node.text.toLowerCase().search(text) >= 0 ||
          entity.node.keywords.join(' ').toLowerCase().search(text) >= 0}
        ListItem={IdeaItem}
      />
    )
  }
}

const IdeasListGQL = graphql(ideasListQuery, {
  options: props => ({
    notifyOnNetworkStatusChange: true,
    variables: { first: 15, after: '', filter: props.filter },
  }),
})(DumbIdeasList)

export const mapStateToProps = state => ({
  filter: state.search.text,
})

export default connect(mapStateToProps)(IdeasListGQL)
