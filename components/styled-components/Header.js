import glamorous from 'glamorous-native'

export const HeaderContainer = glamorous.view(
  {
    flexDirection: 'column',
    elevation: 3,
    shadowColor: '#000',
    shadowOpacity: 1.0,
  },
  (props, theme) => ({
    backgroundColor: theme.primary.bgColor,
  }),
)

export const Container = glamorous.view({
  flexDirection: 'row',
  justifyContent: 'space-between',
  alignItems: 'flex-end',
  padding: 5,
  paddingLeft: 5,
  paddingTop: 33,
  paddingBottom: 12,
})

export const MessageBox = glamorous.view(
  {
    flexDirection: 'row',
    justifyContent: 'space-between',
    padding: 5,
  },
  (props, theme) => ({
    backgroundColor: props.error ? theme.color.red : theme.color.gray,
  }),
)

export const MessageText = glamorous.text({
  color: 'white',
  fontSize: 12,
})

export const HeaderItemContainer = glamorous.view({
  marginLeft: 2,
})
