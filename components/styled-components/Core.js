import glamorous from 'glamorous-native'

const AppContainer = glamorous.view(
  {
    flex: 1,
  },
  (props, theme) => ({
    backgroundColor: theme.primary.appBgColor,
  }),
)

export const DrawerContainer = glamorous.view(
  {
    flex: 1,
  },
  (props, theme) => ({
    backgroundColor: theme.primary.bgColor,
  }),
)

export default AppContainer
