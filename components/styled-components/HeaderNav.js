import glamorous from 'glamorous-native'

export const Header = glamorous.view(
  {
    flexDirection: 'row',
    padding: 5,
    paddingLeft: 10,
    paddingTop: 33,
    paddingBottom: 12,
    shadowColor: '#000',
    shadowOpacity: 1.0,
    elevation: 3,
  },
  (props, theme) => ({
    backgroundColor: props.bgColor || theme.primary.bgColor,
  }),
)

export const HeaderIcon = glamorous.view({
  width: 30,
  marginLeft: 5,
  paddingTop: 3,
})

export const HeaderTitleContainer = glamorous.view({
  width: '85%',
})

export const HeaderTitle = glamorous.text({
  opacity: 0.8,
  color: 'white',
  fontSize: 19,
})
