import glamorous from 'glamorous-native'

const SearchContainer = glamorous.view({
  flexDirection: 'row',
  marginRight: 5,
  marginLeft: 10,
  paddingBottom: 0,
})

export const InputContainer = glamorous.view(
  {
    flex: 1,
    height: 25,
    paddingLeft: 10,
    borderBottomWidth: 0.5,
  },
  (props, theme) => ({
    borderBottomColor: theme.primary.lightColor,
  }),
)

export const IconContainer = glamorous.view(
  {
    height: 25,
    paddingTop: 2,
    borderBottomWidth: 0.5,
  },
  (props, theme) => ({
    borderBottomColor: theme.primary.lightColor,
  }),
)

export default SearchContainer
