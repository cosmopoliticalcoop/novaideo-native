import React from 'react'
import ShallowRenderer from 'react-test-renderer/shallow'

import { DumbEntitiesList } from './EntitiesList'
import Examination from './Examination'
import { theme } from '../theme'

describe('EntitiesList component', () => {
  it('should match EntitiesList loading snapshot', () => {
    const props = {
      data: {
        error: {},
        networkStatus: 1,
      },
      getEntities: entities => entities.ideas,
      theme,
    }

    const renderer = new ShallowRenderer()
    renderer.render(<DumbEntitiesList {...props} />)
    const rendered = renderer.getRenderOutput()
    expect(rendered).toMatchSnapshot()
  })

  it('should match EntitiesList with data snapshot', () => {
    const props = {
      data: {
        error: {},
        loading: false,
        ideas: {
          pageInfo: {
            endCursor: 'idea_id',
          },
          edges: [
            {
              node: {
                title: 'Foobar',
                keywords: ['k1', 'k2'],
                text: 'Idea text',
                createdAt: '2017-01-01T12:30:59',
                presentationText: 'Presentation text',
                state: ['published', 'submitted_support'],
                author: {
                  picture: {
                    url: 'http//example.nova-ideo.com/principals/users/JohnDoe/avatar.jpg',
                  },
                  title: 'John Doe',
                },
              },
            },
          ],
        },
      },
      adapters: {
        examination: Examination,
      },
      instance: {
        support_ideas: true,
      },
      getEntities: entities => entities.ideas,
      theme,
    }
    const renderer = new ShallowRenderer()
    renderer.render(<DumbEntitiesList {...props} />)
    const rendered = renderer.getRenderOutput()
    expect(rendered).toMatchSnapshot()
  })
})
