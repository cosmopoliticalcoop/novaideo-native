import React from 'react'
import ShallowRenderer from 'react-test-renderer/shallow'

import LoadingPage from './LoadingPage'

describe('LoadingPage component', () => {
  it('should match LoadingPage snapshot', () => {
    const renderer = new ShallowRenderer()
    renderer.render(<LoadingPage />)
    const rendered = renderer.getRenderOutput()
    expect(rendered).toMatchSnapshot()
  })
})
