import React from 'react'
import { View, Image, Text, StyleSheet, TouchableOpacity, Linking, BackHandler } from 'react-native'
import ResponsiveImage from 'react-native-responsive-image'

const styles = StyleSheet.create({
  container: {
    borderLeftColor: '#e8e8e8',
    borderLeftWidth: 3,
    paddingLeft: 10,
    marginBottom: 5,
  },
  header: {
    flexDirection: 'row',
    alignItems: 'center',
    padding: 5,
  },
  headerAvatar: {
    width: 15,
    height: 15,
    borderRadius: 3,
    borderWidth: 0.5,
    borderColor: '#d6d7da',
  },
  authorAvatar: {
    width: 20,
    height: 20,
    borderRadius: 3,
    borderWidth: 0.5,
    borderColor: '#d6d7da',
  },
  headerTitle: {
    fontSize: 13,
    color: '#999999ff',
    justifyContent: 'space-around',
    paddingLeft: 5,
  },
  contributionTitle: {
    fontSize: 13,
    paddingLeft: 5,
    width: '99%',
  },
  image: {
    width: 300,
    height: 150,
    borderRadius: 3,
    borderWidth: 0.5,
    borderColor: '#d6d7da',
    marginTop: 10,
    marginBottom: 10,
  },
  url: {
    color: '#337ab7',
  },
  description: {
    color: 'gray',
    paddingLeft: 5,
    fontSize: 12,
  },
  sliderHeader: {
    marginLeft: 4,
    fontSize: 17,
    color: 'white',
    width: '90%',
  },
  authorName: {
    color: 'gray',
    fontSize: 11,
    paddingLeft: 5,
  },
})

export default class Url extends React.Component {
  constructor(props) {
    super(props)
    this.handleClick = this.handleClick.bind(this)
    this.state = {
      opened: false,
    }
    this.onPress = this.onPress.bind(this)
    this.onClose = this.onClose.bind(this)
    this.getImageHeader = this.getImageHeader.bind(this)
  }

  componentDidMount() {
    this.subs = BackHandler.addEventListener('backPress', () =>
      this.setState(state => (state.opened ? { opened: false } : null)),
    )
  }

  componentWillUnmount() {
    if (this.subs) this.subs.remove()
  }

  onPress() {
    if (!this.state.opened) {
      this.setState({ opened: true })
      return this.props.navigation.navigate('ImagesSlider', {
        images: [{ url: this.props.data.imageUrl }],
        sliderHeader: this.getImageHeader,
        onClose: this.onClose,
      })
    }
    return undefined
  }

  onClose() {
    if (this.state.opened) {
      this.setState({ opened: false })
    }
  }

  getImageHeader() {
    const { data } = this.props
    if (data.authorName || data.authorAvatar) {
      return (
        <View style={styles.header}>
          <Image style={styles.authorAvatar} source={{ uri: data.authorAvatar }} />
          <Text numberOfLines={1} style={styles.sliderHeader}>
            {data.title}
          </Text>
        </View>
      )
    }
    return (
      <View style={styles.header}>
        <Image style={styles.headerAvatar} source={{ uri: data.favicon }} />
        <Text numberOfLines={1} style={styles.sliderHeader}>
          {data.title}
        </Text>
      </View>
    )
  }

  handleClick() {
    Linking.openURL(this.props.data.url)
  }

  render() {
    const { data } = this.props
    return (
      <View style={styles.container}>
        {data.authorName || data.authorAvatar
          ? <View style={styles.header}>
            <Image style={styles.authorAvatar} source={{ uri: data.authorAvatar }} />
            <TouchableOpacity onPress={this.handleClick}>
              <Text numberOfLines={1} style={styles.contributionTitle}>
                {data.title}
              </Text>
              <Text numberOfLines={1} style={styles.authorName}>
                {data.authorName}
              </Text>
            </TouchableOpacity>
          </View>
          : <View>
            <View style={styles.header}>
              <Image style={styles.headerAvatar} source={{ uri: data.favicon }} />
              <Text style={styles.headerTitle}>
                {data.siteName}
              </Text>
            </View>
            <TouchableOpacity onPress={this.handleClick}>
              <Text style={styles.url}>
                {data.title}
              </Text>
            </TouchableOpacity>
          </View>}
        <Text style={styles.description}>
          {data.description}
        </Text>
        <TouchableOpacity onPress={this.onPress}>
          <ResponsiveImage source={{ uri: data.imageUrl }} style={styles.image} />
        </TouchableOpacity>
        {data.authorName || data.authorAvatar
          ? <View style={styles.header}>
            <Image style={styles.headerAvatar} source={{ uri: data.favicon }} />
            <Text style={styles.headerTitle}>
              {data.siteName}
            </Text>
          </View>
          : undefined}
      </View>
    )
  }
}
