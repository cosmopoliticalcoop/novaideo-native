import React from 'react'
import renderer from 'react-test-renderer'

import { DumbIdea } from './Idea'
import Examination from './Examination'

describe('Idea component', () => {
  it('should match Idea with support snapshot', () => {
    const props = {
      navigation: {
        state: {
          params: {
            getEvaluationActions: () => ({ top: undefined, down: undefined }),
            ideaId: 'idea_id',
            instance: {
              support_ideas: true,
            },
            adapters: {
              examination: Examination,
            },
          },
        },
      },
      data: {
        error: undefined,
        idea: {
          id: 'idea_id',
          title: 'Foobar',
          text: 'Text idea',
          createdAt: '2017-01-01T12:30:59',
          keywords: ['key1', 'key2'],
          state: ['published', 'submitted_support'],
          urls: [],
          actions: [],
          author: {
            title: 'Alice',
            picture: {
              url: 'https://foobar.com/Alice.png',
            },
          },
        },
      },
    }
    const rendered = renderer.create(<DumbIdea {...props} />).toJSON()
    expect(rendered).toMatchSnapshot()
  })

  it('should match Idea without support snapshot', () => {
    const props = {
      navigation: {
        state: {
          params: {
            getEvaluationActions: () => ({ top: undefined, down: undefined }),
            ideaId: 'idea_id',
            instance: {
              support_ideas: false,
            },
            adapters: {
              examination: Examination,
            },
          },
        },
      },
      data: {
        error: undefined,
        idea: {
          id: 'idea_id',
          title: 'Foobar',
          text: 'Text idea',
          createdAt: '2017-01-01T12:30:59',
          keywords: ['key1', 'key2'],
          state: ['published', 'examined', 'favorable'],
          urls: [],
          actions: [],
          author: {
            title: 'Alice',
            picture: {
              url: 'https://foobar.com/Alice.png',
            },
          },
        },
      },
    }
    const rendered = renderer.create(<DumbIdea {...props} />).toJSON()
    expect(rendered).toMatchSnapshot()
  })
})
