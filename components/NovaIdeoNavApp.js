/* eslint-disable react/no-did-mount-set-state */
// @flow
import React from 'react'
import { connect } from 'react-redux'
import { View, TouchableOpacity } from 'react-native'
import { StackNavigator } from 'react-navigation'
import { ThemeProvider } from 'glamorous-native'

import Login from './Login'
import Icon from './Icon'
import ImagesSlider from './ImagesSlider'
import Idea from './Idea'
import App from './NovaIdeoApp'
// import getAllAdapters from './components/vendor/utils'
import CreateIdea from './forms/CreateIdea'
import Comments from './Comments'
import Comment from './Comment'
import User from './User'
import ChangeTheme from './forms/ChangeTheme'
import InstanceSwitch from './InstanceSwitch'
import { Header, HeaderTitle, HeaderIcon } from './styled-components/HeaderNav'
import { updateGlobalProps } from '../actions'

export const navigationOptions = {
  header: (props: any) =>
    <Header>
      <HeaderIcon>
        <TouchableOpacity
          onPress={() => {
            const onClose = props.scene.route.params.onClose
            if (onClose) onClose()
            props.navigation.goBack()
          }}
          hitSlop={{ top: 15, bottom: 15, left: 15, right: 15 }}
        >
          <Icon
            name="arrow-left"
            size={22}
            color="white"
            hitSlop={{ top: 15, bottom: 15, left: 15, right: 15 }}
          />
        </TouchableOpacity>
      </HeaderIcon>
      <View>
        <HeaderTitle>
          {props.scene.route.params.title}
        </HeaderTitle>
      </View>
    </Header>,
}

const NovaIdeoApp = StackNavigator(
  {
    Home: { screen: App },
    Idea: { screen: Idea },
    Login: { screen: Login },
    ImagesSlider: { screen: ImagesSlider },
    CreateIdea: { screen: CreateIdea },
    Comments: { screen: Comments },
    Comment: { screen: Comment },
    ChangeTheme: { screen: ChangeTheme },
    InstanceSwitch: { screen: InstanceSwitch },
    User: { screen: User },
  },
  { navigationOptions },
)

export class DumbNovaIdeoNavApp extends React.Component {
  componentWillReceiveProps(nextProps: any) {
    if (nextProps.rootProps) {
      this.props.updateGlobalProps(nextProps.rootProps)
    }
  }

  render() {
    const { instance, history, adapters } = this.props
    const historyEntry = history[instance.id]
    const appColor = historyEntry ? historyEntry.userPreferences.appColor : undefined
    return (
      <ThemeProvider theme={adapters.theme(appColor)}>
        <NovaIdeoApp />
      </ThemeProvider>
    )
  }
}

const mapStateToProps = state => ({
  adapters: state.adapters,
  instance: state.instance,
  history: state.history,
})

export const mapDispatchToProps = {
  updateGlobalProps,
}

export default connect(mapStateToProps, mapDispatchToProps)(DumbNovaIdeoNavApp)
