import React from 'react'
import ActionButton from 'react-native-action-button'
import { withTheme } from 'glamorous-native'

import Icon from './Icon'

const MyActionButton = (props) => {
  const { theme, iconName, ...rest } = props
  const { color, bgColor } = theme.primary
  return (
    <ActionButton
      buttonColor={bgColor}
      offsetX={20}
      offsetY={20}
      icon={<Icon name={iconName} size={20} color={color} />}
      {...rest}
    />
  )
}

const ThemedActionButton = withTheme(MyActionButton)
export default ThemedActionButton
