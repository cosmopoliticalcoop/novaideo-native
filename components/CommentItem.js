/* eslint-disable react/no-array-index-key */
import React from 'react'
import { View, Image, Text, TouchableOpacity, StyleSheet, Linking, BackHandler } from 'react-native'
import Moment from 'moment'
import Hyperlink from 'react-native-hyperlink'
import { connect } from 'react-redux'
import HTMLView from 'react-native-htmlview'
import Emoji from 'node-emoji'

import Avatar from './Avatar'
import ImagesPreview from './ImagesPreview'
import IconWithText from './IconWithText'
import { searchEntities } from '../actions'
import Url from './Url'
import I18n from '../locale'
import * as constants from '../constants'

const styles = StyleSheet.create({
  header: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
    padding: 5,
    paddingLeft: 10,
  },
  headerTitle: {
    fontSize: 14,
    fontWeight: 'bold',
    justifyContent: 'space-around',
    paddingLeft: 10,
  },
  headerAddOn: {
    fontSize: 10,
    color: '#999999ff',
    paddingLeft: 5,
  },
  body: {
    flex: 1,
    flexDirection: 'row',
    marginTop: -5,
  },
  bodyTitle: {
    width: '90%',
  },
  bodyLeft: {
    width: 60,
    justifyContent: 'center',
    alignItems: 'center',
  },
  bodyContent: {
    width: '85%',
    justifyContent: 'space-between',
  },
  contentText: {
    width: '95%',
  },
  sliderHeader: {
    marginLeft: 4,
    fontSize: 17,
    color: 'white',
    width: '90%',
  },
  urlsContainer: {
    paddingRight: 30,
    paddingLeft: 10,
    marginTop: 15,
  },
  bodyFooter: {
    flexDirection: 'row',
    justifyContent: 'flex-end',
  },
  actionsText: {
    fontSize: 14,
    color: 'gray',
    marginLeft: 8,
    marginRight: 50,
  },
  actionsIcon: {
    marginTop: 1,
  },
})

export class DumbCommentItem extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      opened: false,
    }
    this.onPress = this.onPress.bind(this)
    this.onUserPress = this.onUserPress.bind(this)
    this.onClose = this.onClose.bind(this)
  }

  componentDidMount() {
    this.subs = BackHandler.addEventListener('backPress', () =>
      this.setState(state => (state.opened ? { opened: false } : null)),
    )
  }

  componentWillUnmount() {
    if (this.subs) this.subs.remove()
  }

  onPress() {
    if (!this.state.opened) {
      this.setState({ opened: true })
      return this.props.globalProps.navigation.navigate('Comment', {
        id: this.props.node.id,
        comment: this.props.node,
        onClose: this.onClose,
        markCommentsAsRead: this.props.itemdata.markCommentsAsRead,
        theme: this.props.theme,
      })
    }
    return undefined
  }

  onUserPress() {
    const { node, globalProps: { navigation } } = this.props
    const user = node.author
    if (!this.state.opened && !user.isAnonymous) {
      this.setState({ opened: true })
      navigation.navigate('User', {
        user,
        onClose: this.onClose,
        theme: this.props.theme,
      })
    }
  }

  onClose() {
    if (this.state.opened) {
      this.setState({ opened: false })
    }
  }

  render() {
    const { node, itemdata, globalProps } = this.props
    const unreadComments = itemdata.channel
      ? itemdata.channel.unreadComments.map(comment => comment.id)
      : []
    const isUnread = unreadComments.includes(node.id)
    const author = node.author
    const authorPicture = author.picture
    const defaultPicture = author.isAnonymous
      ? constants.DEFAULT_ANONYMOUS_IMG
      : constants.DEFAULT_USER_IMG
    const createdAt = Moment(node.createdAt).format(I18n.t('datetimeFormat'))
    const images = node.attachedFiles ? node.attachedFiles.filter(image => image.isImage) : []
    return (
      <TouchableOpacity onPress={this.onPress}>
        <TouchableOpacity onPress={this.onUserPress} style={styles.header}>
          <Avatar
            size={40}
            image={
              <Image
                source={authorPicture ? { uri: `${authorPicture.url}profil` } : defaultPicture}
              />
            }
          />
          <Text style={[styles.headerTitle, isUnread ? { color: '#ef6e18' } : {}]}>
            {author.title}
          </Text>
          <Text style={[styles.headerAddOn, isUnread ? { color: '#ef6e18' } : {}]}>
            {createdAt}
          </Text>
        </TouchableOpacity>
        <View style={styles.body}>
          <View style={styles.bodyLeft} />
          <View style={styles.bodyContent}>
            <View>
              <Hyperlink
                onPress={url => Linking.openURL(url)}
                linkStyle={{ fontSize: 16, color: '#1990b8' }}
              >
                <View style={styles.contentText}>
                  <HTMLView value={Emoji.emojify(node.text)} />
                </View>
              </Hyperlink>
              <ImagesPreview
                navigation={globalProps.navigation}
                images={images}
                sliderHeader={() =>
                  <IconWithText
                    iconColor={'white'}
                    styleText={styles.sliderHeader}
                    name="comment-outline"
                    text={node.author.title}
                    iconSize={17}
                    numberOfLines={1}
                  />}
              />
            </View>
            <View style={styles.urlsContainer}>
              {node.urls.map((url, key) =>
                <Url navigation={globalProps.navigation} key={key} data={url} onPressUrlImage />,
              )}
            </View>
            {node.lenComments > 0
              ? <View style={styles.bodyFooter}>
                <IconWithText
                  styleText={styles.actionsText}
                  styleIcon={styles.actionsIcon}
                  name="comment-multiple-outline"
                  iconSize={15}
                  iconColor="gray"
                  text={`${node.lenComments} ${I18n.t(
                      `reply${node.lenComments > 1 ? '*' : ''}`,
                    )}`}
                  numberOfLines={1}
                />
              </View>
              : null}
          </View>
        </View>
      </TouchableOpacity>
    )
  }
}

export const mapDispatchToProps = { searchEntities }

export const mapStateToProps = state => ({
  globalProps: state.globalProps,
  instance: state.instance,
})

export default connect(mapStateToProps, mapDispatchToProps)(DumbCommentItem)
