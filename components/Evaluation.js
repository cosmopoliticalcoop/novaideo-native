import React from 'react'
import { View, Text, StyleSheet, TouchableOpacity, Platform } from 'react-native'

import Icon from './Icon'

const styles = StyleSheet.create({
  tokenContainer: {
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'center',
    paddingTop: 5,
    paddingBottom: 5,
    marginBottom: 10,
  },
  tokenTop: {
    color: '#4eaf4e',
    textShadowColor: 'gray',
    textShadowOffset: { width: -1, height: 1 },
    textShadowRadius: Platform.OS === 'android' ? 7 : 2,
  },
  tokenBottom: {
    color: '#ef6e18',
    textShadowColor: 'gray',
    textShadowOffset: { width: -1, height: 1 },
    textShadowRadius: Platform.OS === 'android' ? 7 : 2,
  },
  tokenNbBottom: {
    color: '#ef6e18',
    fontWeight: 'bold',
  },
  tokenNbTop: {
    color: '#4eaf4e',
    fontWeight: 'bold',
  },
})

const inactiveColor = '#a9a9a9'

const Evaluation = ({ icon, text, action, onPress, onLongPress, active }) => {
  if (active) {
    return (
      <View style={styles.tokenContainer}>
        <TouchableOpacity
          onPress={() => onPress.top(action.top)}
          onLongPress={() => onLongPress.top(action.top)}
          hitSlop={{ top: 15, bottom: 15, left: 15, right: 15 }}
        >
          <Icon style={styles.tokenTop} name={icon.top} size={35} />
        </TouchableOpacity>
        <Text style={styles.tokenNbTop}>
          {text.top}
        </Text>
        <Text style={styles.tokenNbBottom}>
          {text.down}
        </Text>
        <TouchableOpacity
          onPress={() => onPress.down(action.down)}
          onLongPress={() => onLongPress.down(action.down)}
          hitSlop={{ top: 15, bottom: 15, left: 15, right: 15 }}
        >
          <Icon style={styles.tokenBottom} name={icon.down} size={35} />
        </TouchableOpacity>
      </View>
    )
  }
  return (
    <View style={styles.tokenContainer}>
      <Icon style={[styles.tokenTop, { color: inactiveColor }]} name={icon.top} size={30} />
      <Text style={[styles.tokenNbTop, { color: inactiveColor }]}>
        {text.top}
      </Text>
      <Text style={[styles.tokenNbBottom, { color: inactiveColor }]}>
        {text.down}
      </Text>
      <Icon style={[styles.tokenBottom, { color: inactiveColor }]} name={icon.down} size={30} />
    </View>
  )
}

export default Evaluation
