/* eslint-disable react/no-array-index-key */
import React from 'react'
import { View, TouchableOpacity } from 'react-native'
import { graphql } from 'react-apollo'
import debounce from 'lodash.debounce'
import { connect } from 'react-redux'

import Icon from './Icon'
import EntitiesList from './EntitiesList'
import CommentItem from './CommentItem'
import I18n from '../locale'
import { commentQuery } from '../queries'
import { Header, HeaderTitle, HeaderIcon } from './styled-components/HeaderNav'
import Comment from './forms/Comment'
import AppContainer from './styled-components/Core'

export class DumbComment extends React.Component {
  static navigationOptions = {
    header: props =>
      <Header>
        <HeaderIcon>
          <TouchableOpacity
            onPress={() => {
              props.navigation.goBack()
            }}
            hitSlop={{ top: 15, bottom: 15, left: 15, right: 15 }}
          >
            <Icon
              name="arrow-left"
              size={22}
              hitSlop={{ top: 15, bottom: 15, left: 15, right: 15 }}
              color="white"
            />
          </TouchableOpacity>
        </HeaderIcon>
        <View>
          <HeaderTitle>
            {I18n.t('thread')}
          </HeaderTitle>
        </View>
      </Header>,
  }
  componentDidUpdate() {
    const { network, data } = this.props
    if (network.isLogged && data.node && data.node.channel.unreadComments.length > 0) {
      const markCommentsAsRead = this.props.navigation.state.params.markCommentsAsRead
      debounce(() => markCommentsAsRead(data.node.channel), 400)()
    }
  }
  componentWillUnmount() {
    this.props.navigation.state.params.onClose()
  }
  render() {
    const { data, navigation } = this.props
    if (data.error) {
      // the fact of checking data.error remove the Unhandled (in react-apollo)
      // ApolloError error when the graphql server is down
      // Do nothing
    }
    let commentdata = data
    const { comment } = navigation.state.params
    const commentId = comment.id
    if (data.node && data.node.id !== commentId) {
      // There seems to be an issue with react-apollo when offline. We can get
      // a wrong result, we get the previous clicked comment when we were online.
      // The issue may be related to the reuse of the same queryObservable
      // for different Comment component instances.
      // We need to investigate this.
      // Related code: https://github.com/apollographql/react-apollo/blob/93ba6ee34edc664c788a29343c56e4033b0807ff/src/graphql.tsx#L560
      commentdata = { ...data, node: { oid: comment.oid, comments: { edges: [] } }, actions: [] }
    }
    const commentAction = commentdata.node
      ? commentdata.node.actions.filter(action => action.nodeId === 'respond')[0]
      : null

    return (
      <AppContainer>
        <EntitiesList
          data={commentdata}
          inverted
          withoutSeparator
          getEntities={entities => (entities.node ? entities.node.comments : undefined)}
          noContentIcon="comment-multiple-outline"
          noContentMessage={I18n.t('noReply')}
          offlineFilter={(entity, text) => entity.node.text.toLowerCase().search(text) >= 0}
          ListItem={CommentItem}
          itemdata={{
            channel: commentdata.node ? commentdata.node.channel : null,
            markCommentsAsRead: navigation.state.params.markCommentsAsRead,
          }}
        />
        {commentAction
          ? <Comment
            action={commentAction}
            rootContext={commentdata.node.rootOid}
            context={commentdata.node.oid}
            channel={commentdata.node.channel}
          />
          : null}
      </AppContainer>
    )
  }
}

const DumbCommentGQL = graphql(commentQuery, {
  options: props => ({
    fetchPolicy: 'cache-and-network',
    variables: {
      id: props.navigation.state.params.comment.id,
      first: 15,
      after: '',
      filter: '',
      processId: '',
      nodeIds: ['respond'],
    },
  }),
})(DumbComment)

export const mapStateToProps = state => ({
  network: state.network,
})

export default connect(mapStateToProps)(DumbCommentGQL)
