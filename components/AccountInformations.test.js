import React from 'react'
import ShallowRenderer from 'react-test-renderer/shallow'

import AccountInformations from './AccountInformations'
import { theme } from '../theme'

describe('AccountInformations component', () => {
  it('should match AccountInformations with data snapshot', () => {
    const instance = {
      id: 'foobar',
      url: 'https://foobar.com/logo.png/',
      isPrivate: true,
      logo: 'https://foobar.com/logo.png/',
      title: 'Foobar',
      loadingState: 'completed',
    }
    const props = {
      instance,
      user: { token: 'usertoken' },
      theme,
      globalProps: {
        account: {
          title: 'Alice',
          picture: {
            url: 'https://foobar.com/Alice.png',
          },
        },
      },
    }

    const renderer = new ShallowRenderer()
    renderer.render(<AccountInformations {...props} />)
    const rendered = renderer.getRenderOutput()
    expect(rendered).toMatchSnapshot()
  })
  it('should match AccountInformations with data snapshot', () => {
    const instance = {
      id: 'foobar',
      url: 'https://foobar.com/logo.png/',
      isPrivate: true,
      logo: 'https://foobar.com/logo.png/',
      title: 'Foobar',
      loadingState: 'completed',
    }
    const props = {
      instance,
      user: { token: undefined },
      theme,
      globalProps: {},
    }
    const renderer = new ShallowRenderer()
    renderer.render(<AccountInformations {...props} />)
    const rendered = renderer.getRenderOutput()
    expect(rendered).toMatchSnapshot()
  })
})
