import React from 'react'
import renderer from 'react-test-renderer'
import { ThemeProvider } from 'glamorous-native'

import { theme } from '../theme'
import * as actions from '../actions'
// import { mapDispatchToProps } from './InstanceSelector'
import { DumbInstanceSelector, mapDispatchToProps } from './InstanceSelector'

describe('InstanceSelector component', () => {
  const updateInstanceSpy = jest.fn(() => {})
  const network = { isConnected: true, url: { error: false } }
  it('should match snapshot', () => {
    const rendered = renderer
      .create(
        <ThemeProvider theme={theme}>
          <DumbInstanceSelector network={network} updateInstance={updateInstanceSpy} />
        </ThemeProvider>,
      )
      .toJSON()
    expect(rendered).toMatchSnapshot()
  })
})

describe('InstanceSelector container', () => {
  describe('mapDispatchToProps', () => {
    it('should map setInstance action to updateInstance prop', () => {
      const expected = actions.setInstance('foobar')
      expect(mapDispatchToProps.setInstance('foobar')).toEqual(expected)
    })
  })
})
