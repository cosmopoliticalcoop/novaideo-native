import React from 'react'
import ShallowRenderer from 'react-test-renderer/shallow'

import { DumbPublicChannels } from './PublicChannels'

describe('PublicChannels component', () => {
  it('should match PublicChannels with data snapshot', () => {
    const props = {
      navigation: {},
    }
    const renderer = new ShallowRenderer()
    renderer.render(<DumbPublicChannels {...props} />)
    const rendered = renderer.getRenderOutput()
    expect(rendered).toMatchSnapshot()
  })
})
