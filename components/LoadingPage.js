import React from 'react'
import { StyleSheet, Animated, Easing } from 'react-native'

import * as constants from '../constants'
import NovaIdeoLogo from '../novaideo_logo.png'
import AppContainer from './styled-components/Core'

const styles = StyleSheet.create({
  container: {
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
  },
  logo: {
    width: constants.HOME_IMG_HEIGHT,
    height: constants.HOME_IMG_HEIGHT,
  },
})

export default class LoadingPage extends React.Component {
  constructor() {
    super()
    this.angle = new Animated.Value(0)
    this.restart = this.restart.bind(this)
    this.rotateLogo = this.rotateLogo.bind(this)
  }
  componentDidMount() {
    this.rotateLogo()
  }
  restart() {
    this.angle.setValue(0)
  }
  rotateLogo() {
    this.restart()
    Animated.timing(this.angle, {
      toValue: 100,
      duration: 1000,
      easing: Easing.linear,
    }).start(this.rotateLogo)
  }
  render() {
    const rotate = this.angle.interpolate({
      inputRange: [0, 100],
      outputRange: ['0deg', '360deg'],
    })
    return (
      <AppContainer style={styles.container}>
        <Animated.Image source={NovaIdeoLogo} style={[styles.logo, { transform: [{ rotate }] }]} />
      </AppContainer>
    )
  }
}
