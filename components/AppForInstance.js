import React from 'react'
import { StyleSheet } from 'react-native'
import { MenuContext } from 'react-native-menu'
import { TabNavigator } from 'react-navigation'
import { connect } from 'react-redux'
import { withTheme } from 'glamorous-native'
import DrawerLayout from 'react-native-drawer-layout-polyfill'
import { graphql } from 'react-apollo'

import IdeasList from './IdeasList'
import MyContents from './MyContents'
import MySupports from './MySupports'
import MyFollowings from './MyFollowings'
import Header from './Header'
import CreateIdeaButton from './CreateIdeaButton'
import Channels from './Channels'
import { accountQuery } from '../queries'
import { updateGlobalProps } from '../actions'

const styles = StyleSheet.create({
  createIdeaButton: {
    alignSelf: 'flex-end',
  },
  root: {
    flex: 1,
  },
})

// we use connect here to have a pure component, and to not create a different
// Nav class each time
export const NavBar = withTheme(
  connect()((props) => {
    const { theme, instance } = props
    const tabs = {
      Ideas: { screen: IdeasList },
      MyContent: { screen: MyContents },
    }
    if (instance.support_ideas) tabs.MySupports = { screen: MySupports }
    tabs.MyFollowings = { screen: MyFollowings }
    const Nav = TabNavigator(tabs, {
      tabBarOptions: {
        activeTintColor: theme.primary.bgColor,
        inactiveTintColor: '#d2d2d2',
        showLabel: false,
        showIcon: true,
        style: {
          backgroundColor: 'white',
          elevation: 2,
          borderBottomWidth: 0.5,
          borderBottomColor: '#b3b3b3',
        },
        labelStyle: {
          fontSize: 17,
        },
        indicatorStyle: {
          backgroundColor: theme.primary.bgColor,
        },
      },
    })
    return <Nav />
  }),
)

export class DumbAppForInstance extends React.Component {
  constructor(props) {
    super(props)
    this.drawerLayout = undefined
    this.openDrawer = this.openDrawer.bind(this)
    this.renderNavigationView = this.renderNavigationView.bind(this)
  }
  componentWillReceiveProps(nextProps) {
    const { data } = nextProps
    this.props.updateGlobalProps({
      account: data.account,
      siteConf: data.root,
      openDrawer: this.openDrawer,
    })
  }
  openDrawer() {
    this.drawerLayout.openDrawer()
  }
  renderNavigationView() {
    const { network } = this.props
    return network.isLogged ? <Channels /> : null
  }
  render() {
    const { instance, network } = this.props
    return (
      <DrawerLayout
        drawerWidth={300}
        keyboardDismissMode="on-drag"
        drawerPosition={DrawerLayout.positions.Left}
        renderNavigationView={this.renderNavigationView}
        drawerLockMode={network.isLogged ? 'unlocked' : 'locked-closed'}
        ref={(layout) => {
          this.drawerLayout = layout
        }}
      >
        <MenuContext style={styles.root}>
          <Header />
          {network.isLogged ? <NavBar {...{ instance }} /> : <IdeasList />}
          {network.isLogged ? <CreateIdeaButton style={styles.createIdeaButton} /> : null}
        </MenuContext>
      </DrawerLayout>
    )
  }
}

export const mapDispatchToProps = {
  updateGlobalProps,
}

export const mapStateToProps = state => ({
  instance: state.instance,
  network: state.network,
})

export default connect(mapStateToProps, mapDispatchToProps)(
  graphql(accountQuery, {
    options: () => ({
      fetchPolicy: 'cache-first',
    }),
  })(DumbAppForInstance),
)
