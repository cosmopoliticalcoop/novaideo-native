/* eslint-disable no-confusing-arrow */
import React from 'react'
import { StyleSheet, TouchableOpacity } from 'react-native'
import { gql, graphql } from 'react-apollo'
import update from 'immutability-helper'
import debounce from 'lodash.debounce'
import { connect } from 'react-redux'

import Icon from './Icon'
import IconWithText from './IconWithText'
import EntitiesList from './EntitiesList'
import CommentItem from './CommentItem'
import { commentsQuery } from '../queries'
import I18n from '../locale'
import { Header, HeaderIcon, HeaderTitleContainer } from './styled-components/HeaderNav'
import AppContainer from './styled-components/Core'
import Comment from './forms/Comment'

const styles = StyleSheet.create({
  headerTitle: {
    opacity: 0.8,
    color: 'white',
    paddingLeft: 4,
    fontSize: 19,
  },
  styleIcon: {
    marginTop: 5,
  },
})

const commentsActions = ['comment', 'general_discuss', 'discuss']

export class DumbComments extends React.Component {
  static navigationOptions = {
    header: props => (
      <Header>
        <HeaderIcon>
          <TouchableOpacity
            onPress={() => {
              props.navigation.goBack()
            }}
            hitSlop={{ top: 15, bottom: 15, left: 15, right: 15 }}
          >
            <Icon
              name="arrow-left"
              size={22}
              hitSlop={{ top: 15, bottom: 15, left: 15, right: 15 }}
              color="white"
            />
          </TouchableOpacity>
        </HeaderIcon>
        <HeaderTitleContainer>
          <IconWithText
            iconColor={'white'}
            styleText={styles.headerTitle}
            styleIcon={styles.styleIcon}
            name="comment-outline"
            text={props.scene.route.params.title}
            iconSize={17}
            numberOfLines={1}
          />
        </HeaderTitleContainer>
      </Header>
      ),
  }

  componentDidUpdate() {
    const { network, data, markCommentsAsRead } = this.props
    if (network.isLogged && data.node && data.node.unreadComments.length > 0) {
      debounce(() => markCommentsAsRead(data.node), 400)()
    }
  }
  componentWillUnmount() {
    const { onClose, action } = this.props.navigation.state.params
    onClose(action)
  }
  render() {
    const { data } = this.props
    const commentAction = data.node
      ? data.node.subject.actions.filter(action => commentsActions.includes(action.behaviorId))[0]
      : null
    // the context is the Idea
    const contextOid = data.node ? data.node.subject.oid : ''
    return (
      <AppContainer>
        <EntitiesList
          data={data}
          inverted
          withoutSeparator
          getEntities={entities => (entities.node ? entities.node.comments : undefined)}
          noContentIcon="comment-outline"
          noContentMessage={I18n.t('noComment')}
          offlineFilter={(entity, text) => entity.node.text.toLowerCase().search(text) >= 0}
          ListItem={CommentItem}
          itemdata={{
            channel: data.node ? data.node : null,
            markCommentsAsRead: this.props.markCommentsAsRead,
          }}
        />
        {commentAction
          ? <Comment
            action={commentAction}
            rootContext={contextOid}
            context={contextOid}
            channel={data.node}
          />
          : null}
      </AppContainer>
    )
  }
}

const markCommentsAsRead = gql`
  mutation($context: String!) {
    markCommentsAsRead(context: $context) {
      status
    }
  }
`

const DumbCommentsGQL = graphql(commentsQuery, {
  options: props => ({
    notifyOnNetworkStatusChange: true,
    variables: {
      first: 15,
      after: '',
      filter: '',
      id: props.navigation.state.params.id,
      processId: '',
      nodeIds: commentsActions,
    },
  }),
})(
  graphql(markCommentsAsRead, {
    props({ ownProps, mutate }) {
      const isDiscussion = ownProps.navigation.state.params.isDiscussion
      return {
        markCommentsAsRead(channel) {
          const channelId = channel.id
          return mutate({
            variables: { context: channel.oid },
            updateQueries: {
              Channels: (prev) => {
                if (isDiscussion) return prev
                const currentChannel = prev.account.channels.edges.filter(
                  item => item && item.node.id === channelId,
                )[0]
                if (!currentChannel) {
                  return prev
                }
                const index = prev.account.channels.edges.indexOf(currentChannel)
                const newChannel = update(currentChannel, {
                  node: {
                    unreadComments: {
                      $set: [],
                    },
                  },
                })
                return update(prev, {
                  account: {
                    channels: {
                      edges: {
                        $splice: [[index, 1, newChannel]],
                      },
                    },
                  },
                })
              },
              Discussion: (prev) => {
                if (!isDiscussion) return prev
                const currentChannel = prev.account.discussions.edges.filter(
                  item => item && item.node.id === channelId,
                )[0]
                if (!currentChannel) {
                  return prev
                }
                const index = prev.account.discussions.edges.indexOf(currentChannel)
                const newChannel = update(currentChannel, {
                  node: {
                    unreadComments: {
                      $set: [],
                    },
                  },
                })
                return update(prev, {
                  account: {
                    discussions: {
                      edges: {
                        $splice: [[index, 1, newChannel]],
                      },
                    },
                  },
                })
              },
            },
          })
        },
      }
    },
  })(DumbComments),
)

export const mapStateToProps = state => ({
  network: state.network,
  globalProps: state.globalProps,
})

export default connect(mapStateToProps)(DumbCommentsGQL)
