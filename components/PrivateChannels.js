/* eslint-disable no-undef */
import React from 'react'
import { graphql } from 'react-apollo'

import ChannelItem from './ChannelItem'
import EntitiesList from './EntitiesList'
import I18n from '../locale'
import { discussionsQuery } from '../queries'

export class DumbPrivateChannels extends React.Component {
  componentWillReceiveProps(nextProps) {
    if (__DEV__) {
      Object.keys(nextProps).forEach((key) => {
        if (nextProps[key] !== this.props[key]) {
          console.log('PrivateChannels', 'prop changed:', key) // eslint-disable-line
        }
      })
    }
  }

  render() {
    const { data, listStyle } = this.props
    if (__DEV__) {
      console.log('render PrivateChannels') // eslint-disable-line
    }
    return (
      <EntitiesList
        withoutSeparator
        data={data}
        getEntities={entities => (entities.account ? entities.account.discussions : [])}
        noContentIcon="comment-outline"
        noContentMessage={I18n.t('noPrivateDiscussions')}
        ListItem={ChannelItem}
        itemdata={{ isDiscussion: true }}
        style={listStyle}
        activityIndicatorColor="white"
      />
    )
  }
}

export default graphql(discussionsQuery, {
  options: () => ({
    fetchPolicy: 'cache-and-network',
    notifyOnNetworkStatusChange: true,
    variables: { first: 15, after: '' },
  }),
})(DumbPrivateChannels)
