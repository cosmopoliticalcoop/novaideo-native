/* eslint-disable react/no-array-index-key */
import React from 'react'
import { View, StyleSheet, TouchableOpacity } from 'react-native'
import ResponsiveImage from 'react-native-responsive-image'
import Swiper from 'react-native-swiper'

import Icon from './Icon'
import { Header, HeaderIcon } from './styled-components/HeaderNav'

const styles = StyleSheet.create({
  sliderContainer: {
    flex: 1,
    justifyContent: 'center',
    backgroundColor: 'black',
    alignItems: 'center',
  },
  item: {
    width: '100%',
    height: '50%',
  },
  slide: {
    flex: 1,
    justifyContent: 'center',
  },
})

export default class ImagesSlider extends React.Component {
  static navigationOptions = {
    header: (props) => {
      const { params } = props.scene.route
      return (
        <Header bgColor="black">
          <HeaderIcon>
            <TouchableOpacity
              onPress={() => {
                props.navigation.goBack()
              }}
              hitSlop={{ top: 15, bottom: 15, left: 15, right: 15 }}
            >
              <Icon
                name="arrow-left"
                size={22}
                hitSlop={{ top: 15, bottom: 15, left: 15, right: 15 }}
                color="white"
              />
            </TouchableOpacity>
          </HeaderIcon>
          <View>
            {params.sliderHeader ? params.sliderHeader() : undefined}
          </View>
        </Header>
      )
    },
  }
  componentWillUnmount() {
    this.props.navigation.state.params.onClose()
  }
  render() {
    const { images } = this.props.navigation.state.params
    return (
      <View style={styles.sliderContainer}>
        <Swiper loop={false} dotColor={'gray'}>
          {images.map((image, key) =>
            <View key={key} style={styles.slide}>
              <ResponsiveImage
                resizeMode="contain"
                source={{ uri: image.url }}
                style={styles.item}
              />
            </View>,
          )}
        </Swiper>
      </View>
    )
  }
}
