import React from 'react'
import renderer from 'react-test-renderer'
import { ThemeProvider } from 'glamorous-native'

import { theme } from '../theme'
import { DumbLogin } from './Login'

describe('Login component', () => {
  it('should match Login with error snapshot', () => {
    const instance = {
      id: 'foobar',
      url: 'https://foobar.com/logo.png/',
      isPrivate: true,
      logo: 'https://foobar.com/logo.png/',
      title: 'Foobar',
      loadingState: 'completed',
    }
    const props = {
      instance,
      network: { isConnected: true, url: { error: false } },
      user: {},
    }
    const rendered = renderer
      .create(
        <ThemeProvider theme={theme}>
          <DumbLogin {...props} />
        </ThemeProvider>,
      )
      .toJSON()
    expect(rendered).toMatchSnapshot()
  })
})
