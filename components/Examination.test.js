import React from 'react'
import renderer from 'react-test-renderer'

import Examination from './Examination'

describe('Examination component', () => {
  it('should match Examination top snapshot', () => {
    const props = {
      value: 'top',
    }
    const rendered = renderer.create(<Examination {...props} />).toJSON()
    expect(rendered).toMatchSnapshot()
  })

  it('should match Examination middle snapshot', () => {
    const props = {
      value: 'middle',
    }
    const rendered = renderer.create(<Examination {...props} />).toJSON()
    expect(rendered).toMatchSnapshot()
  })

  it('should match Examination bottom snapshot', () => {
    const props = {
      value: 'bottom',
    }
    const rendered = renderer.create(<Examination {...props} />).toJSON()
    expect(rendered).toMatchSnapshot()
  })
})
