/* eslint-disable jsx-boolean-value */
import React from 'react'
import {
  StyleSheet,
  View,
  KeyboardAvoidingView,
  Keyboard,
  Animated,
  Text,
  TouchableOpacity,
  Linking,
} from 'react-native'
import { connect } from 'react-redux'

import ThemedButton from './ThemedButton'
import TextField from './TextField'
import { setInstance, initSelection } from '../actions'
import NovaIdeoLogo from '../novaideo_logo.png'
import LoadingState from './LoadingState'
import * as constants from '../constants'
import I18n from '../locale'

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'space-between',
  },
  logoContainer: {
    alignItems: 'center',
    paddingTop: 60,
  },
  welcome: {
    color: '#bbb',
    fontSize: 26,
  },
  image: {
    margin: 20,
    marginBottom: 0,
  },
  submitView: {
    padding: 10,
  },
  loadingState: {
    alignItems: 'center',
  },
  fieldDescription: {
    fontSize: 12,
    color: '#bbb',
    opacity: 0.6,
  },
  policyText: {
    color: 'gray',
    fontSize: 12,
    textDecorationLine: 'underline',
  },
  policyContainer: {
    alignItems: 'center',
    marginTop: 10,
  },
})

export class DumbInstanceSelector extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      instanceId: '',
    }
    this.onChangeText = this.onChangeText.bind(this)
    this.keyboardWillShow = this.keyboardWillShow.bind(this)
    this.keyboardWillHide = this.keyboardWillHide.bind(this)
    this.imageHeight = new Animated.Value(constants.HOME_IMG_HEIGHT)
    this.welcomeFontSize = new Animated.Value(constants.HOME_TEXT_FS)
  }

  componentWillMount() {
    this.keyboardWillShowSub = Keyboard.addListener('keyboardDidShow', this.keyboardWillShow)
    this.keyboardWillHideSub = Keyboard.addListener('keyboardDidHide', this.keyboardWillHide)
  }

  componentWillUnmount() {
    this.keyboardWillShowSub.remove()
    this.keyboardWillHideSub.remove()
  }
  onChangeText(text) {
    this.setState({
      instanceId: text,
    })
    if (this.props.loadingState !== constants.LOADING_STATES.progress) {
      this.props.initSelection()
    }
  }

  keyboardWillShow = () => {
    Animated.timing(this.imageHeight, {
      duration: constants.ANIM_DURATION,
      toValue: constants.HOME_IMG_HEIGHT_SMALL,
    }).start()
    Animated.timing(this.welcomeFontSize, {
      duration: constants.ANIM_DURATION,
      toValue: constants.HOME_TEXT_FS_SMALL,
    }).start()
  }

  keyboardWillHide = () => {
    Animated.timing(this.imageHeight, {
      duration: constants.ANIM_DURATION,
      toValue: constants.HOME_IMG_HEIGHT,
    }).start()
    Animated.timing(this.welcomeFontSize, {
      duration: constants.ANIM_DURATION,
      toValue: constants.HOME_TEXT_FS,
    }).start()
  }

  render() {
    const { loadingState, network } = this.props
    const textInputColor = loadingState !== 'error' ? '#a2a4a2ff' : '#f00'
    const networkError = !network.isConnected || network.url.error
    return (
      <KeyboardAvoidingView style={styles.container} behavior="padding">
        <View style={styles.logoContainer}>
          <Animated.Text style={[styles.welcome, { fontSize: this.welcomeFontSize }]}>
            {I18n.t('WelcomeToNovaIdeo')}
          </Animated.Text>
          <Animated.Image
            resizeMode="contain"
            source={NovaIdeoLogo}
            style={[styles.image, { width: this.imageHeight, height: this.imageHeight }]}
          />
        </View>
        <View style={styles.submitView}>
          <TextField
            disabled={loadingState === constants.LOADING_STATES.pending}
            label={I18n.t('enterNovaIdeoInstance')}
            value={this.state.instanceId}
            dense
            highlightColor={textInputColor}
            labelColor={textInputColor}
            borderColor={textInputColor}
            textColor={'#ef6e18ff'}
            onChangeText={this.onChangeText}
          />
          <Text style={styles.fieldDescription}>
            {I18n.t('instanceSelectorText')}
          </Text>
          <LoadingState
            state={loadingState}
            errorText={I18n.t('instanceNotFound')}
            indicatorColor="#ef6e18"
            style={styles.loadingState}
          />
          <ThemedButton
            primary
            disabled={networkError || loadingState === constants.LOADING_STATES.pending}
            text={I18n.t('continue')}
            value="Continue"
            raised
            onPress={() => this.props.setInstance(this.state.instanceId)}
          />
          <TouchableOpacity
            style={styles.policyContainer}
            onPress={() => Linking.openURL(I18n.t('policyUrl'))}
          >
            <Text style={styles.policyText}>
              {I18n.t('privacyPolicy')}
            </Text>
          </TouchableOpacity>
        </View>
      </KeyboardAvoidingView>
    )
  }
}

export const mapStateToProps = state => ({
  loadingState: state.instance.loadingState,
  network: state.network,
})

export const mapDispatchToProps = {
  setInstance,
  initSelection,
}

export default connect(mapStateToProps, mapDispatchToProps)(DumbInstanceSelector)
