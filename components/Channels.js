/* eslint-disable no-undef */
import React from 'react'
import { View, Text, StyleSheet } from 'react-native'

import I18n from '../locale'
import { DrawerContainer } from './styled-components/Core'
import PrivateChannels from './PrivateChannels'
import PublicChannels from './PublicChannels'

const styles = StyleSheet.create({
  channelsContainer: {
    marginTop: 40,
    flex: 1,
  },
  channelBlok: {
    flex: 1,
  },
  channelBlokTitle: {
    color: 'white',
    fontSize: 12,
    opacity: 0.7,
    marginLeft: 14,
  },
})

class Channels extends React.Component {
  componentWillReceiveProps(nextProps) {
    if (__DEV__) {
      Object.keys(nextProps).forEach((key) => {
        if (nextProps[key] !== this.props[key]) {
          console.log('Channels', 'prop changed:', key) // eslint-disable-line
        }
      })
    }
  }

  render() {
    if (__DEV__) {
      console.log('render Channels') // eslint-disable-line
    }
    const listStyle = {
      noContentIconColor: 'white',
      noContentText: {
        color: 'white',
        opacity: 0.6,
        fontSize: 13,
      },
    }
    return (
      <DrawerContainer>
        <View style={styles.channelsContainer}>
          <View style={styles.channelBlok}>
            <Text style={styles.channelBlokTitle}>
              {I18n.t('channels').toUpperCase()}
            </Text>
            <PublicChannels listStyle={listStyle} />
          </View>
        </View>
        <View style={styles.channelsContainer}>
          <View style={styles.channelBlok}>
            <Text style={styles.channelBlokTitle}>
              {I18n.t('privateDiscussions').toUpperCase()}
            </Text>
            <PrivateChannels listStyle={listStyle} />
          </View>
        </View>
      </DrawerContainer>
    )
  }
}

export default Channels
