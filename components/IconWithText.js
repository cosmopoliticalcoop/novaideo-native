import React from 'react'
import { View, Text, StyleSheet } from 'react-native'

import Icon from './Icon'

const styles = StyleSheet.create({
  containerStyle: {
    flexDirection: 'row',
    alignItems: 'flex-start',
  },
  styleText: {
    marginLeft: 4,
    fontSize: 17,
  },
  styleIcon: {
    marginTop: 2,
  },
})

const IconWithText = ({
  name,
  text,
  containerStyle,
  styleText,
  styleIcon,
  iconSize,
  iconColor,
  numberOfLines,
}) =>
  <View style={containerStyle || styles.containerStyle}>
    <Icon style={styleIcon || styles.styleIcon} name={name} size={iconSize} color={iconColor} />
    <Text style={styleText || styles.styleText} numberOfLines={numberOfLines}>
      {text}
    </Text>
  </View>

export default IconWithText
