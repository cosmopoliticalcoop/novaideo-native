import React from 'react'
import { withTheme } from 'glamorous-native'
import Button from './Button'

const MyButton = (props) => {
  const { theme, primary, ...rest } = props
  const { color, bgColor } = primary ? theme.primary : theme.secondary
  return (
    <Button
      overrides={{
        textColor: color,
        backgroundColor: bgColor,
      }}
      {...rest}
    />
  )
}

const ThemedButton = withTheme(MyButton)
export default ThemedButton
