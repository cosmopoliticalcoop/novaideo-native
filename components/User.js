/* eslint-disable react/no-array-index-key, no-undef */
import React, { Component } from 'react'
import { Image, TouchableOpacity, StyleSheet, Text, View, RefreshControl } from 'react-native'
import { connect } from 'react-redux'
import { graphql } from 'react-apollo'
import ParallaxScrollView from 'react-native-parallax-scroll-view'

import SeparatorComponent from './SeparatorComponent'
import * as constants from '../constants'
import Icon from './Icon'
import IconWithText from './IconWithText'
import Avatar from './Avatar'
import { personQuery } from '../queries'
import { getActions } from '../utils'
import IdeaItem from './IdeaItem'
import EntitiesList from './EntitiesList'
import I18n from '../locale'

const AVATAR_SIZE = 120
const PARALLAX_HEADER_HEIGHT = 300
const STICKY_HEADER_HEIGHT = 75

const styles = StyleSheet.create({
  fixedSection: {
    position: 'absolute',
    bottom: 10,
    left: 10,
  },
  parallaxHeader: {
    flex: 1,
    alignItems: 'center',
    flexDirection: 'column',
    paddingTop: 90,
  },
  avatar: {
    marginBottom: 10,
    borderRadius: AVATAR_SIZE / 2,
    borderWidth: 2,
    borderColor: 'white',
    width: AVATAR_SIZE,
    height: AVATAR_SIZE,
  },
  sectionSpeakerText: {
    color: 'white',
    fontSize: 23,
  },
  sectionSpeakerFunction: {
    color: 'white',
    opacity: 0.8,
    fontSize: 13,
    width: '80%',
    textAlign: 'center',
  },
  header: {
    flexDirection: 'row',
    alignItems: 'center',
    padding: 25,
    paddingTop: 30,
    paddingLeft: 35,
  },
  headerAvatar: {
    justifyContent: 'space-around',
  },
  headerTitle: {
    fontSize: 17,
    color: 'white',
    opacity: 0.8,
    justifyContent: 'space-around',
    paddingLeft: 10,
  },
  container: { padding: 10, flex: 1 },
  userDescription: {
    marginTop: 15,
    marginBottom: 15,
    fontSize: 14,
    color: 'gray',
  },
  bodyFooter: {
    flexDirection: 'row',
    justifyContent: 'center',
    marginTop: 15,
    marginBottom: 15,
  },
  actionsText: {
    fontSize: 14,
    color: '#585858',
    marginLeft: 8,
    marginRight: 50,
  },
  actionsIcon: {
    marginTop: 1,
  },
  background: {
    position: 'absolute',
    top: 0,
    width: '100%',
    backgroundColor: 'rgba(0,0,0,.4)',
    height: PARALLAX_HEADER_HEIGHT,
  },
})

export class DumbUser extends Component {
  static navigationOptions = {
    header: () => <View />,
  }
  constructor(props) {
    super(props)
    this.state = {
      opened: false,
      actionOpened: false,
    }
    this.onRefresh = this.onRefresh.bind(this)
    this.onClose = this.onClose.bind(this)
    this.onActionClose = this.onActionClose.bind(this)
    this.onActionPress = this.onActionPress.bind(this)
    this.performAction = this.performAction.bind(this)
    this.onActionLongPress = this.onActionLongPress.bind(this)
  }
  componentWillUnmount() {
    this.props.navigation.state.params.onClose()
  }
  onClose() {
    if (this.state.opened) {
      this.setState({ opened: false })
    }
  }

  onActionClose() {
    if (this.state.actionOpened) {
      this.setState({ actionOpened: false })
    }
  }

  onActionLongPress(action) {
    if (action && action.description) this.props.globalProps.showMessage(action.description)
  }

  onActionPress(action) {
    this.performAction(action, this.props.data.person, this)
  }

  onRefresh() {
    this.props.data.refetch()
  }

  performAction(action, user, source) {
    const { network, globalProps } = this.props
    const { theme } = this.props.navigation.state.params
    // TODO create new channel in user.channel is undefined
    if (action.nodeId === 'discuss' && user.channel) {
      if (!source.state.actionOpened) {
        source.setState({ actionOpened: true })
        this.props.globalProps.navigation.navigate('Comments', {
          id: user.channel.id,
          isDiscussion: true,
          title: user.title,
          onClose: source.onActionClose,
          theme,
          action,
        })
      }
    } else if (!network.isLogged) {
      globalProps.showMessage(I18n.t('LogInToPerformThisAction'))
    } else {
      globalProps.showMessage(I18n.t('comingSoon'))
    }
  }
  render() {
    const { data, navigation } = this.props
    const { user, theme } = this.props.navigation.state.params
    const userPicture = user.picture
    const defaultPicture = user.isAnonymous
      ? constants.DEFAULT_ANONYMOUS_IMG
      : constants.DEFAULT_USER_IMG
    const communicationActions = data.person
      ? getActions(data.person.actions, constants.ACTIONS.communicationAction)
      : []

    return (
      <ParallaxScrollView
        refreshControl={
          <RefreshControl refreshing={data.networkStatus === 4} onRefresh={this.onRefresh} />
        }
        backgroundColor={theme.primary.bgColor}
        stickyHeaderHeight={STICKY_HEADER_HEIGHT}
        parallaxHeaderHeight={PARALLAX_HEADER_HEIGHT}
        backgroundSpeed={10}
        renderBackground={() =>
          <View key="background">
            <Image
              resizeMode="cover"
              height={PARALLAX_HEADER_HEIGHT}
              source={
                userPicture
                  ? { uri: `${userPicture.url}/blur`, height: PARALLAX_HEADER_HEIGHT }
                  : defaultPicture
              }
            />
            <View style={styles.background} />
          </View>}
        renderForeground={() =>
          <View key="parallax-header" style={styles.parallaxHeader}>
            <Image
              style={styles.avatar}
              source={
                userPicture ? { uri: `${userPicture.url}/profil` } : constants.DEFAULT_USER_IMG
              }
            />
            <Text style={styles.sectionSpeakerText}>
              {user.title}
            </Text>
            <Text style={styles.sectionSpeakerFunction}>
              {user.function}
            </Text>
          </View>}
        renderStickyHeader={() =>
          <View style={styles.header}>
            <Avatar
              size={40}
              style={styles.headerAvatar}
              image={
                <Image
                  source={
                    userPicture ? { uri: `${userPicture.url}/profil` } : constants.DEFAULT_USER_IMG
                  }
                />
              }
            />
            <Text style={styles.headerTitle}>
              {user.title}
            </Text>
          </View>}
        renderFixedHeader={() =>
          <View key="fixed-header" style={styles.fixedSection}>
            <TouchableOpacity
              onPress={() => {
                navigation.goBack()
              }}
              hitSlop={{ top: 15, bottom: 15, left: 15, right: 15 }}
            >
              <Icon name="arrow-left" size={22} color={theme.primary.color} />
            </TouchableOpacity>
          </View>}
      >
        <View style={styles.container}>
          <View>
            <Text style={styles.userDescription}>
              {user.description}
            </Text>
          </View>
          {communicationActions.length > 0
            ? <View>
              <SeparatorComponent />
              <View style={styles.bodyFooter}>
                {communicationActions.map((action, key) =>
                  <TouchableOpacity
                    key={key}
                    onPress={() => this.onActionPress(action)}
                    onLongPress={() => this.onActionLongPress(action)}
                    hitSlop={{ top: 15, bottom: 15, left: 15, right: 15 }}
                  >
                    <IconWithText
                      styleText={styles.actionsText}
                      styleIcon={styles.actionsIcon}
                      iconColor="#585858"
                      name={action.stylePicto}
                      text={action.counter}
                      iconSize={17}
                      numberOfLines={1}
                    />
                  </TouchableOpacity>,
                  )}
              </View>
              <SeparatorComponent />
            </View>
            : null}
        </View>
        <EntitiesList
          data={data}
          getEntities={entities => (entities.person ? entities.person.contents : null)}
          noContentIcon="lightbulb"
          noContentMessage={I18n.t('noIdeas')}
          noContentFoundMessage={I18n.t('noIdeaFound')}
          offlineFilter={(entity, text) =>
            entity.node.title.toLowerCase().search(text) >= 0 ||
            entity.node.text.toLowerCase().search(text) >= 0 ||
            entity.node.keywords.join(' ').toLowerCase().search(text) >= 0}
          ListItem={IdeaItem}
        />
      </ParallaxScrollView>
    )
  }
}

export const mapStateToProps = state => ({
  globalProps: state.globalProps,
  network: state.network,
})

export default connect(mapStateToProps)(
  graphql(personQuery, {
    options: props => ({
      fetchPolicy: 'cache-and-network',
      variables: {
        id: props.navigation.state.params.user.id,
        first: 15,
        after: '',
        filter: '',
      },
    }),
  })(DumbUser),
)
