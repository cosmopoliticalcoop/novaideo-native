import React from 'react'
import ShallowRenderer from 'react-test-renderer/shallow'
import { ThemeProvider } from 'glamorous-native'

import { theme } from '../theme'
import { ThemedDumbHeader } from './Header'

describe('Header component', () => {
  it('should match Header with data snapshot', () => {
    const instance = {
      id: 'foobar',
      url: 'https://foobar.com/logo.png/',
      isPrivate: false,
      logo: 'https://foobar.com/logo.png/',
      title: 'Foobar',
      loadingState: 'completed',
    }
    const user = {
      token: undefined,
      loadingState: undefined,
    }
    const initialState = {
      instance,
      user,
      network: { isConnected: true, url: { error: false } },
      search: { text: '' },
    }
    const network = initialState.network
    const renderer = new ShallowRenderer()
    renderer.render(
      <ThemeProvider theme={theme}>
        <ThemedDumbHeader network={network} search={{ text: '' }} />
      </ThemeProvider>,
    )
    const rendered = renderer.getRenderOutput()

    expect(rendered).toMatchSnapshot()
  })
})
