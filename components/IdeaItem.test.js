import React from 'react'
import renderer from 'react-test-renderer'

import { DumbIdeaItem } from './IdeaItem'
import Examination from './Examination'

describe('IdeaItem component', () => {
  it('should match IdeaItem with support snapshot', () => {
    const idea = {
      title: 'Foobar',
      keywords: ['k1', 'k2'],
      text: 'Idea text',
      createdAt: '2017-01-01T12:30:59',
      presentationText: 'Presentation text',
      state: ['published', 'submitted_support'],
      actions: [],
      author: {
        picture: {
          url: 'http//example.nova-ideo.com/principals/users/JohnDoe/avatar.jpg',
        },
        title: 'John Doe',
      },
    }
    const props = {
      node: idea,
      instance: {
        support_ideas: true,
      },
      adapters: {
        examination: Examination,
      },
      globalProps: {},
    }
    const rendered = renderer.create(<DumbIdeaItem {...props} />).toJSON()
    expect(rendered).toMatchSnapshot()
  })

  it('should match IdeaItem without support snapshot', () => {
    const idea = {
      title: 'Foobar',
      keywords: ['k1', 'k2'],
      text: 'Idea text',
      createdAt: '2017-01-01T12:30:59',
      presentationText: 'Presentation text',
      state: ['published', 'examined', 'favorable'],
      actions: [],
      author: {
        picture: {
          url: 'http//example.nova-ideo.com/principals/users/JohnDoe/avatar.jpg',
        },
        title: 'John Doe',
      },
    }
    const props = {
      node: idea,
      instance: {
        support_ideas: false,
      },
      adapters: {
        examination: Examination,
      },
      globalProps: {},
    }
    const rendered = renderer.create(<DumbIdeaItem {...props} />).toJSON()
    expect(rendered).toMatchSnapshot()
  })
})
