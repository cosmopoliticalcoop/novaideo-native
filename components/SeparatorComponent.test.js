import React from 'react'
import renderer from 'react-test-renderer'

import SeparatorComponent from './SeparatorComponent'

describe('SeparatorComponent component', () => {
  it('should match SeparatorComponent snapshot', () => {
    const rendered = renderer.create(<SeparatorComponent />).toJSON()
    expect(rendered).toMatchSnapshot()
  })
})
