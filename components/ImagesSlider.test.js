import React from 'react'
import renderer from 'react-test-renderer'

import ImagesSlider from './ImagesSlider'

describe('ImagesSlider component', () => {
  it('should match ImagesSlider with data snapshot', () => {
    const props = {
      navigation: {
        state: {
          params: {
            images: [
              { url: 'https://foobar.com/image1' },
              { url: 'https://foobar.com/image2' },
              { url: 'https://foobar.com/image3' },
              { url: 'https://foobar.com/image4' },
            ],
          },
        },
      },
    }
    const rendered = renderer.create(<ImagesSlider {...props} />).toJSON()
    expect(rendered).toMatchSnapshot()
  })
  it('should match ImagesSlider without images snapshot', () => {
    const props = {
      navigation: {
        state: {
          params: {
            images: [],
          },
        },
      },
    }
    const rendered = renderer.create(<ImagesSlider {...props} />).toJSON()
    expect(rendered).toMatchSnapshot()
  })
})
