/* eslint-disable no-undef */
import React from 'react'
import { graphql } from 'react-apollo'

import ChannelItem from './ChannelItem'
import EntitiesList from './EntitiesList'
import I18n from '../locale'
import { channelsQuery } from '../queries'

export class DumbPublicChannels extends React.Component {
  componentWillReceiveProps(nextProps) {
    if (__DEV__) {
      Object.keys(nextProps).forEach((key) => {
        if (nextProps[key] !== this.props[key]) {
          console.log('PublicChannels', 'prop changed:', key) // eslint-disable-line
        }
      })
    }
  }

  render() {
    const { data, listStyle } = this.props
    if (__DEV__) {
      console.log('render PublicChannels') // eslint-disable-line
    }
    return (
      <EntitiesList
        withoutSeparator
        data={data}
        getEntities={entities => (entities.account ? entities.account.channels : [])}
        noContentIcon="comment-outline"
        noContentMessage={I18n.t('noChannels')}
        ListItem={ChannelItem}
        style={listStyle}
        activityIndicatorColor="white"
      />
    )
  }
}

export default graphql(channelsQuery, {
  options: () => ({
    fetchPolicy: 'cache-and-network',
    notifyOnNetworkStatusChange: true,
    variables: { first: 15, after: '' },
  }),
})(DumbPublicChannels)
