import React from 'react'
import ShallowRenderer from 'react-test-renderer/shallow'

import { DumbPrivateChannels } from './PrivateChannels'

describe('PrivateChannels component', () => {
  it('should match PrivateChannels with data snapshot', () => {
    const props = {
      navigation: {},
    }
    const renderer = new ShallowRenderer()
    renderer.render(<DumbPrivateChannels {...props} />)
    const rendered = renderer.getRenderOutput()
    expect(rendered).toMatchSnapshot()
  })
})
