/* eslint-disable import/prefer-default-export, no-underscore-dangle, no-param-reassign */
import { UploadHTTPFetchNetworkInterface } from 'apollo-upload-client'

import { ICONS_MAPPING } from './constants'

export function asyncGetInstanceInfo(instanceId) {
  const url = `https://infra.nova-ideo.com/instances/${instanceId}`
  return fetch(url, {
    method: 'get',
    headers: { Accept: 'application/json', 'Content-Type': 'application/json' },
  }).then((response) => {
    if (response.ok) {
      return response.json()
    }
    // no instance with this id
    return undefined
  })
}

export function asyncLogin(instance, login, password, token) {
  const url = `${instance.url}/json_login`
  return fetch(url, {
    method: 'post',
    headers: { Accept: 'application/json', 'Content-Type': 'application/json' },
    body: JSON.stringify({ login, password, token }),
  }).then((response) => {
    if (response.ok) {
      return response.json()
    }
    // login failed
    return undefined
  })
}

export function asyncLogout(instance) {
  const url = `${instance.url}/json_logout`
  return fetch(url, {
    method: 'post',
    headers: { Accept: 'application/json', 'Content-Type': 'application/json' },
  }).then((response) => {
    if (response.ok) {
      return response.json()
    }
    // logout failed
    return undefined
  })
}

export class DynamicUploadHTTPFetchNetworkInterface extends UploadHTTPFetchNetworkInterface {
  reset(props) {
    const { uri } = props
    this._uri = uri || '/graphql'
    this._middlewares = []
  }
}

export const createNetworkInterface = ({ uri, opts = {} }) =>
  new DynamicUploadHTTPFetchNetworkInterface(uri, opts)

export const configureNetworkInterface = (client, data) => {
  const { uri, token } = data
  client.networkInterface.reset({ uri })
  if (token) {
    client.networkInterface.use([
      {
        applyMiddleware(req, next) {
          req.options.headers = {
            'X-Api-Key': token,
          }
          next()
        },
      },
    ])
  }
}

export const getActions = (actions, descriminator) => {
  const newActions = actions
    .filter(action => action.styleDescriminator === descriminator)
    .map((action) => {
      const newAction = { ...action }
      newAction.stylePicto =
        action.stylePicto in ICONS_MAPPING
          ? ICONS_MAPPING[action.stylePicto]
          : 'alert-circle-outline'
      return newAction
    })
  newActions.sort((a1, a2) => a1.styleOrder - a2.styleOrder)
  return newActions
}
