/* eslint-disable import/prefer-default-export */
import * as constants from './constants'
import { asyncGetInstanceInfo, asyncLogin, asyncLogout } from './utils'

export const loadAdapters = instanceId => ({
  type: constants.LOAD_ADAPTERS,
  instanceId,
})

export const setInstance = instanceId => ({
  payload: asyncGetInstanceInfo(instanceId),
  type: constants.SET_INSTANCE,
})

export const initSelection = () => ({
  type: constants.IN_PROGRESS,
})

export const userLogin = (instance, login, password, token) => ({
  payload: asyncLogin(instance, login, password, token),
  type: constants.LOGIN,
})

export const updateUserToken = (instance, token) => ({
  instance,
  token,
  type: constants.UPDATE_TOKEN,
})

export const initLogin = () => ({
  type: constants.LOGIN_IN_PROGRESS,
})

export const searchEntities = text => ({
  type: constants.SEARCH_ENTITIES,
  text,
})

export const logout = instance => ({
  payload: asyncLogout(instance),
  type: constants.LOGOUT,
})

export const initInstance = instance => ({
  payload: asyncLogout(instance),
  type: constants.INIT_INSTANCE,
})

export const setConnectionState = isConnected => ({
  type: constants.SET_CONNECTION_STATE,
  isConnected,
})

export const setURLState = (error, messages) => ({
  type: constants.SET_URL_STATE,
  error,
  messages,
})

export const setTheme = (instance, color) => ({
  type: constants.SET_THEME,
  instance,
  color,
})

export const updateGlobalProps = props => ({
  type: constants.UPDATE_GLOBAL_PROPS,
  props,
})
