/* eslint-disable react/no-did-mount-set-state */
// @flow
import React from 'react'
import { ApolloProvider } from 'react-apollo'
import { View, StyleSheet } from 'react-native'
import I18n from 'react-native-i18n'
import Expo from 'expo'
import Toast from 'react-native-easy-toast'

// import getAllAdapters from './components/vendor/utils'
import apolloClient from './apolloClient'
import getStore from './store'
import NovaIdeoNavApp from './components/NovaIdeoNavApp'

const styles = StyleSheet.create({
  container: { flex: 1, zIndex: 40 },
})

// const instance = {
//   id: 'timeo',
//   url: 'http://192.168.1.29:6543',
//   isPrivate: false,
//   logo: 'https://demo.nova-ideo.com/novaideostatic/images/novaideo_logo.png',
//   title: 'Ecreall',
//   examine_ideas: true,
//   support_ideas: true,
//   loadingState: 'completed',
// }
//
// const user = {
//   token: '0bfa81ae040541aeb65df6d8a710631e',
//   loadingState: 'completed',
// }
//
// const search = {
//   text: '',
// }
const initialState = {
  // instance,
  // user,
  // search,
  // network: { isConnected: false },
  // adapters: getAllAdapters('ecreall'),
}

export default class MainApp extends React.Component {
  constructor(props: any) {
    super(props)
    this.state = {
      storeLoaded: false,
    }
    this.store = getStore(initialState, apolloClient, () => this.setState({ storeLoaded: true }))
    Expo.Util.getCurrentLocaleAsync().then((lng) => {
      const locale = lng.split('_')[0]
      I18n.locale = locale
    })
    this.showMessage = this.showMessage.bind(this)
    this.showError = this.showError.bind(this)
  }

  state: { storeLoaded: boolean }
  store: Object
  toast: Object
  showMessage: Function
  showError: Function

  showMessage(message: string) {
    this.toast.show(message, 1500)
  }
  showError(error: Object) {
    this.showMessage(error.graphQLErrors.map(e => e.message)[0])
  }
  render() {
    if (!this.state.storeLoaded) return null
    const rootProps = {
      showMessage: this.showMessage,
      showError: this.showError,
    }
    return (
      <ApolloProvider client={apolloClient} store={this.store}>
        <View style={styles.container}>
          <Toast
            ref={(toast) => {
              this.toast = toast
            }}
            style={{ backgroundColor: '#00b4f9' }}
            fadeInDuration={200}
            fadeOutDuration={200}
          />
          <NovaIdeoNavApp rootProps={rootProps} />
        </View>
      </ApolloProvider>
    )
  }
}
