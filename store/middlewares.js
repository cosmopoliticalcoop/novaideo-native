import promiseMiddleware from 'redux-promise-middleware'

const middlewares = [promiseMiddleware()]

export default middlewares
