import { AsyncStorage } from 'react-native'
import { createStore, applyMiddleware, compose, combineReducers } from 'redux'
import { reducer as formReducer } from 'redux-form'
import { persistStore, autoRehydrate } from 'redux-persist' // add new import

export default function configureStore(
  initialState,
  rootReducer,
  middlewares,
  apolloClient,
  onComplete,
) {
  const store = createStore(
    combineReducers({ ...rootReducer, apollo: apolloClient.reducer(), form: formReducer }),
    initialState,
    compose(autoRehydrate(), applyMiddleware(...middlewares, apolloClient.middleware())),
  )
  // begin periodically persisting the store
  persistStore(
    store,
    { storage: AsyncStorage, blacklist: ['network', 'adapters', 'search'] },
    onComplete,
  )
  return store
}
