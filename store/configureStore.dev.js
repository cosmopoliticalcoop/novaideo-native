import { AsyncStorage } from 'react-native'
import { createStore, applyMiddleware, compose, combineReducers } from 'redux'
import { reducer as formReducer } from 'redux-form'
// import { composeWithDevTools } from 'remote-redux-devtools'
import { persistStore, autoRehydrate } from 'redux-persist' // add new import

export default function configureStore(
  initialState,
  rootReducer,
  middlewares,
  apolloClient,
  onComplete,
) {
  const reducers = apolloClient
    ? combineReducers({ ...rootReducer, apollo: apolloClient.reducer(), form: formReducer })
    : combineReducers(rootReducer)
  const allmiddlewares = apolloClient
    ? applyMiddleware(...middlewares, apolloClient.middleware())
    : applyMiddleware(...middlewares)
  const store = createStore(
    reducers,
    initialState,
    // compose(autoRehydrate(), composeWithDevTools(allmiddlewares)),
    compose(autoRehydrate(), allmiddlewares),
  )
  // begin periodically persisting the store
  persistStore(
    store,
    { storage: AsyncStorage, blacklist: ['network', 'adapters', 'search'] },
    onComplete,
  )
  return store
}
