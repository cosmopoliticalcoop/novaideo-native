import configureStore from './configureStore'
import middlewares from './middlewares'
import rootReducer from '../reducers'

export default (initialState, apolloClient, onComplete) =>
  configureStore(initialState, rootReducer, middlewares, apolloClient, onComplete)
