import * as constants from './constants'
import * as reducers from './reducers'
import TimeoExamination from './components/vendor/timeo/Examination'
import getTimeoTheme from './components/vendor/timeo/theme'
import Examination from './components/Examination'
import getTheme from './theme'

describe('Reducers', () => {
  describe('instance reducer', () => {
    const { instance } = reducers
    it('should return state', () => {
      const state = {
        id: 'foobar',
        loadingState: 'completed',
        url: 'http://foobar.nova-ideo.com',
        isPrivate: true,
        logo: 'http://foobar.nova-ideo.com',
        title: 'Foobar',
      }
      const actual = instance(state, {})
      expect(actual).toEqual(state)
    })

    it('should handle SET_INSTANCE action type', () => {
      const state = {
        id: 'foobar',
        url: 'http://foobar.nova-ideo.com',
        isPrivate: true,
        logo: 'http://foobar.nova-ideo.com',
        title: 'Foobar',
        examine_ideas: true,
        support_ideas: true,
        loadingState: 'completed',
      }
      const action = {
        payload: {
          app: 'myinstance',
          url: 'http://myinstance.nova-ideo.com',
          type: 'private',
          logoUrl: 'http://foobar.nova-ideo.com',
          title: 'Foobar',
          config: {
            examine_ideas: true,
            support_ideas: true,
          },
        },
        type: `${constants.SET_INSTANCE}_FULFILLED`,
      }
      const actual = instance(state, action)
      const expected = {
        id: 'myinstance',
        loadingState: 'completed',
        url: 'http://myinstance.nova-ideo.com',
        isPrivate: true,
        logo: 'http://foobar.nova-ideo.com',
        title: 'Foobar',
        examine_ideas: true,
        support_ideas: true,
      }
      expect(actual).toEqual(expected)
    })
  })

  describe('user reducer', () => {
    const { user } = reducers
    it('should return state', () => {
      const state = {
        token: 'foobar',
        loadingState: 'completed',
      }
      const actual = user(state, {})
      expect(actual).toEqual(state)
    })

    it('should handle LOGIN action type', () => {
      const state = {
        instance: {
          url: 'http://myinstance.nova-ideo.com',
        },
        login: 'foo',
        password: 'bar',
      }
      const action = {
        payload: {
          token: 'foobar',
          status: true,
        },
        type: `${constants.LOGIN}_FULFILLED`,
      }
      const actual = user(state, action)
      const expected = {
        token: 'foobar',
        loadingState: 'completed',
      }
      expect(actual).toEqual(expected)
    })
  })

  describe('search reducer', () => {
    const { search } = reducers
    it('should return state', () => {
      const state = {
        text: 'foobar',
      }
      const actual = search(state, {})
      expect(actual).toEqual(state)
    })

    it('should handle SEARCH_ENTITIES action type', () => {
      const state = {
        text: 'foobar',
      }
      const action = {
        payload: {
          text: 'foobar',
        },
        type: `${constants.SEARCH_ENTITIES}_FULFILLED`,
      }
      const actual = search(state, action)
      const expected = {
        text: 'foobar',
      }
      expect(actual).toEqual(expected)
    })
  })

  describe('network reducer', () => {
    const { network } = reducers
    it('should return state', () => {
      const state = {
        isConnected: false,
      }
      const actual = network(state, {})
      expect(actual).toEqual(state)
    })

    it('should handle SET_CONNECTION_STATE action type', () => {
      const state = {
        isConnected: false,
        isLogged: false,
        url: { error: false, messages: [] },
      }
      const action = {
        isConnected: true,
        type: constants.SET_CONNECTION_STATE,
      }
      const actual = network(state, action)
      const expected = {
        isConnected: true,
        isLogged: false,
        url: { error: false, messages: [] },
      }
      expect(actual).toEqual(expected)
    })

    it('should handle SET_URL_STATE action type', () => {
      const state = {
        isConnected: false,
        isLogged: false,
        url: { error: false, messages: [] },
      }
      const action = {
        error: true,
        messages: ['foobar'],
        type: constants.SET_URL_STATE,
      }
      const actual = network(state, action)
      const expected = {
        isConnected: false,
        isLogged: false,
        url: { error: true, messages: ['foobar'] },
      }
      expect(actual).toEqual(expected)
    })

    it('should handle LOGIN action type', () => {
      const state = {
        isConnected: false,
        isLogged: false,
        url: { error: false, messages: [] },
      }
      const action = {
        payload: { status: true },
        type: `${constants.LOGIN}_FULFILLED`,
      }
      const actual = network(state, action)
      const expected = {
        isConnected: false,
        isLogged: true,
        url: { error: false, messages: [] },
      }
      expect(actual).toEqual(expected)
    })

    it('should handle LOGOUT action type', () => {
      const state = {
        isConnected: false,
        isLogged: true,
        url: { error: false, messages: [] },
      }
      const action = {
        payload: { status: true },
        type: `${constants.LOGOUT}_FULFILLED`,
      }
      const actual = network(state, action)
      const expected = {
        isConnected: false,
        isLogged: false,
        url: { error: false, messages: [] },
      }
      expect(actual).toEqual(expected)
    })

    it('should handle LOGIN action type', () => {
      const state = {
        isConnected: false,
        isLogged: false,
        url: { error: false, messages: [] },
      }
      const action = {
        payload: { status: true },
        type: `${constants.LOGIN}_REJECTED`,
      }
      const actual = network(state, action)
      const expected = {
        isConnected: false,
        isLogged: false,
        url: { error: true, messages: ['Login failed'] },
      }
      expect(actual).toEqual(expected)
    })

    it('should handle LOGOUT rejected action type', () => {
      const state = {
        isConnected: false,
        isLogged: true,
        url: { error: false, messages: [] },
      }
      const action = {
        type: `${constants.LOGOUT}_REJECTED`,
      }
      const actual = network(state, action)
      const expected = {
        isConnected: false,
        isLogged: true,
        url: { error: true, messages: ['Logout failed'] },
      }
      expect(actual).toEqual(expected)
    })
  })

  describe('adapters reducer', () => {
    const { adapters } = reducers
    it('should handle SET_INSTANCE action type (Default components)', () => {
      const state = {
        examination: undefined,
      }
      const action = {
        payload: {
          app: 'myinstance',
        },
        type: `${constants.SET_INSTANCE}_FULFILLED`,
      }
      const actual = adapters(state, action)
      const expected = {
        examination: Examination,
        theme: getTheme,
      }
      expect(actual).toEqual(expected)
    })

    it('should handle SET_INSTANCE action type (timeo components)', () => {
      const state = {
        examination: undefined,
      }
      const action = {
        payload: {
          app: 'timeo',
        },
        type: `${constants.SET_INSTANCE}_FULFILLED`,
      }
      const actual = adapters(state, action)
      const expected = {
        examination: TimeoExamination,
        theme: getTimeoTheme,
      }
      expect(actual).toEqual(expected)
    })
  })
})
