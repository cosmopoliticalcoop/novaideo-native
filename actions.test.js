import * as actions from './actions'
import * as constants from './constants'
import { asyncGetInstanceInfo, asyncLogin } from './utils'

describe('Actions', () => {
  describe('setInstance action', () => {
    it('should return a SET_INSTANCE async action', () => {
      const { setInstance } = actions
      const actual = setInstance('foobar')
      const expected = {
        payload: asyncGetInstanceInfo('foobar'),
        type: constants.SET_INSTANCE,
      }
      expect(actual).toEqual(expected)
    })
  })

  describe('initSelection action', () => {
    it('should return a IN_PROGRESS action', () => {
      const { initSelection } = actions
      const actual = initSelection()
      const expected = {
        type: constants.IN_PROGRESS,
      }
      expect(actual).toEqual(expected)
    })
  })

  describe('userLogin action', () => {
    it('should return a LOGIN action', () => {
      const { userLogin } = actions
      const actual = userLogin({ url: 'http://foobar.nova-ideo.com' }, 'foo', 'bar')
      const expected = {
        payload: asyncLogin({ url: 'http://foobar.nova-ideo.com' }, 'foo', 'bar'),
        type: constants.LOGIN,
      }
      expect(actual).toEqual(expected)
    })
  })

  describe('initLogin action', () => {
    it('should return a LOGIN_IN_PROGRESS action', () => {
      const { initLogin } = actions
      const actual = initLogin()
      const expected = {
        type: constants.LOGIN_IN_PROGRESS,
      }
      expect(actual).toEqual(expected)
    })
  })

  describe('searchEntities action', () => {
    it('should return a SEARCH_ENTITIES action', () => {
      const { searchEntities } = actions
      const actual = searchEntities('foobar')
      const expected = {
        type: constants.SEARCH_ENTITIES,
        text: 'foobar',
      }
      expect(actual).toEqual(expected)
    })
  })
})
