/* eslint-disable global-require */
/* eslint-disable import/prefer-default-export, import/no-extraneous-dependencies */

export const PRIVACY_POLICY_URL_FR = 'https://www.iubenda.com/privacy-policy/8216991'

export const PRIVACY_POLICY_URL_EN = 'https://www.iubenda.com/privacy-policy/8216928'

export const FONTS = {
  robotoRegular: require('./assets/fonts/Roboto-Regular.ttf'),
  robotoMedium: require('./assets/fonts/Roboto-Medium.ttf'),
}

export const DEFAULT_USER_IMG = require('./user.png')

export const DEFAULT_ANONYMOUS_IMG = require('./anonymous.png')

export const DEFAULT_USER_IMG_BLUR = require('./user_blur.png')

export const SET_INSTANCE = 'SET_INSTANCE'

export const IN_PROGRESS = 'IN_PROGRESS'

export const LOGIN_IN_PROGRESS = 'LOGIN_IN_PROGRESS'

export const LOGIN = 'LOGIN'

export const LOGOUT = 'LOGOUT'

export const INIT_INSTANCE = 'INIT_INSTANCE'

export const SEARCH_ENTITIES = 'SEARCH_ENTITIES'

export const SET_CONNECTION_STATE = 'SET_CONNECTION_STATE'

export const SET_URL_STATE = 'SET_URL_STATE'

export const SET_THEME = 'SET_THEME'

export const UPDATE_TOKEN = 'UPDATE_TOKEN'

export const LOAD_ADAPTERS = 'LOAD_ADAPTERS'

export const UPDATE_GLOBAL_PROPS = 'UPDATE_GLOBAL_PROPS'

export const LOADING_STATES = {
  pending: 'pending',
  progress: 'progress',
  error: 'error',
  success: 'success',
}

export const ANIM_DURATION = 200

export const HOME_IMG_HEIGHT = 100

export const HOME_IMG_HEIGHT_SMALL = 80

export const HOME_TEXT_FS = 22

export const HOME_TEXT_FS_SMALL = 20

export const ACTIONS = {
  communicationAction: 'communication-action',
}

export const ICONS_MAPPING = {
  'ion-chatbubble': 'comment-outline',
  'glyphicon glyphicon-share-alt': 'share',
  'glyphicon glyphicon-star-empty': 'star-outline',
  'glyphicon glyphicon-star': 'star',
}
